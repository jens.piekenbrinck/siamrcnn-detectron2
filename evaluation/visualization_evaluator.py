import logging
import numpy as np
import torch
import cv2
import matplotlib.cm as cm

from detectron2.data import MetadataCatalog
from detectron2.evaluation import DatasetEvaluator
from detectron2.structures.instances import Instances
from detectron2.utils.events import get_event_storage
from detectron2.utils.visualizer import Visualizer
from detectron2.modeling.postprocessing import detector_postprocess


def blended_objectness_heatmap(
    image: np.array, objectness: np.array, blend: float = 0.5
):
    jet_r = cm.get_cmap(name="jet")
    cmap = jet_r(objectness)[..., :3] * 255.0
    cmap = cv2.resize(
        cmap, (image.shape[1], image.shape[0]), interpolation=cv2.INTER_NEAREST
    )
    ret = blend * cmap + (1 - blend) * image
    ret = ret.astype(dtype=image.dtype)
    return ret, cmap


class VisualizationEvaluator(DatasetEvaluator):
    """
    Visualize instance detection outputs in tensorboard.
    """

    def __init__(self, dataset_name, cfg):
        self.dataset_name = dataset_name
        self.cfg = cfg

        self.input_format = cfg.INPUT.FORMAT

        self._cpu_device = torch.device("cpu")
        self._logger = logging.getLogger(__name__)
        self._metadata = MetadataCatalog.get(dataset_name)

        self._visualized = False

    def reset(self):
        self._visualized = False

    def process(self, inputs, outputs):
        """
        Args:
            inputs: the inputs to the model (e.g., GeneralizedSiamContextRCNN).
                It is a list of dict. Each dict corresponds to a video and each frame is a dict that
                contains keys like "height", "width", "file_name", "image_id".
            outputs: the outputs of the model. It is in the format tuple[list[list[dict], context]],
                where the dict has the key "instances" that contains :class:`Instances`.
        """
        if self._visualized:
            return
        self._visualized = True

        outputs = outputs[0]  # at position 1 is the last context which is not needed
        storage = get_event_storage()
        vis_top_k = 10
        scale = 0.4

        postprocessed = []
        for out, input in zip(outputs, inputs):
            processed = []
            for t, frame in enumerate(input["search"]):
                inst = Instances(
                    out[t]["instances"].image_size,
                    **out[t]["instances"].to(self._cpu_device).get_fields()
                )
                pred_masks = None
                if inst.has(
                    "pred_masks"
                ):  # pred masks not needed and causes issues when resizing in postprocess
                    pred_masks = inst.pred_masks
                    inst.remove("pred_masks")
                inst = detector_postprocess(inst, *frame["image"].size()[1:])
                if pred_masks is not None:
                    inst.pred_masks = (
                        torch.nn.functional.interpolate(
                            pred_masks.unsqueeze(1).float(),
                            size=tuple(frame["image"].size()[1:]),
                            mode="nearest",
                        )
                        .squeeze(1)
                        .bool()
                    )
                processed.append({"instances": inst})
            postprocessed.append(processed)

        for input, prediction in zip(inputs, postprocessed):
            res = []
            for t, frame in enumerate(input["search"]):
                if t % 2 == 1:
                    continue
                img = frame["image"].cpu().numpy()
                assert img.shape[0] == 3, "Images should have 3 channels."
                if self.input_format == "BGR":
                    img = img[::-1, :, :]
                img = img.transpose(1, 2, 0)

                pred = prediction[t]["instances"]
                box_size = min(len(pred.pred_boxes), vis_top_k)
                v_pred = Visualizer(img, metadata=self._metadata, scale=scale)
                v_pred.overlay_instances(
                    boxes=frame["instances"].gt_boxes, assigned_colors=[[0.0, 1.0, 0.0]]
                )
                v_pred = v_pred.overlay_instances(
                    boxes=pred[:box_size].pred_boxes,
                    labels=["{:.0f}%".format(s * 100) for s in pred[:box_size].scores],
                )
                preds_img = v_pred.get_image()

                v_best_pred = Visualizer(img, metadata=self._metadata, scale=scale)
                v_best_pred.overlay_instances(
                    boxes=frame["instances"].gt_boxes, assigned_colors=[[0.0, 1.0, 0.0]]
                )
                v_best_pred.overlay_instances(
                    boxes=pred[:1].pred_boxes,
                    masks=pred[:1].pred_masks if pred[:1].has("pred_masks") else None,
                    assigned_colors=[[1.0, 0.0, 0.0]],
                    labels=["{:.0f}%".format(s * 100) for s in pred[:1].scores],
                )
                best_pred_img = v_best_pred.get_output().get_image()

                vis_img = np.concatenate((best_pred_img, preds_img), axis=1)

                vis_img = vis_img.transpose(2, 0, 1)
                res.append(vis_img)

            vis_name = "Evaluation; Left: Template; Center: GT bounding boxes and best prediction; Right: Top predictions;"
            vis_img = np.concatenate(res, axis=1)
            storage.put_image(vis_name, vis_img)
            break  # only visualize one video in a batch

    def evaluate(self):
        return {}
