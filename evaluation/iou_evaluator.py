from collections import OrderedDict
import copy
import itertools
import random
import string
import logging
import json
from detectron2.structures.instances import Instances
import numpy as np
import os
import torch

import detectron2.utils.comm as comm
from detectron2.data import MetadataCatalog
from detectron2.evaluation import DatasetEvaluator
from detectron2.structures.boxes import Boxes, BoxMode, pairwise_iou
from detectron2.modeling.postprocessing import detector_postprocess


class IoUEvaluator(DatasetEvaluator):
    """
    Evaluate instance detection outputs using the IoU metric.
    """

    def __init__(self, dataset_name, cfg, distributed, output_dir=None):
        self.dataset_name = dataset_name
        self.cfg = cfg

        self._tasks = ("bbox",)
        self._distributed = distributed
        self._output_dir = output_dir

        self._cpu_device = torch.device("cpu")
        self._logger = logging.getLogger(__name__)
        self._metadata = MetadataCatalog.get(dataset_name)

    def reset(self):
        self._predictions = []

    def process(self, inputs, outputs):
        """
        Args:
            inputs: the inputs to the model (e.g., GeneralizedSiamContextRCNN).
                It is a list of dict. Each dict corresponds to a video and each frame is a dict that
                contains keys like "height", "width", "file_name", "image_id".
            outputs: the outputs of the model. It is in the format tuple[list[list[dict], context]],
                where the dict has the key "instances" that contains :class:`Instances`.
        """
        outputs = outputs[0] # at position 1 is the last context which is not needed
        # output is post-processed => has to be undone or gt boxes has to be resized to original image size
        for input, output in zip(inputs, outputs):
            prediction = {"video_id": input.get("video_id", ''.join(random.choices(string.ascii_uppercase + string.digits, k=8))), "search": []}

            for t, (frame, out) in enumerate(zip(input["search"], output)):
                assert "instances" in frame, f"missing field 'instances' in input: {list(frame.keys())}"
                if len(frame["instances"]) == 0:
                    self._logger.warn(f"[IoUEvaluator] Frame has no ground truth instance: {prediction['video_id']} | {frame['image_id']}")
                    continue
                assert len(frame["instances"]) == 1, f"for IoU evaluation currently only one ground-truth instance is supported but got {len(frame['instances'])}"
                frame_prediction = {"image_id": frame["image_id"], "time": frame["time"]}
                if "instances" in out:
                    inst = Instances(out["instances"].image_size, **out["instances"].to(self._cpu_device).get_fields())
                    if inst.has("pred_masks"): # pred masks not needed and causes issues when resizing in postprocess
                        inst.remove("pred_masks")
                    frame_prediction["instances"] = detector_postprocess(inst, *frame["image"].size()[1:])
                    frame_prediction["gt_instances"] = frame["instances"].to(self._cpu_device)

                prediction["search"].append(frame_prediction)

            self._predictions.append(prediction)

    def evaluate(self):
        if self._distributed:
            comm.synchronize()
            predictions = comm.gather(self._predictions, dst=0)
            predictions = list(itertools.chain(*predictions))

            if not comm.is_main_process():
                return {}
        else:
            predictions = self._predictions

        if len(predictions) == 0:
            self._logger.warning("[IoUEvaluator] Did not receive valid predictions.")
            return {}

        if self._output_dir:
            os.makedirs(self._output_dir, exist_ok=True)
            file_path = os.path.join(self._output_dir, "instances_predictions.pth")
            with open(self._output_dir, "wb") as f:
                torch.save(predictions, f)

        self._results = OrderedDict()
        if "instances" in predictions[0]["search"][0]: #TODO rework
            self._eval_predictions(predictions)
        else:
            assert False, (f"no instances found in: {predictions[0]['search']}")
        # Copy so the caller can do whatever with results
        return copy.deepcopy(self._results)

    def _eval_predictions(self, predictions):
        """
        Evaluate predictions and fill self._results with the IoU metric.
        """
        # what about ["AP", "AP50", "AP75", "APs", "APm", "APl"]
        self._logger.info("Preparing results for IoU format ...")

        if self._output_dir:
            file_path = os.path.join(self._output_dir, "iou_instances_results.json")
            self._logger.info("Saving results to {}".format(file_path))
            with open(file_path, "w") as f:
                f.write(json.dumps(predictions))
                f.flush()

        self._logger.info("Evaluating predictions ...")
        TOP_K = [5, 10]
        ious_per_prediction = []
        for pred in predictions:
            pred_ious = []
            for frame in pred["search"]:
                if len(frame["instances"]) == 0:
                    pred_ious.append(None)
                else:
                    ious = pairwise_iou(
                        frame["gt_instances"].gt_boxes,
                        frame["instances"].pred_boxes
                    )
                    ious = ious[0] # only one target
                    result_ious = {
                        "best": ious[0].item(), # iou for highest score bbox
                        "all": torch.max(ious).item() # iou for best box, not best score
                    }
                    for k in TOP_K:
                        result_ious[f"top_{k}"] = torch.max(ious[:k]).item()
                    pred_ious.append(result_ious)
            ious_per_prediction.append(pred_ious)

        merged_per_prediction = []
        for ious in ious_per_prediction:
            merged = {
                "best": np.mean([x["best"] if x is not None else 0 for x in ious]) if len(ious) > 0 else 0,
                "all": np.mean([x["all"] if x is not None else 0 for x in ious]) if len(ious) > 0 else 0
            }
            for k in TOP_K:
                merged[f"top_{k}"] = np.mean([x[f"top_{k}"] if x is not None else 0 for x in ious]) if len(ious) > 0 else 0
            merged_per_prediction.append(merged)

        merged_results = {
            "mean_iou/best_score": np.mean([x["best"] for x in merged_per_prediction]) if len(merged_per_prediction) > 0 else 0,
            "mean_iou/top_all": np.mean([x["all"] for x in merged_per_prediction]) if len(merged_per_prediction) > 0 else 0
        }
        for k in TOP_K:
            merged_results[f"mean_iou/top_{k}"] = np.mean([x[f"top_{k}"] for x in merged_per_prediction]) if len(merged_per_prediction) > 0 else 0

        self._results[self._tasks[0]] = merged_results
