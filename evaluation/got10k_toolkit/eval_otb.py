import os
import time
import shutil
import argparse
import json
from datetime import timedelta, datetime
from tqdm import tqdm
import contextlib
import io
from pathlib import Path

from detectron2.config import get_cfg
from got10k.experiments import ExperimentOTB

import utils
from modeling import *  # used for REGISTRY

from evaluation.got10k_toolkit.siam_rcnn_tracker import SiamRCNNTracker


parser = argparse.ArgumentParser(description="Run Got10kToolkit tracker on OTB2015")
parser.add_argument("--config", type=str, help="Path to the detectron2 config file")
parser.add_argument(
    "--model",
    type=str,
    help="Path to the detectron2 model directory or explicit checkpoint file",
    required=True,
)
parser.add_argument(
    "--name", type=str, help="Tracker name for Got10kToolkit", required=True
)
parser.add_argument(
    "--start", type=int, help="Start index of experiment, defaults to 0", default=0
)
parser.add_argument(
    "--end", type=int, help="End index of experiment, defaults to None", default=None
)
parser.add_argument(
    "--force",
    action="store_true",
    help="Clean output directory if tracker with same name found",
)
parser.add_argument("--report", action="store_true", help="Report tracker result")
parser.add_argument("--verbose", action="store_true", help="Verbose output")
parser.add_argument(
    "--opts",
    help="Modify config options using the command-line",
    default=None,
    nargs=argparse.REMAINDER,
)
args = parser.parse_args()

ROOT_DIR = "output/got_toolkit_tracking_data"
RESULT_DIR = os.path.join(ROOT_DIR, "results")
REPORT_DIR = os.path.join(ROOT_DIR, "reports")

RESULT_TRACKER_DIR = os.path.join(RESULT_DIR, "OTB2015", args.name)
REPORT_TRACKER_DIR = os.path.join(REPORT_DIR, "OTB2015", args.name)

if args.force:
    if os.path.exists(RESULT_TRACKER_DIR):
        shutil.rmtree(RESULT_TRACKER_DIR, ignore_errors=True)

    if os.path.exists(REPORT_TRACKER_DIR):
        shutil.rmtree(REPORT_TRACKER_DIR, ignore_errors=True)

if os.path.isfile(args.model):
    MODEL_CHECKPOINT = args.model
    assert args.config is not None and os.path.isfile(
        args.config
    ), "missing or invalid config that is required if --model is a checkpoint file"
    CONFIG = args.config
else:
    with open(os.path.join(args.model, "last_checkpoint"), "r") as f:
        MODEL_CHECKPOINT = os.path.join(args.model, f.readline())
    CONFIG = os.path.join(args.model, "config.yaml")
    assert os.path.isfile(CONFIG), f"missing or invalid config ({CONFIG})"

cfg = get_cfg()
cfg = utils.extend_config(cfg)
cfg.merge_from_file(CONFIG)
if args.opts is not None:
    cfg.merge_from_list(args.opts)
cfg.MODEL.WEIGHTS = MODEL_CHECKPOINT

if args.opts is not None:
    assert len(args.opts) % 2 == 0, "config command-line arguments have to be even"
    print()
    print("using config command-line arguments:")
    for i in range(len(args.opts) // 2):
        print(f"{args.opts[2 * i]}:", args.opts[2 * i + 1])
    print()

print("using model checkpoint:", MODEL_CHECKPOINT)

tracker = SiamRCNNTracker(cfg=cfg, name=args.name)

CONFIG_DIR = os.path.join(RESULT_TRACKER_DIR, "config")
Path(CONFIG_DIR).mkdir(parents=True, exist_ok=True)
with open(os.path.join(CONFIG_DIR, "config.yaml"), "w") as f:
    f.write(cfg.dump())

meta = {
    "time": datetime.now().strftime("%d.%m.%Y %H:%M:%S"),
    "args": {
        "config": args.config,
        "model": args.model,
        "name": args.name,
        "start": args.start,
        "end": args.end,
        "force": args.force,
        "report": args.report,
        "verbose": args.verbose,
        "opts": args.opts,
    },
    "model_checkpoint": MODEL_CHECKPOINT,
    "result_dir": RESULT_TRACKER_DIR,
    "report_dir": REPORT_TRACKER_DIR,
}

with open(os.path.join(CONFIG_DIR, "meta.json"), "w") as f:
    json.dump(meta, f, sort_keys=True, indent=4)

experiment = ExperimentOTB(
    root_dir="/globalwork/datasets/OTB_new/",
    version=2015,
    result_dir=RESULT_DIR,
    report_dir=REPORT_DIR,
    start_idx=args.start,
    end_idx=args.end,
)

print(f"OTB{experiment.dataset.version} has", len(experiment.dataset), "items")
print("this will take a while... get some coffee and relax ☕☕☕")

total = len(experiment.dataset) if args.end is None else args.end
progress = tqdm(total=total, desc="Run tracker")


def callback(state: int, name: str):
    if state == 1:
        progress.update()


tracker.callback = callback

start = time.time()
with contextlib.redirect_stdout(io.StringIO()):
    experiment.run(tracker, visualize=False)
progress.close()

print("it took:", str(timedelta(seconds=time.time() - start)))

if args.report:
    experiment.report([tracker.name])

    with open(os.path.join(REPORT_TRACKER_DIR, "performance.json")) as file:
        data = json.load(file)
        data = data[tracker.name]["overall"]
        print("success score (AUC):", data["success_score"])
        print("precision score:", data["precision_score"])
        print("success rate:", data["success_rate"])
        print("fps:", data["speed_fps"])
