from typing import Any, Callable, Optional
from pathlib import Path

import numpy as np
from detectron2.config import CfgNode
from detectron2.structures import BoxMode
from detectron2.utils.visualizer import Visualizer
from got10k.trackers import Tracker

from predictor.siam_rcnn_predictor import SiamRCNNPredictor


class SiamRCNNTracker(Tracker):
    def __init__(self, cfg: CfgNode, name="SiamRCNNTracker"):
        super().__init__(name=name, is_deterministic=True)
        assert (
            cfg.MODEL.META_ARCHITECTURE == "SiamRCNN"
        ), f"only 'SiamRCNN' meta arch is supported but got '{cfg.MODEL.META_ARCHITECTURE}'"
        self.config = cfg
        self.context = None
        self.prev_box = None
        self.predictor = SiamRCNNPredictor(cfg)
        self.idx = 0

        # color is used in base class Tracker for creating a video. It is missing there!
        self.color = {"gt": (0, 255, 0), "pred": (255, 0, 0)}

        self.callback: Callable[[int, str], Any] = lambda *args: None
        self.video_name: Optional[str] = None

    def init(self, image, box):
        """
        image: PIL.JpegImagePlugin.JpegImageFile (RGB), has to convert to numpy array,
        with channel format like in cfg.INPUT.FORMAT.
        box: list[float], format (x,y,w,h), has to be converted to (x1,y1,x2,y2).
        """
        image = np.array(image)[:, :, ::-1]
        box[2] += box[0]
        box[3] += box[1]
        self.context = self.predictor.init(image, box)
        self.prev_box = box

    def update(self, image):
        """
        image: PIL.JpegImagePlugin.JpegImageFile (RGB), has to convert to numpy array,
        with channel format like in cfg.INPUT.FORMAT.
        returns:
            box: format (x,y,w,h)
        """
        image = np.array(image)[:, :, ::-1]
        predictions, context = self.predictor(image, context=self.context)
        self.context = context

        box = predictions["bbox"]
        _ = predictions["score"]

        box = BoxMode.convert(
            box[np.newaxis], from_mode=BoxMode.XYXY_ABS, to_mode=BoxMode.XYWH_ABS
        )
        box = box[0]
        self.prev_box = box

        # if no predictions are made in this step, then use previous predicted box
        return self.prev_box

    def set_video_name(self, name: str):
        if self.callback is not None:
            if name is None:
                self.callback(1, self.video_name)
                self.video_name = name
            else:
                self.video_name = name
                self.callback(0, self.video_name)
