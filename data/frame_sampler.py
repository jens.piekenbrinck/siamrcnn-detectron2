from abc import ABCMeta, abstractmethod
import random
from typing import Any, Dict, List, Tuple


class FrameSampler(metaclass=ABCMeta):
    def __call__(self, dataset_dict: Dict[str, Any]) -> Dict[str, Any]:
        return self.sample(dataset_dict)

    @abstractmethod
    def sample(
        self, dataset_dict: Dict[str, Any]
    ) -> Tuple[Dict[str, Any], List[Dict[str, Any]]]:
        pass


class SiamVideoSampler(FrameSampler):
    """
    A frame sampler for siamese networks.
    The first frame is the template frame.
    """

    def sample(
        self, dataset_dict: Dict[str, Any]
    ) -> Tuple[Dict[str, Any], List[Dict[str, Any]]]:
        return (dataset_dict["frames"][0], dataset_dict["frames"][1:])


class SiamFrameSampler(FrameSampler):
    """
    A sampler for siamese networks.
    Sample output is a tuple that contains the template frame and a list that contains the search frame.
    A list is returned for search frames to not broke dataloaders and models designed for multiple search frames.
    For SiamRCNN sampling: (max_count=1, max_frame_range=100, sequential=False, allow_template_reuse=True)
    """

    def __init__(
        self,
        max_count: int,
        max_frame_range: int,
        sequential: bool,
        allow_template_reuse: bool,
        distribution: str = "random",
    ):
        """
        Args:
            max_count: maximal amount of search images.
            max_frame_range: maximal distance of search images to template.
            sequential: if template is before search frames.
            allow_template_reuse: if False the search images should not contain the template if possible.
            distribution: how to sample a sequence, "random" or "equal", to ensure equal spacing between frames.
        """

        self.max_count = max_count

        assert (
            max_frame_range >= 0
        ), f"max_frame_range has to be greater than zero but got {max_frame_range}"
        self.max_frame_range = max_frame_range

        self.sequential = sequential

        self.allow_template_reuse = allow_template_reuse

        assert distribution in set(["random", "equal"])
        self.distribution = distribution

    def sample(
        self, dataset_dict: Dict[str, Any]
    ) -> Tuple[Dict[str, Any], List[Dict[str, Any]]]:
        if len(dataset_dict["frames"]) == 1:
            return dataset_dict["frames"][0], dataset_dict["frames"]

        max_idx = len(dataset_dict["frames"]) - 1 - self.max_frame_range
        range_start_idx = random.randint(0, max(0, max_idx))
        frames = dataset_dict["frames"][
            range_start_idx : range_start_idx + self.max_frame_range + 1
        ]

        if self.sequential:
            if self.distribution == "equal":
                step_size = max(1, (len(frames) - 1) // self.max_count)
                search = frames[step_size::step_size][: self.max_count]
                template = frames[0]
            else:
                search_start_idx = random.randint(
                    1, max(1, len(frames) - 1 - self.max_count)
                )
                count = len(frames[search_start_idx:])
                if count < self.max_count:
                    search_start_idx = 0
                    count = len(frames)
                search_indices = sorted(
                    random.sample(range(count), min(count, self.max_count))
                )
                search = [frames[search_start_idx + i] for i in search_indices]
                template_end_idx = search_start_idx + min(search_indices)
                template_idx = random.randint(
                    0,
                    max(0, template_end_idx - 1)
                    if not self.allow_template_reuse
                    else template_end_idx,
                )
                template = frames[template_idx]
        else:
            search_start_idx = random.randint(
                0, max(0, len(frames) - 1 - self.max_count)
            )
            count = len(frames[search_start_idx:])
            search_indices = sorted(
                random.sample(range(count), min(count, self.max_count))
            )
            search = [frames[search_start_idx + i] for i in search_indices]
            if not self.allow_template_reuse:
                free_indices = list(set(range(len(frames))) - set(search_indices))
                if len(free_indices) > 0:
                    template_idx = random.choice(free_indices)
                else:
                    # no free indices => select template at random
                    template_idx = random.choice(search_indices)
            else:
                template_idx = random.randint(0, len(frames) - 1)
            template = frames[template_idx]

        return template, search


if __name__ == "__main__":
    frames = [{"time": i} for i in range(100)]
    dataset_dict = {"frames": frames}

    sampler = SiamFrameSampler(
        max_count=4,
        max_frame_range=60,
        sequential=True,
        allow_template_reuse=False,
        distribution="equal",
    )
    for i in range(3):
        if i > 0:
            print("-" * 100)
        print(sampler.sample(dataset_dict))
