import copy
import torch
import numpy as np
from PIL import Image
from fvcore.common.file_io import PathManager

from detectron2.config import CfgNode
from detectron2.data import DatasetCatalog
from detectron2.data import detection_utils as utils
from detectron2.data import transforms as T

from data.augmentations import RandomGrayscale, RandomMotionBlur


class SiamDatasetMapper:
    def __init__(self, cfg: CfgNode, is_train=True, keep_annotations=False):
        # copied from the default detectron2 DatasetMapper but without cropping

        shared_tfm_gens = []
        if is_train:
            shared_tfm_gens = [
                RandomMotionBlur(fraction=0.25, kernel_size_range=(5, 15)),
                RandomGrayscale(fraction=0.25),
            ]
        else:
            shared_tfm_gens = []

        self.tfm_gens = {
            # negative
            False: {
                "shared": shared_tfm_gens
                + utils.build_transform_gen(cfg, is_train),  # resize images in same way
                "template": [],
                "search": [],
            },
            True: {
                "shared": shared_tfm_gens,
                "template": utils.build_transform_gen(
                    cfg, is_train
                ),  # needed for resize
                "search": utils.build_transform_gen(cfg, is_train),  # needed for resize
            },
        }

        # fmt: off
        self.img_format = cfg.INPUT.FORMAT
        self.mask_on = cfg.MODEL.MASK_ON
        self.mask_format = cfg.INPUT.MASK_FORMAT
        self.keypoint_on = cfg.MODEL.KEYPOINT_ON
        self.load_proposals = cfg.MODEL.LOAD_PROPOSALS
        # fmt: on
        if self.keypoint_on and is_train:
            # Flip only makes sense in training
            self.keypoint_hflip_indices = utils.create_keypoint_hflip_indices(
                cfg.DATASETS.TRAIN
            )
        else:
            self.keypoint_hflip_indices = None

        if self.load_proposals:
            self.min_box_side_len = cfg.MODEL.PROPOSAL_GENERATOR.MIN_SIZE
            self.proposal_topk = (
                cfg.DATASETS.PRECOMPUTED_PROPOSAL_TOPK_TRAIN
                if is_train
                else cfg.DATASETS.PRECOMPUTED_PROPOSAL_TOPK_TEST
            )
        self.is_train = is_train
        self.keep_annotations = keep_annotations

    def __call__(self, dataset_dict):
        """
        Args:
            dataset_dict (dict): Video dict that contains the tracks, in Detectron2 dataset format.

        Returns:
            dict: a format that recurrent models accept
        """

        dataset_dict = copy.deepcopy(dataset_dict)  # it will be modified by code below

        def _map(
            frame: dict,
            is_template: bool,
            negative: bool,
            shared_transforms,
            other_transforms,
        ):
            # USER: Write your own image loading if it's not from a file
            image = utils.read_image(frame["file_name"], format=self.img_format)
            utils.check_image_size(frame, image)

            if shared_transforms is None:
                image, sh_transforms = T.apply_transform_gens(
                    self.tfm_gens[negative]["shared"], image
                )
                shared_transforms = sh_transforms.transforms
            else:
                image, sh_transforms = T.apply_transform_gens(shared_transforms, image)
                shared_transforms = sh_transforms.transforms
            if (is_template and template_transforms is None) or (
                not is_template and search_transforms is None
            ):
                image, transforms = T.apply_transform_gens(
                    self.tfm_gens[negative]["template" if is_template else "search"],
                    image,
                )
                other_transforms = transforms.transforms
            else:
                image, transforms = T.apply_transform_gens(other_transforms, image)
            transforms = sh_transforms + transforms

            image_shape = image.shape[:2]  # h, w

            # Pytorch's dataloader is efficient on torch.Tensor due to shared-memory,
            # but not efficient on large generic data structures due to the use of pickle & mp.Queue.
            # Therefore it's important to use torch.Tensor.
            frame["image"] = torch.as_tensor(
                np.ascontiguousarray(image.transpose(2, 0, 1))
            )

            # USER: Remove if you don't use pre-computed proposals.
            if self.load_proposals:
                utils.transform_proposals(
                    frame,
                    image_shape,
                    transforms,
                    self.min_box_side_len,
                    self.proposal_topk,
                )

            # always populate ground truth data for template frame
            if not self.is_train and not is_template and not self.keep_annotations:
                # USER: Modify this if you want to keep them for some reason.
                frame.pop("annotations", None)
                frame.pop("sem_seg_file_name", None)
                return frame

            if "annotations" in frame:
                # USER: Modify this if you want to keep them for some reason.
                for anno in frame["annotations"]:
                    if not self.mask_on:
                        anno.pop("segmentation", None)
                    if not self.keypoint_on:
                        anno.pop("keypoints", None)
                    # IMPORTANT: NUM_CLASSES = 1 => only one category id
                    anno["category_id"] = 0

                # USER: Implement additional transformations if you have other types of data
                annos = [
                    utils.transform_instance_annotations(
                        obj,
                        transforms,
                        image_shape,
                        keypoint_hflip_indices=self.keypoint_hflip_indices,
                    )
                    for obj in frame.pop("annotations")
                    if obj.get("iscrowd", 0) == 0
                ]
                instances = utils.annotations_to_instances(
                    annos, image_shape, mask_format=self.mask_format
                )

                frame["instances"] = utils.filter_empty_instances(instances)
                if len(frame["instances"]) == 0:
                    debug = {
                        "is_template": is_template,
                        "negative": negative,
                        "image_shape": image_shape,
                        "instances": instances,
                    }
                    debug_frame = {
                        k: v.shape
                        if v is not None and isinstance(v, torch.Tensor)
                        else v
                        for k, v in frame.items()
                    }
                    assert False, ("empty instances", debug_frame, debug)

            # USER: Remove if you don't do semantic/panoptic segmentation.
            if "sem_seg_file_name" in frame:
                with PathManager.open(frame.pop("sem_seg_file_name"), "rb") as f:
                    sem_seg_gt = Image.open(f)
                    sem_seg_gt = np.asarray(sem_seg_gt, dtype="uint8")
                sem_seg_gt = transforms.apply_segmentation(sem_seg_gt)
                sem_seg_gt = torch.as_tensor(sem_seg_gt.astype("long"))
                frame["sem_seg"] = sem_seg_gt

            return frame, other_transforms, shared_transforms

        shared_transforms = None
        template_transforms = None
        template_frame, template_transforms, shared_transforms = _map(
            dataset_dict["template"],
            is_template=True,
            negative=dataset_dict["negative"],
            shared_transforms=shared_transforms,
            other_transforms=template_transforms,
        )
        dataset_dict["template"] = template_frame

        search_transforms = None
        search_frames = []
        for frame in dataset_dict["search"]:
            search_frame, search_transforms, shared_transforms = _map(
                frame,
                is_template=False,
                negative=dataset_dict["negative"],
                shared_transforms=shared_transforms,
                other_transforms=search_transforms,
            )
            search_frames.append(search_frame)
        dataset_dict["search"] = search_frames

        if self.is_train:
            if not dataset_dict["negative"]:
                all_frames = [dataset_dict["template"]] + dataset_dict["search"]
            else:
                all_frames = dataset_dict["search"]
            frame_sizes = list(map(lambda x: x["instances"].image_size, all_frames))
            if frame_sizes.count(frame_sizes[0]) != len(all_frames):
                print(
                    f"all frames have to be the same size,"
                    f" disable random resizing or apply the same"
                    f" transformation to all frames: {frame_sizes} | negative: {dataset_dict['negative']}"
                )
                print(template_transforms)
                print(search_transforms, dataset_dict["negative"])
                assert False

        return dataset_dict


if __name__ == "__main__":
    from pathlib import Path
    import argparse

    from detectron2.config import get_cfg
    from detectron2.utils.visualizer import Visualizer, ColorMode

    from data.datasets.videos import ImageNetVIDVideoDataset, YouTubeVOSVideoDataset
    from data import SiamLimitedTrackFilter
    from data.datasets.build import build_detection_train_loader
    from engine import build_dataset_config, build_dataset
    from tqdm import tqdm

    from utils import extend_config

    parser = argparse.ArgumentParser(
        description="Test SiamDatasetMapper on ImageNetVID dataset"
    )
    parser.add_argument("--config", type=str, help="Path to the config")
    args = parser.parse_args()

    dataset_filter = SiamLimitedTrackFilter(min_length=5, min_time_step=1)
    ImageNetVIDVideoDataset.register(
        dataset_filter=dataset_filter, subsets=set(["train"])
    )
    YouTubeVOSVideoDataset.register(
        dataset_filter=dataset_filter, subsets=set(["train"])
    )

    cfg = get_cfg()
    cfg = extend_config(cfg)
    if args.config is not None:
        cfg.merge_from_file(args.config)
    cfg.DATASETS.TRAIN = ["imagenet_vid_video_train"]
    cfg.SOLVER.IMS_PER_BATCH = 1
    cfg.DATALOADER.NUM_WORKERS = 0

    dataset_config = build_dataset_config(
        cfg=cfg, dataset_names=cfg.DATASETS.TRAIN, is_train=True
    )
    mapper = SiamDatasetMapper(cfg=cfg, is_train=True)
    dataset = build_dataset(dataset_config=dataset_config)
    data_loader = build_detection_train_loader(cfg=cfg, dataset=dataset, mapper=mapper)
    print("data loader:", data_loader)
    dataset_size = len(DatasetCatalog.get(cfg.DATASETS.TRAIN[0]))
    output_dir = Path("output/vis/data/mapper/siam_dataset_mapper")
    output_dir.mkdir(parents=True, exist_ok=True)

    VIS_ITEMS = 5  # dataset_size//cfg.SOLVER.IMS_PER_BATCH
    for i, data in tqdm(enumerate(data_loader), total=VIS_ITEMS):
        if i == VIS_ITEMS:
            break
        image = data[0]["template"]["image"].numpy().transpose(1, 2, 0)
        visualizer = Visualizer(
            image[:, :, ::-1],
            metadata=None,
            scale=0.5,
            instance_mode=ColorMode.SEGMENTATION,
        )
        vis = visualizer.overlay_instances(
            boxes=data[0]["template"]["instances"].gt_boxes.tensor
        )
        vis.save(str(output_dir / f"/template_{i}.png"))

        image = data[0]["search"][0]["image"].numpy().transpose(1, 2, 0)
        visualizer = Visualizer(
            image[:, :, ::-1],
            metadata=None,
            scale=0.5,
            instance_mode=ColorMode.SEGMENTATION,
        )
        vis = visualizer.overlay_instances(
            boxes=data[0]["search"][0]["instances"].gt_boxes.tensor
        )
        vis.save(str(output_dir / f"search_{i}.png"))
