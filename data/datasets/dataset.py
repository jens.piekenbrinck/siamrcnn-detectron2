from typing import List
import numpy as np
import torch.utils.data as data

from data.datasets.subdataset import SiamRCNNSubDataset


class SiamRCNNDataset(data.Dataset):
    def __init__(
        self,
        subdatasets: List[SiamRCNNSubDataset],
        num_use: int = 0,
        repeat: int = 1,
        negative: float = 0.2,
    ):
        self.subdatasets = subdatasets
        self.length = sum([len(d) for d in self.subdatasets])
        self.ranges = [0] + list(np.cumsum([len(d) for d in self.subdatasets]))
        self.negative = negative
        self.length_use = num_use if num_use > 0 else self.length
        self.length_use *= max(1, repeat)
        self.indices = self.shuffle(self.length_use)

    def __len__(self):
        return self.length_use

    def shuffle(self, num):
        indices = []
        while len(indices) < num:
            p = list(range(0, self.length))
            np.random.shuffle(p)
            indices += p
        return indices[:num]

    def _find_subdataset(self, index):
        for i in range(len(self.ranges[:-1])):
            if index in range(self.ranges[i], self.ranges[i + 1]):
                return self.subdatasets[i], index - self.ranges[i]
        assert False, f"invalid index {index} for ranges {self.ranges}"

    def __getitem__(self, index):
        index = self.indices[index]
        dataset, index = self._find_subdataset(index)
        neg = self.negative and self.negative > np.random.random_sample()

        if neg:
            template, _, _ = dataset.sample(index)
            _, search, _ = np.random.choice(self.subdatasets).sample()
        else:
            template, search, _ = dataset.sample(index)

        return {"template": template, "search": search, "negative": neg}


if __name__ == "__main__":
    import os
    import pickle
    from data import SiamFrameSampler

    # cache datasets before calling this function!!!
    dataset_names = [
        "imagenet_vid_video_train",
        "imagenet_det_video_train",
        "coco_video_2017_train",
        "youtube_bb_video_train",
        "youtube_vos_video_train",
        "got10k_video_train",
    ]
    datasets = [
        (
            name,
            pickle.load(
                open(os.path.join("cache/datasets/siam_rcnn_pp", f"{name}.pkl"), "rb")
            )["videos"],
        )
        for name in dataset_names
    ]

    print("loaded dataset_dicts")

    sampler = SiamFrameSampler(max_count=3, max_frame_range=30)
    sub_datasets = [
        SiamRCNNSubDataset(
            name,
            dicts,
            sampler=sampler,
            repeat=25 if name.startswith("imagenet_vid") else 1,
        )
        for name, dicts in datasets
    ]

    dataset = SiamRCNNDataset(sub_datasets, num_use=600_000, repeat=30, negative=0.2)

    print(
        "Subdataset sizes:",
        [len(d) for d in sub_datasets],
        sum([len(d) for d in sub_datasets]),
    )
    print("Total:", len(dataset))

    for index in np.random.randint(len(dataset), size=20):
        try:
            d, i = dataset._find_subdataset(dataset.indices[index])
            # print(index, "->", d.name, "->", i)
            dataset[i]  # test indexing
        except Exception as e:
            print(index)
            raise e

    for i, data in enumerate(dataset):
        template = data["template"]
        search = data["search"]
        if i % 1000 == 0:
            print(
                template["image_id"],
                template["time"],
                "|",
                len(search),
                [(s["image_id"], s["time"]) for s in search],
                "|",
                data["negative"],
            )
        if i >= 10_000:
            break
