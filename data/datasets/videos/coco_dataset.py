"""
COCO dataset implementation
"""

import os
import platform
import getpass
from typing import List, Any, Optional
import contextlib
import io
from pycocotools.coco import COCO
import pycocotools.mask as mask_util

from detectron2.structures import BoxMode
from detectron2.data import DatasetCatalog, MetadataCatalog
from detectron2.utils.colormap import random_color

from data.datasets.videos.video_dataset import VideoDataset
from data.dataset_filter import NoOpFilter


class COCOVideoDataset(VideoDataset):
    def __init__(
        self, subset: str, version: str, cache_root: str = "./cache/datasets/videos"
    ):
        super().__init__()

        assert subset in ["train", "val"], (
            f"invalid subset {subset}," " only [train, val] is supported by COCO"
        )
        assert version in ["2014", "2017"], (
            f"invalid version {version}," " only [2014, 2017] is supported by COCO"
        )
        self.subset = subset
        self.version = version
        self.cache_root = cache_root
        self.tqdm_desc = f"coco_video_{version}_{subset} loading"

        is_claix = "hpc.itc.rwth-aachen.de" in platform.node()
        root = f"/work/{getpass.getuser()}/globalwork" if is_claix else "/globalwork"
        self.dataset_root = os.path.join(root, "datasets/coco/")

        super().init()

    def get_cache_path(self) -> str:
        return os.path.join(
            self.cache_root, f"coco_video_{self.version}_{self.subset}_cache.pkl"
        )

    def get_tqdm_desc(self) -> Optional[str]:
        return self.tqdm_desc

    def get_job_inputs(self) -> List[Any]:
        json_file = os.path.join(
            self.dataset_root, f"annotations/instances_{self.subset}{self.version}.json"
        )
        with contextlib.redirect_stdout(io.StringIO()):
            coco_api = COCO(json_file)
        img_ids = sorted(coco_api.imgs.keys())
        imgs = coco_api.loadImgs(img_ids)
        anns = [coco_api.imgToAnns[img_id] for img_id in img_ids]
        imgs_anns = list(zip(imgs, anns))
        return imgs_anns

    def _load_frames(self, img_dict, anno_dict_list) -> List[dict]:
        """
        Load frames and annotations from video
        """
        assert self.metadata is not None
        image_root = os.path.join(self.dataset_root, f"{self.subset}{self.version}")
        ann_keys = ["iscrowd", "bbox", "category_id"]

        frames = []

        record = {}
        record["file_name"] = os.path.join(image_root, img_dict["file_name"])
        record["height"] = img_dict["height"]
        record["width"] = img_dict["width"]
        image_id = record["image_id"] = img_dict["id"]
        record["time"] = 0
        objs = []
        for i, anno in enumerate(anno_dict_list):
            assert anno["image_id"] == image_id
            assert (
                anno.get("ignore", 0) == 0
            ), '"ignore" in COCO json file is not supported.'
            obj = {key: anno[key] for key in ann_keys if key in anno}
            obj["obj_id"] = i
            obj["bbox_mode"] = BoxMode.XYWH_ABS
            obj["category_id"] = self.metadata["thing_dataset_id_to_contiguous_id"][
                obj["category_id"]
            ]

            segm = anno.get("segmentation", None)
            if segm is not None:  # either list[list[float]] or dict(RLE)
                if isinstance(segm, dict):
                    if isinstance(segm["counts"], list):
                        # convert to compressed RLE
                        segm = mask_util.frPyObjects(segm, *segm["size"])
                else:
                    # filter out invalid polygons (< 3 points)
                    segm = [
                        poly for poly in segm if len(poly) % 2 == 0 and len(poly) >= 6
                    ]
                    if len(segm) == 0:
                        # num_instances_without_valid_segmentation += 1
                        continue  # ignore this instance
                obj["segmentation"] = segm

            objs.append(obj)

        record["annotations"] = objs
        frames.append(record)

        return frames

    def load(self, x) -> dict:
        """
        Creating the video dict based on the {video_job}.
        This is called by a multithreading executor.
        """

        img_dict = x[0]
        anno_dict_list = x[1]
        image_root = os.path.join(self.dataset_root, f"{self.subset}{self.version}")
        frames = self._load_frames(img_dict, anno_dict_list)
        video = {
            "file_name": os.path.join(image_root, img_dict["file_name"]),
            "id": img_dict["id"],
            "height": img_dict["height"],
            "width": img_dict["width"],
            "frames": frames,
        }

        return video

    def load_metadata(self) -> dict:
        json_file = os.path.join(
            self.dataset_root, f"annotations/instances_{self.subset}{self.version}.json"
        )
        with contextlib.redirect_stdout(io.StringIO()):
            coco_api = COCO(json_file)

        cat_ids = sorted(coco_api.getCatIds())
        cats = coco_api.loadCats(cat_ids)

        thing_classes = [c["name"] for c in sorted(cats, key=lambda x: x["id"])]
        id_map = {v: i for i, v in enumerate(cat_ids)}
        return {
            "thing_classes": thing_classes,
            "thing_colors": [random_color(rgb=True) for _ in thing_classes],
            "thing_dataset_id_to_contiguous_id": id_map,
        }

    @staticmethod
    def register(
        dataset_filter=NoOpFilter(),
        subsets=set(["train", "val"]),
        versions=set(["2014", "2017"]),
        cache_root: str = "./cache/datasets/videos",
    ):
        assert isinstance(subsets, set), "subsets parameter has to be a set"
        valid_subsets = set(["train", "val"])
        assert subsets.issubset(
            valid_subsets
        ), f"subsets {subsets - valid_subsets} not found in {valid_subsets}"

        assert isinstance(versions, set), "subsets parameter has to be a set"
        valid_versions = set(["2014", "2017"])
        assert versions.issubset(
            valid_versions
        ), f"versions {versions - valid_versions} not found in {valid_versions}"

        for v in versions:
            for d in subsets:
                dataset_name = f"coco_video_{v}_{d}"
                dataset = COCOVideoDataset(subset=d, version=v, cache_root=cache_root)
                DatasetCatalog.register(
                    dataset_name, dataset.get_dataset_dicts(dataset_filter)
                )
                MetadataCatalog.get(dataset_name).set(**dataset.metadata)


if __name__ == "__main__":
    from data.datasets.videos.utils import video_dataset_stats
    from detectron2.data import DatasetCatalog, MetadataCatalog

    COCOVideoDataset.register()

    for v in ["2014", "2017"]:
        for d in ["train", "val"]:
            dataset_name = f"coco_video_{v}_{d}"
            video_dataset_stats(
                dataset_name=dataset_name,
                vis_output=f"output/vis/datasets/videos/{dataset_name}",
            )

            dataset: List[dict] = DatasetCatalog.get(dataset_name)
            dataset_metadata = MetadataCatalog.get(dataset_name)
            masks = 0
            for video in dataset:
                for frame in video["frames"]:
                    for anno in frame["annotations"]:
                        if "segmentation" in anno:
                            masks += 1
            print(dataset_name, "has", masks, "masks")
