from .video_dataset import VideoDataset
from .got10k_dataset import GOT10kVideoDataset
from .imagenet_vid_dataset import ImageNetVIDVideoDataset
from .imagenet_det_dataset import ImageNetDETVideoDataset
from .lasot_dataset import LaSOTVideoDataset
from .otb_dataset import OTBVideoDataset
from .youtube_vos_dataset import YouTubeVOSVideoDataset
from .youtube_bb_dataset import YouTubeBBVideoDataset
from .coco_dataset import COCOVideoDataset
from .tao_dataset import TAOVideoDataset
