import os
import multiprocessing
import concurrent.futures
import pickle
from typing import List, Any, Optional, Callable
from abc import ABCMeta, abstractmethod
from tqdm import tqdm

from data.datasets.videos.utils import extend_video_with_tracks


class VideoDataset(metaclass=ABCMeta):
    def __init__(self, num_workers=multiprocessing.cpu_count()):
        self.num_workers = num_workers
        self.metadata = None
        self.dataset_dicts = None

    @abstractmethod
    def get_cache_path(self) -> str:
        pass

    def get_tqdm_desc(self) -> Optional[str]:
        return None

    @abstractmethod
    def get_job_inputs(self) -> List[Any]:
        pass

    @abstractmethod
    def load(self, x) -> dict:
        pass

    def load_videos(self) -> List[dict]:
        jobs = self.get_job_inputs()
        videos = []
        with concurrent.futures.ThreadPoolExecutor(
            max_workers=self.num_workers
        ) as executor:
            videos = list(
                tqdm(
                    executor.map(self.load, jobs),
                    total=len(jobs),
                    # ncols=100,
                    desc=self.get_tqdm_desc(),
                )
            )
            print(self.get_tqdm_desc(), "->", "extending videos with tracks")
            videos = [
                extend_video_with_tracks(video)
                for video in tqdm(videos, total=len(videos), desc=self.get_tqdm_desc())
            ]
        return videos

    @abstractmethod
    def load_metadata(self) -> dict:
        pass

    def init(self):
        cache_path = self.get_cache_path()
        if cache_path is not None and os.path.exists(cache_path):
            result = pickle.load(open(cache_path, "rb"))
            self.dataset_dicts = result["videos"]
            self.metadata = result["metadata"]
        else:
            self.metadata = self.load_metadata()

    def get_dataset_dicts(
        self, callback: Callable[[List[dict]], List[dict]]
    ) -> Callable[[], List[dict]]:
        def load():
            if self.dataset_dicts is None:
                cache_path = self.get_cache_path()
                if cache_path is not None and os.path.exists(cache_path):
                    result = pickle.load(open(cache_path, "rb"))
                    self.dataset_dicts = result["videos"]
                else:
                    self.dataset_dicts = self.load_videos()
                    if cache_path is not None:
                        dirname = os.path.dirname(cache_path)
                        if dirname != "" and dirname != ".":
                            os.makedirs(dirname, exist_ok=True)
                        pickle.dump(
                            {"videos": self.dataset_dicts, "metadata": self.metadata},
                            open(cache_path, "wb"),
                        )
            return callback(self.dataset_dicts)

        return load
