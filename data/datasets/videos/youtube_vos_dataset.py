"""
YouTube VOS dataset implementation
"""

import os
import platform
import getpass
from typing import List, Any, Optional
import json
import glob
import numpy as np
import cv2
import PIL
import pycocotools

from detectron2.structures import BoxMode
from detectron2.data import DatasetCatalog, MetadataCatalog
from detectron2.utils.colormap import random_color

from data.datasets.videos.video_dataset import VideoDataset
from data.dataset_filter import NoOpFilter


class YouTubeVOSVideoDataset(VideoDataset):
    def __init__(self, subset: str, cache_root: str = "./cache/datasets/videos"):
        super().__init__()

        assert subset in ["train"], (
            f"invalid subset {subset}," " only [train] is supported by YouTubeVOS"
        )
        self.subset = subset
        self.cache_root = cache_root
        self.tqdm_desc = f"youtube_vos_video_{subset} loading"

        is_claix = "hpc.itc.rwth-aachen.de" in platform.node()
        root = f"/work/{getpass.getuser()}/globalwork" if is_claix else "/globalwork"
        self.dataset_root = os.path.join(root, "datasets/youtube-vos/")

        super().init()

    def get_cache_path(self) -> str:
        return os.path.join(
            self.cache_root, f"youtube_vos_video_{self.subset}_cache.pkl"
        )

    def get_tqdm_desc(self) -> Optional[str]:
        return self.tqdm_desc

    def get_job_inputs(self) -> List[Any]:
        jobs = []
        with open(
            os.path.join(
                self.dataset_root,
                "valid" if self.subset == "val" else "train",
                "meta.json",
            )
        ) as file:
            for video_id, video in json.load(file)["videos"].items():
                jobs.append((video_id, video))
        return jobs

    @staticmethod
    def get_bbox_from_segmentation_mask_np(mask):
        object_locations = (np.stack(np.where(np.equal(mask, 1))).T[:, :2]).astype(
            np.int32
        )
        y0 = np.min(object_locations[:, 0])
        x0 = np.min(object_locations[:, 1])
        y1 = np.max(object_locations[:, 0]) + 1
        x1 = np.max(object_locations[:, 1]) + 1
        return np.stack([x0, y0, x1, y1]).astype(dtype=np.float64)

    def _load_frames(self, video_id, video) -> List[dict]:
        """
        Load frames and annotations from video
        """
        assert self.metadata is not None
        frame_path = os.path.join(
            self.dataset_root,
            "valid" if self.subset == "val" else "train",
            "JPEGImages",
            video_id,
        )
        anno_path = os.path.join(
            self.dataset_root,
            "valid" if self.subset == "val" else "train",
            "Annotations" if self.subset == "val" else "CleanedAnnotations",
            video_id,
        )

        image_files = sorted(glob.glob(os.path.join(frame_path, "*.jpg")))
        annotation_files = sorted(glob.glob(os.path.join(anno_path, "*.png")))

        frames = []
        for t, (image_file, annotation_file) in enumerate(
            zip(image_files, annotation_files)
        ):
            image_id = image_file.split("/")[-1].replace(".jpg", "")
            masks = np.array(PIL.Image.open(annotation_file))
            obj_ids = set(np.setdiff1d(np.unique(masks), [0]))

            record = {}

            record["file_name"] = image_file
            record["image_id"] = image_id
            record["time"] = t
            objs = []

            for obj_id in obj_ids:
                if video["objects"].get(str(obj_id), None) is None:
                    continue
                mask = masks == obj_id
                box = self.get_bbox_from_segmentation_mask_np(mask)
                obj = {
                    "obj_id": obj_id,
                    "bbox": box.tolist(),
                    "bbox_mode": BoxMode.XYXY_ABS,
                    "category_id": self.metadata["thing_classes"].index(
                        video["objects"][str(obj_id)]["category"]
                    ),
                    "segmentation": pycocotools.mask.encode(
                        np.asarray(mask, order="F")
                    ),  # bitmask
                }
                objs.append(obj)

            record["annotations"] = objs
            frames.append(record)

        return frames

    def load(self, x) -> dict:
        """
        Creating the video dict based on the {video_job}.
        This is called by a multithreading executor.
        """

        frame_path = os.path.join(
            self.dataset_root,
            "valid" if self.subset == "val" else "train",
            "JPEGImages",
        )
        video_id = x[0]
        video = x[1]
        first_frame_path = sorted(
            glob.glob(os.path.join(frame_path, video_id, "*.jpg"))
        )[0]
        height, width = cv2.imread(first_frame_path).shape[:2]
        frames = self._load_frames(video_id, video)
        video = {
            "file_name": os.path.join(frame_path, video_id),
            "id": video_id,
            "height": height,
            "width": width,
            "frames": frames,
        }

        return video

    def load_metadata(self) -> dict:
        dataset_path = os.path.join(
            self.dataset_root, "valid" if self.subset == "val" else "train", "meta.json"
        )
        categories = set()
        with open(dataset_path) as file:
            for video in json.load(file)["videos"].values():
                for obj in video["objects"].values():
                    categories.add(obj["category"])
        return {
            "thing_classes": list(categories),
            "thing_colors": [random_color(rgb=True) for _ in categories],
            "thing_dataset_id_to_contiguous_id": {k: k for k in range(len(categories))},
        }

    @staticmethod
    def register(
        dataset_filter=NoOpFilter(),
        subsets=set(["train"]),
        cache_root: str = "./cache/datasets/videos",
    ):
        assert isinstance(subsets, set), "subsets parameter has to be a set"
        valid_subsets = set(["train"])
        assert subsets.issubset(
            valid_subsets
        ), f"subsets {subsets - valid_subsets} not found in {valid_subsets}"
        for d in subsets:
            dataset_name = f"youtube_vos_video_{d}"
            dataset = YouTubeVOSVideoDataset(subset=d, cache_root=cache_root)
            DatasetCatalog.register(
                dataset_name, dataset.get_dataset_dicts(dataset_filter)
            )
            MetadataCatalog.get(dataset_name).set(**dataset.metadata)


if __name__ == "__main__":
    from data.datasets.videos.utils import video_dataset_stats

    YouTubeVOSVideoDataset.register()

    for d in ["train"]:
        dataset_name = f"youtube_vos_video_{d}"
        video_dataset_stats(
            dataset_name=dataset_name,
            vis_output=f"output/vis/datasets/videos/{dataset_name}",
            video=True,
        )
