"""
ImageNet VID dataset implementation
"""

import os
import platform
import getpass
from typing import List, Any, Optional
import glob
import xml.etree.ElementTree as ET
import math
import concurrent.futures
import cv2
from tqdm import tqdm

from detectron2.structures import BoxMode
from detectron2.data import DatasetCatalog, MetadataCatalog
from detectron2.utils.colormap import random_color

from data.datasets.videos.video_dataset import VideoDataset
from data.dataset_filter import NoOpFilter


def _parse_words(xmls):
    _words = set([])
    for xml in xmls:
        xmltree = ET.parse(xml)
        for object_iter in xmltree.findall("object"):
            name = object_iter.find("name").text
            _words.add(name)
    return _words


class ImageNetVIDVideoDataset(VideoDataset):
    def __init__(self, subset: str, cache_root: str = "./cache/datasets/videos"):
        super().__init__()

        assert subset in ["train", "val"], (
            f"invalid subset {subset},"
            " only [train, val] is supported by ImageNet VID"
        )
        self.subset = subset
        self.cache_root = cache_root
        self.tqdm_desc = f"imagenet_vid_video_{subset} loading"

        is_claix = "hpc.itc.rwth-aachen.de" in platform.node()
        root = f"/work/{getpass.getuser()}/globalwork" if is_claix else "/globalwork"
        self.dataset_root = os.path.join(root, "data/ILSVRC_VID/ILSVRC/")
        self.subsets = [
            "ILSVRC2015_VID_train_0000",
            "ILSVRC2015_VID_train_0001",
            "ILSVRC2015_VID_train_0002",
            "ILSVRC2015_VID_train_0003",
            "ILSVRC2017_VID_train_0000",
        ]

        super().init()

    def get_cache_path(self) -> str:
        return os.path.join(
            self.cache_root, f"imagenet_vid_video_{self.subset}_cache.pkl"
        )

    def get_tqdm_desc(self) -> Optional[str]:
        return self.tqdm_desc

    def get_job_inputs(self) -> List[Any]:
        videos = []
        if self.subset == "train":
            for s in self.subsets:
                vids = glob.glob(
                    os.path.join(
                        self.dataset_root, f"Annotations/VID/{self.subset}/{s}/", "*"
                    )
                )
                videos.extend([(s, v) for v in vids])
        else:
            vids = glob.glob(
                os.path.join(self.dataset_root, f"Annotations/VID/{self.subset}/", "*")
            )
            videos.extend([(None, v) for v in vids])
        return videos

    def _load_frames(self, frame_annos) -> List[dict]:
        """
        Load frames and annotations from video given by {frame_annos}
        """
        frames = []
        for time, xml in enumerate(frame_annos):
            record = {}

            file_name = xml.replace("Annotations", "Data").replace("xml", "JPEG")

            record["file_name"] = file_name
            record["image_id"] = xml.split("/")[-1].split(".")[0]
            record["time"] = time

            objs = []
            if os.path.exists(xml):
                xmltree = ET.parse(xml)
                for object_iter in xmltree.findall("object"):
                    obj_id = int(object_iter.find("trackid").text)
                    name = object_iter.find("name").text
                    box_elem = object_iter.find("bndbox")
                    box = [
                        float(box_elem.find(x).text)
                        for x in ["xmin", "ymin", "xmax", "ymax"]
                    ]
                    objs.append(
                        {
                            "obj_id": obj_id,
                            "bbox": box,
                            "bbox_mode": BoxMode.XYXY_ABS,
                            "category_id": self.metadata["thing_classes"].index(
                                self.metadata["word_to_category"][name]
                            ),
                            "occluded": int(object_iter.find("occluded").text),
                        }
                    )
            record["annotations"] = objs
            frames.append(record)

        return frames

    def load(self, x) -> dict:
        """
        Creating the video dict based on the {video_job}.
        This is called by a multithreading executor.
        """
        video_anno_path = x[1]
        frame_annos = sorted(glob.glob(os.path.join(video_anno_path, "*.xml")))

        frame_path = (
            frame_annos[0].replace("Annotations", "Data").replace("xml", "JPEG")
        )
        height, width = cv2.imread(frame_path).shape[:2]
        frames = self._load_frames(frame_annos)

        if self.subset == "train":
            video_id = os.path.join(
                x[0], video_anno_path.split("/")[-1]
            )  # each .xml annotation is a video
        else:
            video_id = video_anno_path.split("/")[-1]

        video = {
            "file_name": video_anno_path.replace("Annotations", "Data"),
            "id": video_id,
            "height": height,
            "width": width,
            "frames": frames,
        }

        return video

    def load_metadata(self) -> dict:
        word_map = {}
        with open(os.path.join(self.dataset_root, "words.txt"), "r") as file:
            for line in file:
                word, categories = line.strip().split("\t")
                word_map[word] = categories.split(", ")

        if self.subset == "train":
            words = set([])
            with concurrent.futures.ProcessPoolExecutor(8) as executor:
                for s in self.subsets:
                    xmls = glob.glob(
                        os.path.join(
                            self.dataset_root,
                            f"Annotations/VID/{self.subset}/{s}/",
                            "*",
                            "*.xml",
                        )
                    )
                    jobs = 100
                    items_per_job = int(math.ceil(len(xmls) / float(jobs)))
                    for _words in tqdm(
                        executor.map(
                            _parse_words,
                            [
                                xmls[r * items_per_job : (r + 1) * items_per_job]
                                for r in range(jobs)
                            ],
                        ),
                        desc=f"metadata -> {self.subset} -> {s}",
                        total=jobs,
                    ):
                        words.update(_words)
        elif self.subset == "val":
            words = set([])
            xmls = glob.glob(
                os.path.join(
                    self.dataset_root, f"Annotations/VID/{self.subset}/", "*", "*.xml"
                )
            )
            jobs = 100
            items_per_job = int(math.ceil(len(xmls) / float(jobs)))
            with concurrent.futures.ProcessPoolExecutor(8) as executor:
                for _words in tqdm(
                    executor.map(
                        _parse_words,
                        [
                            xmls[r * items_per_job : (r + 1) * items_per_job]
                            for r in range(jobs)
                        ],
                    ),
                    desc=f"metadata -> {self.subset}",
                    total=jobs,
                ):
                    words.update(_words)
        else:
            words = set([])

        assert self.subset == "test" or len(words) > 0, "empty words"
        categories = sorted([word_map[w][0] for w in words])
        return {
            "thing_classes": categories,
            "thing_colors": [random_color(rgb=True) for _ in categories],
            "thing_dataset_id_to_contiguous_id": {k: k for k in range(len(categories))},
            "word_to_category": {w: word_map[w][0] for w in words},
        }

    @staticmethod
    def register(dataset_filter=NoOpFilter(), subsets=set(["train", "val"])):
        assert isinstance(subsets, set), "subsets parameter has to be a set"
        valid_subsets = set(["train", "val"])
        assert subsets.issubset(
            valid_subsets
        ), f"subsets {subsets - valid_subsets} not found in {valid_subsets}"
        for d in subsets:
            dataset_name = f"imagenet_vid_video_{d}"
            dataset = ImageNetVIDVideoDataset(subset=d)
            DatasetCatalog.register(
                dataset_name, dataset.get_dataset_dicts(dataset_filter)
            )
            MetadataCatalog.get(dataset_name).set(**dataset.metadata)


if __name__ == "__main__":
    from data.datasets.videos.utils import video_dataset_stats

    ImageNetVIDVideoDataset.register()

    for d in ["train", "val"]:
        dataset_name = f"imagenet_vid_video_{d}"
        video_dataset_stats(
            dataset_name=dataset_name,
            vis_output=f"output/vis/datasets/videos/{dataset_name}",
            video=True,
        )

    """
    # ILSVRC2017_VID_train_0000/ILSVRC2017_train_00036000
    from detectron2.data import DatasetCatalog
    dataset: List[dict] = DatasetCatalog.get("imagenet_vid_video_train")
    for data in dataset:
        if data["id"] == "ILSVRC2017_VID_train_0000/ILSVRC2017_train_00036000":
            for frame in data["frames"]:
                if len(frame["annotations"]) == 0:
                    print(frame)
                    break
            break
    """
