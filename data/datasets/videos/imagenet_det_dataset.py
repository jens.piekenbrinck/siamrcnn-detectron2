"""
ImageNet DET dataset implementation
"""

import os
import platform
import getpass
from typing import List, Any, Optional
import glob
import xml.etree.ElementTree as ET
import cv2
from tqdm import tqdm

from detectron2.structures import BoxMode
from detectron2.data import DatasetCatalog, MetadataCatalog
from detectron2.utils.colormap import random_color

from data.datasets.videos.video_dataset import VideoDataset
from data.dataset_filter import NoOpFilter


class ImageNetDETVideoDataset(VideoDataset):
    def __init__(self, subset: str, cache_root: str = "./cache/datasets/videos"):
        super().__init__()

        # currently only train due to missing category_ids for val and test
        assert subset in ["train", "val"], (
            f"invalid subset {subset},"
            " only [train, val] is supported by ImageNet DET"
        )
        self.subset = subset
        self.cache_root = cache_root
        self.tqdm_desc = f"imagenet_det_video_{subset} loading"

        is_claix = "hpc.itc.rwth-aachen.de" in platform.node()
        root = f"/work/{getpass.getuser()}/globalwork" if is_claix else "/globalwork"
        self.dataset_root = os.path.join(root, "data/ILSVRC2015/")
        self.subsets = [
            "ILSVRC2013_train",
            "ILSVRC2014_train_0001",
            "ILSVRC2014_train_0002",
            "ILSVRC2014_train_0003",
            "ILSVRC2014_train_0004",
            "ILSVRC2014_train_0005",
            "ILSVRC2014_train_0006",
        ]

        super().init()

    def get_cache_path(self) -> str:
        return os.path.join(
            self.cache_root, f"imagenet_det_video_{self.subset}_cache.pkl"
        )

    def get_tqdm_desc(self) -> Optional[str]:
        return self.tqdm_desc

    def get_job_inputs(self) -> List[Any]:
        videos = []
        if self.subset == "train":
            for s in self.subsets:
                if s == self.subsets[0]:
                    xmls = glob.glob(
                        os.path.join(
                            self.dataset_root,
                            f"Annotations/DET/{self.subset}/{s}/",
                            "*",
                            "*.xml",
                        )
                    )
                else:
                    xmls = glob.glob(
                        os.path.join(
                            self.dataset_root,
                            f"Annotations/DET/{self.subset}/{s}/",
                            "*.xml",
                        )
                    )
                for xml in xmls:
                    videos.append((s, xml))
        elif self.subset == "val":
            xmls = glob.glob(
                os.path.join(
                    self.dataset_root, f"Annotations/DET/{self.subset}/", "*.xml"
                )
            )
            for xml in xmls:
                videos.append((None, xml))
        return videos

    def _load_frames(self, xml) -> List[dict]:
        """
        Load frames and annotations from video given by {video_path}
        """
        frames = []
        record = {}

        file_name = xml.replace("Annotations", "Data").replace("xml", "JPEG")

        record["file_name"] = file_name
        record["image_id"] = xml.split("/")[-1].split(".")[0]
        record["time"] = 0

        objs = []
        if os.path.exists(xml):
            xmltree = ET.parse(xml)
            for obj_id, object_iter in enumerate(xmltree.findall("object")):
                name = object_iter.find("name").text
                box_elem = object_iter.find("bndbox")
                box = [
                    float(box_elem.find(x).text)
                    for x in ["xmin", "ymin", "xmax", "ymax"]
                ]
                objs.append(
                    {
                        "obj_id": obj_id,
                        "bbox": box,
                        "bbox_mode": BoxMode.XYXY_ABS,
                        "category_id": self.metadata["thing_classes"].index(
                            self.metadata["word_to_category"][name]
                        ),
                    }
                )
        record["annotations"] = objs
        frames.append(record)

        return frames

    def load(self, x) -> dict:
        """
        Creating the video dict based on the {video_job}.
        This is called by a multithreading executor.
        """
        xml = x[1]
        frame_path = xml.replace("Annotations", "Data").replace("xml", "JPEG")
        height, width = cv2.imread(frame_path).shape[:2]
        frames = self._load_frames(xml)

        if self.subset == "train":
            video_id = os.path.join(
                x[0], xml.split("/")[-1].split(".")[0]
            )  # each .xml annotation is a video
        else:
            video_id = xml.split("/")[-1].split(".")[0]

        video = {
            "file_name": os.path.join(*xml.split("/")[:-1]).replace(
                "Annotations", "Data"
            ),
            "id": video_id,
            "height": height,
            "width": width,
            "frames": frames,
        }

        return video

    def load_metadata(self) -> dict:
        word_map = {}
        with open(os.path.join(self.dataset_root, "words.txt"), "r") as file:
            for line in file:
                word, categories = line.strip().split("\t")
                word_map[word] = categories.split(", ")

        if self.subset == "train":
            words = set([])
            for s in self.subsets:
                if s == self.subsets[0]:
                    xmls = glob.glob(
                        os.path.join(
                            self.dataset_root,
                            f"Annotations/DET/{self.subset}/{s}/",
                            "*",
                            "*.xml",
                        )
                    )
                else:
                    xmls = glob.glob(
                        os.path.join(
                            self.dataset_root,
                            f"Annotations/DET/{self.subset}/{s}/",
                            "*.xml",
                        )
                    )
                for xml in tqdm(xmls, desc=f"metadata -> {self.subset}"):
                    xmltree = ET.parse(xml)
                    for object_iter in xmltree.findall("object"):
                        name = object_iter.find("name").text
                        words.add(name)
        elif self.subset == "val":
            words = set([])
            xmls = glob.glob(
                os.path.join(
                    self.dataset_root, f"Annotations/DET/{self.subset}/", "*.xml"
                )
            )
            for xml in tqdm(xmls, desc=f"metadata -> {self.subset}"):
                xmltree = ET.parse(xml)
                for object_iter in xmltree.findall("object"):
                    name = object_iter.find("name").text
                    words.add(name)
        else:
            assert False, "unknown subset"

        assert len(words) > 0, "empty words"
        categories = sorted([word_map[w][0] for w in words])
        return {
            "thing_classes": categories,
            "thing_colors": [random_color(rgb=True) for _ in categories],
            "thing_dataset_id_to_contiguous_id": {k: k for k in range(len(categories))},
            "word_to_category": {w: word_map[w][0] for w in words},
        }

    @staticmethod
    def register(dataset_filter=NoOpFilter(), subsets=set(["train", "val"])):
        assert isinstance(subsets, set), "subsets parameter has to be a set"
        valid_subsets = set(["train", "val"])
        assert subsets.issubset(
            valid_subsets
        ), f"subsets {subsets - valid_subsets} not found in {valid_subsets}"
        for d in subsets:
            dataset_name = f"imagenet_det_video_{d}"
            dataset = ImageNetDETVideoDataset(subset=d)
            DatasetCatalog.register(
                dataset_name, dataset.get_dataset_dicts(dataset_filter)
            )
            MetadataCatalog.get(dataset_name).set(**dataset.metadata)


if __name__ == "__main__":
    from data.datasets.videos.utils import video_dataset_stats

    ImageNetDETVideoDataset.register()

    for d in ["train", "val"]:
        dataset_name = f"imagenet_det_video_{d}"
        video_dataset_stats(
            dataset_name=dataset_name,
            vis_output=f"output/vis/datasets/videos/{dataset_name}",
        )
