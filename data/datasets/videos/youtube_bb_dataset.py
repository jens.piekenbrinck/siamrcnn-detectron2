"""
YouTube BB dataset implementation
"""

import os
import platform
import getpass
from typing import List, Any, Optional
import glob
import xmltodict
import numpy as np
import cv2

from detectron2.structures import BoxMode
from detectron2.data import DatasetCatalog, MetadataCatalog
from detectron2.utils.colormap import random_color

from data.datasets.videos.video_dataset import VideoDataset
from data.dataset_filter import NoOpFilter


class YouTubeBBVideoDataset(VideoDataset):
    def __init__(self, subset: str, cache_root: str = "./cache/datasets/videos"):
        super().__init__()

        assert subset in ["train", "val"], (
            f"invalid subset {subset},"
            " only [train, val] is supported by YouTubeBB"
        )
        self.subset = subset
        self.cache_root = cache_root
        self.tqdm_desc = f"youtube_bb_video_{subset} loading"

        is_claix = "hpc.itc.rwth-aachen.de" in platform.node()
        root = f"/work/{getpass.getuser()}/globalwork" if is_claix else "/globalwork"
        self.dataset_root = os.path.join(root, "datasets/youtube-bb/")

        super().init()

    def get_cache_path(self) -> str:
        return os.path.join(self.cache_root, f"youtube_bb_video_{self.subset}_cache.pkl")

    def get_tqdm_desc(self) -> Optional[str]:
        return self.tqdm_desc

    def get_job_inputs(self) -> List[Any]:
        jobs = []
        with open(os.path.join(self.dataset_root, "yt_bb_detection_validation" if self.subset == "val" else "yt_bb_detection_train", "sets", "clips.txt")) as file:
            jobs = [l.strip() for l in file]
        return jobs

    def _load_frames(self, video_id) -> List[dict]:
        """
        Load frames and annotations from video
        """
        assert self.metadata is not None
        frame_path = os.path.join(self.dataset_root, "yt_bb_detection_validation" if self.subset == "val" else "yt_bb_detection_train", "frames", video_id)
        anno_path = os.path.join(
            self.dataset_root,
            "yt_bb_detection_validation" if self.subset == "val" else "yt_bb_detection_train",
            "annotations",
            video_id
        )

        image_files = sorted(glob.glob(os.path.join(frame_path, "*.jpg")), key=lambda x: int(x.split("-")[-1].replace(".jpg", "")))
        annotation_files = sorted(glob.glob(os.path.join(anno_path, "*.xml")), key=lambda x: int(x.split("-")[-1].replace(".xml", "")))

        frames = []
        for t, (image_file, annotation_file) in enumerate(zip(image_files, annotation_files)):
            if not os.path.exists(annotation_file):
                continue
            image_id = image_file.split("/")[-1].replace(".jpg", "")
            ann = xmltodict.parse(open(annotation_file).read())['annotation']

            record = {}

            record["file_name"] = image_file
            record["image_id"] = image_id
            record["time"] = t * 30 # assume 30 fps
            objs = []

            if "object" in ann:
                obj_anns = ann["object"]
                if not isinstance(obj_anns, list):
                    obj_anns = [obj_anns]
                for object_elem in obj_anns:
                    obj_id = int(object_elem["name"])
                    if "bndbox" in object_elem:
                        box_elem = object_elem["bndbox"]
                        width = float(ann["size"]["width"])
                        height = float(ann["size"]["height"])
                        # casted to int
                        x1 = round(float(box_elem["xmin"]) * width)
                        y1 = round(float(box_elem["ymin"]) * height)
                        x2 = round(float(box_elem["xmax"]) * width)
                        y2 = round(float(box_elem["ymax"]) * height)
                        box = np.asarray([x1, y1, x2, y2], dtype=np.float32)

                        obj = {
                            "obj_id": obj_id,
                            "bbox": box.tolist(),
                            "bbox_mode": BoxMode.XYXY_ABS,
                            "category_id": 0 #self.metadata["thing_classes"].index(video["objects"][str(obj_id)]["category"])
                        }
                        objs.append(obj)

            record["annotations"] = objs
            frames.append(record)

        return frames

    def load(self, x) -> dict:
        """
        Creating the video dict based on the {video_job}.
        This is called by a multithreading executor.
        """
        frame_path = os.path.join(self.dataset_root, "yt_bb_detection_validation" if self.subset == "val" else "yt_bb_detection_train", "frames")
        video_id = x
        first_frame_path = sorted(glob.glob(os.path.join(frame_path, video_id, "*.jpg")))[0]
        height, width = cv2.imread(first_frame_path).shape[:2]
        frames = self._load_frames(video_id)
        video = {
            "file_name": os.path.join(frame_path, video_id),
            "id": video_id,
            "height": height,
            "width": width,
            "frames": frames
        }

        return video

    def load_metadata(self) -> dict:
        return {
            "thing_classes": ["youtube_bb"],
            "thing_colors": [random_color(rgb=True)]
        }

    @staticmethod
    def register(dataset_filter=NoOpFilter(), subsets=set(["train", "val"])):
        assert isinstance(subsets, set), "subsets parameter has to be a set"
        valid_subsets = set(["train", "val"])
        assert subsets.issubset(valid_subsets), f"subsets {subsets - valid_subsets} not found in {valid_subsets}"
        for d in subsets:
            dataset_name = f"youtube_bb_video_{d}"
            dataset = YouTubeBBVideoDataset(subset=d)
            DatasetCatalog.register(dataset_name, dataset.get_dataset_dicts(dataset_filter))
            MetadataCatalog.get(dataset_name).set(**dataset.metadata)


if __name__ == "__main__":
    from data.datasets.videos.utils import video_dataset_stats


    YouTubeBBVideoDataset.register()

    for d in ["train", "val"]:
        dataset_name = f"youtube_bb_video_{d}"
        video_dataset_stats(
            dataset_name=dataset_name,
            vis_output=f"output/vis/datasets/videos/{dataset_name}",
            video=True,
            video_fps=10
        )
