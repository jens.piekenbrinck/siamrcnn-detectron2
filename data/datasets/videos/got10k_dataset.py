"""
GOT10k dataset implementation
"""

import os
import platform
import getpass
from typing import List, Any, Optional
import cv2

from detectron2.structures import BoxMode
from detectron2.data import DatasetCatalog, MetadataCatalog
from detectron2.utils.colormap import random_color

from data.datasets.videos.video_dataset import VideoDataset
from data.dataset_filter import NoOpFilter


class GOT10kVideoDataset(VideoDataset):
    def __init__(self, subset: str, cache_root: str = "./cache/datasets/videos"):
        super().__init__()

        assert subset in ["train", "val"], (
            f"invalid subset {subset}," " only [train, val] is supported by GOT10k"
        )
        self.subset = subset
        self.cache_root = cache_root
        self.tqdm_desc = f"got10k_video_{subset} loading"

        is_claix = "hpc.itc.rwth-aachen.de" in platform.node()
        root = f"/work/{getpass.getuser()}/globalwork" if is_claix else "/globalwork"
        self.dataset_root = os.path.join(root, "data/GOT10k/")

        super().init()

    def get_cache_path(self) -> str:
        return os.path.join(self.cache_root, f"got10k_video_{self.subset}_cache.pkl")

    def get_tqdm_desc(self) -> Optional[str]:
        return f"got10k_video_{self.subset} loading"

    def get_job_inputs(self) -> List[Any]:
        dataset_path = os.path.join(self.dataset_root, self.subset, "list.txt")
        with open(dataset_path) as file:
            return [l.strip() for l in file]

    def _load_frames(self, video_id, video_path) -> List[dict]:
        """
        Load frames and annotations from video given by {video_path}
        """
        gt_file = os.path.join(video_path, "groundtruth.txt")
        absent_file = os.path.join(video_path, "absence.label")
        absent_exists = os.path.exists(absent_file)

        metainfo_file = os.path.join(video_path, "meta_info.ini")
        super_category_id = -1
        category_id = -1
        obj_id = -1
        motion_id = -1
        motion_adverb_id = -1
        with open(metainfo_file) as f:
            for line in [l.strip() for l in f]:
                if line.startswith("root_class"):
                    super_category_id = self.metadata["super_classes"].index(
                        line[len("root_class: ") :]
                    )
                elif line.startswith("major_class"):
                    category_id = self.metadata["thing_classes"].index(
                        line[len("major_class: ") :]
                    )
                elif line.startswith("object_class"):
                    obj_id = self.metadata["object_classes"].index(
                        line[len("object_class: ") :]
                    )
                elif line.startswith("motion_class"):
                    motion_id = self.metadata["motion_classes"].index(
                        line[len("motion_class: ") :]
                    )
                elif line.startswith("motion_adverb"):
                    motion_adverb_id = self.metadata["motion_adverbs"].index(
                        line[len("motion_adverb: ") :]
                    )

        boxes = []
        with open(gt_file) as f:
            for l in f:
                box = [float(x) for x in l.strip().split(",")]
                boxes.append(box)

        if absent_exists:
            absence = []
            with open(absent_file) as f:
                for l in f:
                    absence.append(int(l.strip()))

            assert len(boxes) == len(absence)
            filtered = [(i, x) for i, x in enumerate(boxes) if absence[i] == 0]
        else:
            filtered = [(i, x) for i, x in enumerate(boxes)]

        assert len(filtered) > 0

        frames = []
        for (time, box) in filtered:
            record = {}

            file_name = os.path.join(video_path, "%08d.jpg" % (time + 1))

            record["file_name"] = file_name
            record["image_id"] = f"{video_id}/{'%08d.jpg' % (time + 1)}"
            record["time"] = time

            objs = []
            obj = {
                "bbox": box,
                "bbox_mode": BoxMode.XYWH_ABS,
                "super_category_id": super_category_id,
                "category_id": category_id,
                "obj_id": obj_id,
                "motion_id": motion_id,
                "motion_adverb_id": motion_adverb_id,
            }
            objs.append(obj)
            record["annotations"] = objs
            frames.append(record)

        return frames

    def load(self, x) -> dict:
        """
        Creating the video dict based on the {video_job}.
        This is called by a multithreading executor.
        """
        video_path = os.path.join(self.dataset_root, self.subset, x)
        first_frame_path = os.path.join(video_path, "%08d.jpg" % 1)
        height, width = cv2.imread(first_frame_path).shape[:2]
        video = {
            "file_name": video_path,
            "id": x,
            "height": height,
            "width": width,
            "frames": self._load_frames(x, video_path),
        }
        return video

    def load_metadata(self) -> dict:
        dataset_path = os.path.join(self.dataset_root, self.subset, "list.txt")
        root_classes = set()
        major_classes = set()
        object_classes = set()
        motion_classes = set()
        motion_adverbs = set()
        with open(dataset_path) as file:
            for video_id in [l.strip() for l in file]:
                video_path = os.path.join(self.dataset_root, self.subset, video_id)
                meta_info = os.path.join(video_path, "meta_info.ini")
                with open(meta_info) as meta:
                    for line in [l.strip() for l in meta]:
                        if line.startswith("root_class"):
                            root_classes.add(line[len("root_class: ") :])
                        elif line.startswith("major_class"):
                            major_classes.add(line[len("major_class: ") :])
                        elif line.startswith("object_class"):
                            object_classes.add(line[len("object_class: ") :])
                        elif line.startswith("motion_class"):
                            motion_classes.add(line[len("motion_class: ") :])
                        elif line.startswith("motion_adverb"):
                            motion_adverbs.add(line[len("motion_adverb: ") :])

        return {
            "thing_classes": sorted(list(major_classes)),
            "thing_colors": [random_color(rgb=True) for _ in major_classes],
            "thing_dataset_id_to_contiguous_id": {
                k: k for k in range(len(major_classes))
            },
            "super_classes": sorted(list(root_classes)),
            "object_classes": sorted(list(object_classes)),
            "motion_classes": sorted(list(motion_classes)),
            "motion_adverbs": sorted(list(motion_adverbs)),
        }

    @staticmethod
    def register(dataset_filter=NoOpFilter(), subsets=set(["train", "val"])):
        assert isinstance(subsets, set), "subsets parameter has to be a set"
        valid_subsets = set(["train", "val"])
        assert subsets.issubset(
            valid_subsets
        ), f"subsets {subsets - valid_subsets} not found in {valid_subsets}"
        for d in subsets:
            dataset_name = f"got10k_video_{d}"
            dataset = GOT10kVideoDataset(subset=d)
            DatasetCatalog.register(
                dataset_name, dataset.get_dataset_dicts(dataset_filter)
            )
            MetadataCatalog.get(dataset_name).set(**dataset.metadata)


if __name__ == "__main__":
    from data.datasets.videos.utils import video_dataset_stats

    GOT10kVideoDataset.register()

    for d in ["train", "val"]:
        dataset_name = f"got10k_video_{d}"
        video_dataset_stats(
            dataset_name=dataset_name,
            vis_output=f"output/vis/datasets/videos/{dataset_name}",
            video=True,
        )
