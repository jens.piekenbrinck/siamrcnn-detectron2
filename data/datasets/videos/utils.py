import os
import time
from typing import List, Optional
from pathlib import Path
from copy import deepcopy
from datetime import timedelta
import cv2

from detectron2.utils.visualizer import Visualizer, ColorMode
from detectron2.data import DatasetCatalog, MetadataCatalog


def video_dataset_stats(
    dataset_name: str,
    vis_output: Optional[str],
    video: bool = False,
    video_fps: int = 30,
):
    """
    Calculate dataset stats and visualize a few items.
    """
    print(f"{dataset_name} -> loading")
    start = time.time()
    dataset: List[dict] = DatasetCatalog.get(dataset_name)
    dataset_metadata = MetadataCatalog.get(dataset_name)
    elapsed = time.time() - start
    print(
        f"{dataset_name} -> has {len(dataset)} videos and loaded in {timedelta(seconds=elapsed)}"
    )

    frame_counts = [len(video["frames"]) for video in dataset]
    frame_sum = sum(frame_counts)
    print(
        (
            f"{dataset_name} frame stats -> sum: {frame_sum}"
            f" | min: {min(frame_counts)}"
            f" | avg: {frame_sum/len(frame_counts)}"
            f" | max: {max(frame_counts)}"
        )
    )
    print(f"{dataset_name} -> num classes {len(dataset_metadata.thing_classes)}")
    print(f"{dataset_name} -> classes: {dataset_metadata.thing_classes}")

    if vis_output is not None:
        vis_output = Path(vis_output)
        print(f"{dataset_name} -> visualize and log first items")
        for i, data in enumerate(dataset):
            print(
                {
                    k: len(v)
                    if isinstance(v, list)
                    else {k2: len(v2["frames"]) for k2, v2 in v.items()}
                    if isinstance(v, dict) and k == "tracks"
                    else list(v.keys())
                    if isinstance(v, dict)
                    else v
                    for k, v in data.items()
                }
            )
            root_dir: Path = vis_output / data["id"]
            root_dir.mkdir(parents=True, exist_ok=True)
            for frame in data["frames"][: 6 * 3 : 6]:
                image = cv2.imread(frame["file_name"])
                visualizer = Visualizer(
                    image[:, :, ::-1],
                    metadata=dataset_metadata,
                    scale=1,
                    instance_mode=ColorMode.SEGMENTATION,
                )
                vis = visualizer.draw_dataset_dict(frame)
                vis.save(str(root_dir / f"image_{frame['time']}.png"))

            if video:
                out = None

                for frame in data["frames"][:150]:
                    image = cv2.imread(frame["file_name"])
                    visualizer = Visualizer(
                        image[:, :, ::-1],
                        metadata=dataset_metadata,
                        scale=0.5,
                        instance_mode=ColorMode.SEGMENTATION,
                    )
                    vis = visualizer.draw_dataset_dict(frame)
                    vis_img = vis.get_image()[:, :, ::-1]

                    if out is None:
                        out = cv2.VideoWriter(
                            str(vis_output / f"{data['id']}.mp4"),
                            cv2.VideoWriter_fourcc(*"mp4v"),
                            video_fps,
                            (vis_img.shape[1], vis_img.shape[0]),
                        )

                    out.write(vis_img)

                if out is not None:
                    out.release()

            if i == 2:
                break

        print(f"{dataset_name} visualizations saved to '{vis_output}'")


def extend_video_with_tracks(video: dict) -> dict:
    vid = deepcopy(video)  # will be overwritten by this function
    tracks = {}
    for frame in vid["frames"]:
        for anno in frame["annotations"]:
            assert (
                "obj_id" in anno
            ), f"missing object id 'obj_id: int' in frame: {frame}"
            if anno["obj_id"] in tracks:
                tracks[anno["obj_id"]].append(frame)
            else:
                tracks[anno["obj_id"]] = [frame]

    def filter_obj(obj_id, frame):
        f = deepcopy(
            frame
        )  # deepcopy necessary because it will be overwritten multiple times otherwise
        f["annotations"] = [a for a in f["annotations"] if a["obj_id"] == obj_id]
        return f

    def to_track(obj_id, frames):
        filtered = [filter_obj(obj_id, f) for f in frames]
        return {
            "file_name": vid["file_name"],
            "video_id": vid["id"],
            "height": vid["height"],
            "width": vid["width"],
            "frames": filtered,
            "obj_id": obj_id,
            "category_id": filtered[0]["annotations"][0]["category_id"],
        }

    vid["tracks"] = {
        obj_id: to_track(obj_id, frames) for obj_id, frames in tracks.items()
    }

    return vid
