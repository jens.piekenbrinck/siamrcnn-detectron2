"""
LaSOT dataset implementation
"""

import os
import platform
import getpass
from typing import List, Any, Optional
import glob
import cv2

from detectron2.structures import BoxMode
from detectron2.data import DatasetCatalog, MetadataCatalog
from detectron2.utils.colormap import random_color

from data.datasets.videos.video_dataset import VideoDataset
from data.dataset_filter import NoOpFilter


class LaSOTVideoDataset(VideoDataset):
    def __init__(self, subset: str, cache_root: str = "./cache/datasets/videos"):
        super().__init__()

        assert subset in ["train"], (
            f"invalid subset {subset}," " only [train] is supported by LaSOT"
        )
        self.subset = subset
        self.cache_root = cache_root
        self.tqdm_desc = f"lasot_video_{subset} loading"

        is_claix = "hpc.itc.rwth-aachen.de" in platform.node()
        root = f"/work/{getpass.getuser()}/globalwork" if is_claix else "/globalwork"
        self.dataset_root = os.path.join(root, "datasets/LaSOTBenchmark/")

        super().init()

    def get_cache_path(self) -> str:
        return os.path.join(self.cache_root, f"lasot_video_{self.subset}_cache.pkl")

    def get_tqdm_desc(self) -> Optional[str]:
        return self.tqdm_desc

    def get_job_inputs(self) -> List[Any]:
        jobs = []
        with open(os.path.join(self.dataset_root, "training_set.txt")) as file:
            jobs = [l.strip() for l in file]
        return jobs

    def _load_frames(self, video_path, category) -> List[dict]:
        """
        Load frames and annotations from video
        """
        assert self.metadata is not None
        gt_file = os.path.join(video_path, "groundtruth.txt")
        oov_file = os.path.join(video_path, "out_of_view.txt")
        full_occ_file = os.path.join(video_path, "full_occlusion.txt")

        boxes = []

        with open(gt_file) as file:
            for l in file:
                boxes.append([float(x) for x in l.strip().split(",")])

        with open(oov_file) as f:
            oovs = [int(x) for x in f.read().strip().split(",")]

        with open(full_occ_file) as f:
            full_occs = [int(x) for x in f.read().strip().split(",")]

        assert len(boxes) == len(oovs) == len(full_occs)
        data = [
            (t, x) for t, x in enumerate(boxes) if oovs[t] == 0 and full_occs[t] == 0
        ]
        assert len(data) > 0

        frames = []
        for (time, box) in data:
            record = {}

            record["file_name"] = os.path.join(
                video_path, "img", "%08d.jpg" % (time + 1)
            )
            record["image_id"] = "%08d" % (time + 1)
            record["time"] = time
            objs = []
            obj = {
                "obj_id": 0,
                "bbox": box,
                "bbox_mode": BoxMode.XYWH_ABS,
                "category_id": 0,
            }
            objs.append(obj)

            record["annotations"] = objs
            frames.append(record)

        return frames

    def load(self, x) -> dict:
        """
        Creating the video dict based on the {video_job}.
        This is called by a multithreading executor.
        """

        # x is e.g. "airplane-13"
        category = x.split("-")[0]
        video_path = os.path.join(self.dataset_root, category, x)
        # gt_file = os.path.join(video_path, "groundtruth.txt")
        # oov_file = os.path.join(video_path, "out_of_view.txt")
        # full_occ_file = os.path.join(video_path, "full_occlusion.txt")

        first_frame_path = sorted(glob.glob(os.path.join(video_path, "img", "*.jpg")))[
            0
        ]
        height, width = cv2.imread(first_frame_path).shape[:2]

        frames = self._load_frames(video_path, category)
        video = {
            "file_name": video_path,
            "id": x,
            "height": height,
            "width": width,
            "frames": frames,
        }

        return video

    def load_metadata(self) -> dict:
        categories = sorted(
            [f.name for f in os.scandir(self.dataset_root) if f.is_dir()]
        )
        return {
            "thing_classes": categories,
            "thing_colors": [random_color(rgb=True) for _ in categories],
        }

    @staticmethod
    def register(dataset_filter=NoOpFilter(), subsets=set(["train"])):
        assert isinstance(subsets, set), "subsets parameter has to be a set"
        valid_subsets = set(["train"])
        assert subsets.issubset(
            valid_subsets
        ), f"subsets {subsets - valid_subsets} not found in {valid_subsets}"
        for d in subsets:
            dataset_name = f"lasot_video_{d}"
            dataset = LaSOTVideoDataset(subset=d)
            DatasetCatalog.register(
                dataset_name, dataset.get_dataset_dicts(dataset_filter)
            )
            MetadataCatalog.get(dataset_name).set(**dataset.metadata)


if __name__ == "__main__":
    from data.datasets.videos.utils import video_dataset_stats

    LaSOTVideoDataset.register()

    for d in ["train"]:
        dataset_name = f"lasot_video_{d}"
        video_dataset_stats(
            dataset_name=dataset_name,
            vis_output=f"output/vis/datasets/videos/{dataset_name}",
            video=True,
        )
