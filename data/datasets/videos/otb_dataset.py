"""
OTB dataset implementation
"""

import os
import glob
import platform
import getpass
from typing import List, Any, Optional
import cv2
import json

from detectron2.structures import BoxMode
from detectron2.data import DatasetCatalog, MetadataCatalog
from detectron2.utils.colormap import random_color

from data.datasets.videos.video_dataset import VideoDataset
from data.dataset_filter import NoOpFilter


class OTBVideoDataset(VideoDataset):
    def __init__(self, cache_root: str = "./cache/datasets/videos"):
        super().__init__()

        self.cache_root = cache_root
        self.tqdm_desc = "otb_video loading"

        is_claix = "hpc.itc.rwth-aachen.de" in platform.node()
        root = f"/work/{getpass.getuser()}/globalwork" if is_claix else "/globalwork"
        self.dataset_root = os.path.join(root, "data/OTB_new/")

        super().init()

    def get_cache_path(self) -> str:
        return os.path.join(self.cache_root, "otb_video_cache.pkl")

    def get_tqdm_desc(self) -> Optional[str]:
        return self.tqdm_desc

    def get_job_inputs(self) -> List[Any]:
        with open(os.path.join(self.dataset_root, "OTB100.json")) as f:
            tmp = json.load(f)
            return [(k, v) for k,v in tmp.items()]

        # dataset_path = os.path.join(self.dataset_root, "list.txt")
        # with open(dataset_path) as file:
        #    return [(l.strip(), os.path.join(self.dataset_root, l.strip())) for l in file]

    def _load_frames(self, video) -> List[dict]:
        """
        Load frames and annotations from video given by {video_path}
        """
        boxes = video["gt_rect"]
        image_paths = video["img_names"]

        frames = []
        for time, (img_path, box) in enumerate(zip(image_paths, boxes)):
            record = {}

            file_name = os.path.join(self.dataset_root, img_path)

            record["file_name"] = file_name
            record["image_id"] = f"{video['video_dir']}/{time}"
            record["time"] = time

            objs = []
            obj = {
                "obj_id": 0,
                "bbox": box,
                "bbox_mode": BoxMode.XYWH_ABS,
                "category_id": 0
            }
            objs.append(obj)
            record["annotations"] = objs
            frames.append(record)

        return frames

    def load(self, x) -> dict:
        """
        Creating the video dict based on the {video_job}.
        This is called by a multithreading executor.
        """
        # first_frame_path = sorted(glob.glob(os.path.join(x[1], "img/*.jpg")))[0]
        first_frame_path = os.path.join(self.dataset_root, x[1]["img_names"][0])
        height, width = cv2.imread(first_frame_path).shape[:2]
        video = {
            "id": x[0],
            "file_name": os.path.join(self.dataset_root, x[1]["video_dir"]),
            "height": height,
            "width": width,
            "frames": self._load_frames(x[1]),
            "attributes": x[1]["attr"]
        }
        return video

    def load_metadata(self) -> dict:
        attributes = set([])
        with open(os.path.join(self.dataset_root, "OTB100.json")) as f:
            tmp = json.load(f)
            for k, v in tmp.items():
                attributes.update(v['attr'])
        attributes = sorted(list(attributes))
        return {
            "thing_classes": ["otb"],
            "thing_colors": [random_color(rgb=True)],
            "attributes": attributes
        }

    @staticmethod
    def register(dataset_filter=NoOpFilter()):
        dataset_name = "otb_video"
        dataset = OTBVideoDataset()
        DatasetCatalog.register(dataset_name, dataset.get_dataset_dicts(dataset_filter))
        MetadataCatalog.get(dataset_name).set(**dataset.metadata)


if __name__ == "__main__":
    from data.datasets.videos.utils import video_dataset_stats


    OTBVideoDataset.register()

    dataset_name = "otb_video"
    video_dataset_stats(
        dataset_name=dataset_name,
        vis_output=f"output/vis/datasets/videos/{dataset_name}",
        video=True
    )
