"""
Tracking Any Object dataset implementation
"""

import os
import platform
import getpass
from typing import List, Any, Optional
import json

from detectron2.structures import BoxMode
from detectron2.data import DatasetCatalog, MetadataCatalog
from detectron2.utils.colormap import random_color

from data.datasets.videos.video_dataset import VideoDataset
from data.dataset_filter import NoOpFilter


class TAOVideoDataset(VideoDataset):
    def __init__(self, subset: str, cache_root: str = "./cache/datasets/videos"):
        super().__init__()

        assert subset in ["train", "val"], (
            f"invalid subset {subset},"
            " only [train, val] is supported by TAO"
        )
        self.subset = subset
        self.cache_root = cache_root
        self.tqdm_desc = f"tao_video_{subset} loading"

        is_claix = "hpc.itc.rwth-aachen.de" in platform.node()
        root = f"/work/{getpass.getuser()}/globalwork" if is_claix else "/globalwork"
        self.dataset_root = os.path.join(root, "datasets/TAO/")

        super().init()

    def get_cache_path(self) -> str:
        return os.path.join(self.cache_root, f"tao_video_{self.subset}_cache.pkl")

    def get_tqdm_desc(self) -> Optional[str]:
        return self.tqdm_desc

    def get_job_inputs(self) -> List[Any]:
        dataset_path = os.path.join(self.dataset_root, "annotations-1.1", "validation.json" if self.subset == "val" else "train.json")
        jobs = []
        # TODO refactor for videos?
        with open(dataset_path) as file:
            data = json.load(file)
            annotations = {}
            for annotation in data["annotations"]:
                if annotation["track_id"] in annotations:
                    annotations[annotation["track_id"]].append(annotation)
                else:
                    annotations[annotation["track_id"]] = [annotation]
            for track in data["tracks"]:
                track_video = next((x for x in data["videos"] if x["id"] == track["video_id"]), None)
                if track_video is None:
                    continue
                track_category = next((x for x in data["categories"] if x["id"] == track["category_id"]), None)
                if track_category is None:
                    continue
                track_annotations = annotations[track["id"]]
                if len(track_annotations) == 0:
                    continue
                track_images = [x for x in data["images"] if x["id"] in [y["image_id"] for y in track_annotations]]
                if len(track_images) == 0:
                    continue
                jobs.append((track, track_images, track_annotations, track_category, track_video))
            return jobs

    def _load_frames(self, track, track_images, track_annotations, track_category, track_video) -> List[dict]:
        """
        Load frames and annotations from video given by {video_path}
        """

        frames = []
        for (image, annotation) in zip(track_images, track_annotations):
            record = {}

            file_name = os.path.join(self.dataset_root, "frames", image["file_name"])

            record["file_name"] = file_name
            record["image_id"] = file_name
            record["time"] = image["frame_index"]

            objs = []
            obj = {
                "obj_id": 0,
                "bbox": annotation["bbox"],
                "bbox_mode": BoxMode.XYWH_ABS,
                "category_id": 0 # annotation["category_id"]
            }
            objs.append(obj)
            record["annotations"] = objs
            frames.append(record)

        return frames

    def load(self, x) -> dict:
        """
        Creating the video dict based on the {video_job}.
        This is called by a multithreading executor.
        """
        # TODO load videos instead!
        track = x[0]
        track_images = x[1]
        track_annotations = x[2]
        track_category = x[3]
        track_video = x[4]

        record = {}
        record["file_name"] = track_video["name"]
        record["height"] = track_video["height"]
        record["width"] = track_video["width"]
        record["frames"] = self._load_frames(track, track_images, track_annotations, track_category, track_video)
        return record

    def load_metadata(self) -> dict:
        dataset_path = os.path.join(self.dataset_root, "annotations-1.1", "validation.json" if self.subset == "val" else "train.json")
        categories = []
        with open(dataset_path) as file:
            data = json.load(file)
            categories = data["categories"]
        return {
            "thing_classes": [c["name"] for c in categories],
            "thing_colors": [random_color(rgb=True) for _ in categories]
        }

    @staticmethod
    def register(dataset_filter=NoOpFilter(), subsets=set(["train", "val"])):
        assert isinstance(subsets, set), "subsets parameter has to be a set"
        valid_subsets = set(["train", "val"])
        assert subsets.issubset(valid_subsets), f"subsets {subsets - valid_subsets} not found in {valid_subsets}"
        for d in subsets:
            dataset_name = f"tao_video_{d}"
            dataset = TAOVideoDataset(subset=d)
            DatasetCatalog.register(dataset_name, dataset.get_dataset_dicts(dataset_filter))
            MetadataCatalog.get(dataset_name).set(**dataset.metadata)


if __name__ == "__main__":
    from data.datasets.videos.utils import video_dataset_stats


    TAOVideoDataset.register()

    for d in ["train", "val"]:
        dataset_name = f"tao_video_{d}"
        video_dataset_stats(
            dataset_name=dataset_name,
            vis_output=f"output/vis/datasets/videos/{dataset_name}",
            video=True
        )
