import logging
import pickle
import numpy as np

from data.frame_sampler import SiamVideoSampler


class SiamRCNNSubDataset:
    def __init__(
        self,
        name: str,
        lst: list,
        sampler: SiamVideoSampler,
        num_use: int = 0,
        repeat: int = 1,
    ):
        self.name = name
        self._lst = lst
        self.sampler = sampler
        self.repeat = max(1, repeat)
        self._serialize = True

        def _serialize(data):
            buffer = pickle.dumps(data, protocol=-1)
            return np.frombuffer(buffer, dtype=np.uint8)

        if self._serialize:
            self._lst = [_serialize(x) for x in self._lst]
            self._addr = np.asarray([len(x) for x in self._lst], dtype=np.int64)
            self._addr = np.cumsum(self._addr)
            self._lst = np.concatenate(self._lst)
            logger = logging.getLogger("detectron2")
            logger.info(
                "{} -> serialized dataset takes {:.2f} MiB for {} items".format(
                    self.name, len(self._lst) / 1024 ** 2, len(self._addr)
                )
            )

        self.num_use = num_use
        self.length = len(self._addr if self._serialize else self._lst)
        self.length_use = num_use if num_use > 0 else self.length
        self.length_use *= repeat
        self.indices = self.shuffle(self.length_use)

    def __len__(self):
        return self.length_use

    def _deserialize(self, index):
        if self._serialize:
            start_addr = 0 if index == 0 else self._addr[index - 1].item()
            end_addr = self._addr[index].item()
            bytes = memoryview(self._lst[start_addr:end_addr])
            return pickle.loads(bytes)
        else:
            return self._lst[index]

    def shuffle(self, num):
        indices = []
        while len(indices) < num:
            p = list(range(0, self.length))
            np.random.shuffle(p)
            indices += p
        return indices[:num]

    def sample(self, index=-1):
        if index == -1:
            index = np.random.randint(0, len(self))
        idx = self.indices[index]
        item = self._deserialize(idx)

        obj_id = np.random.choice(list(item["tracks"].keys()))
        track_dict = item["tracks"][obj_id]

        template, search = self.sampler(track_dict)

        return template, search, track_dict