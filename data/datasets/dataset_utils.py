import copy
import torch

from detectron2.structures import BoxMode
from detectron2.structures.boxes import Boxes

from data.datasets.videos import *
from data.dataset_filter import NoOpFilter


def register_datasets(cfg):
    """
    Register custom datasets defined in config.
    This is not used anymore due to support for custom datasets in trainer.
    """
    datasets_with_subsets = [
        "got10k_video",
        "imagenet_vid_video",
        "imagenet_det_video",
        "youtube_vos_video",
        "youtube_bb_video",
        "lasot_video",
    ]
    datasets = {}
    # FilterList([BBoxFilter(min_size=(1., 1.)), SiamLimitedTrackFilter(min_length=15, min_time_step=1)])
    otb_dataset_filter = NoOpFilter()
    for dataset in set(cfg.DATASETS.TRAIN + cfg.DATASETS.TEST):
        if dataset == "otb_video":
            OTBVideoDataset.register(dataset_filter=otb_dataset_filter)
        elif dataset.startswith("coco_video"):
            name, version, subset = dataset.rsplit("_", 2)
            if datasets.get(name, None) is None:
                datasets[name] = set([(version, subset)])
            else:
                datasets[name].add((version, subset))
        else:
            name, subset = dataset.rsplit("_", 1)
            if name in datasets_with_subsets:
                if datasets.get(name, None) is None:
                    datasets[name] = set([subset])
                else:
                    datasets[name].add(subset)

    # BBoxFilter(min_size=(20., 20.))
    # FilterList([SiamLimitedTrackFilter(min_length=2, min_time_step=1)])
    dataset_filter = NoOpFilter()  # BBoxFilter(min_size=(64., 64.))
    for name, subsets in datasets.items():
        if name == "got10k_video":
            GOT10kVideoDataset.register(dataset_filter=dataset_filter, subsets=subsets)
        elif name == "imagenet_vid_video":
            ImageNetVIDVideoDataset.register(
                dataset_filter=dataset_filter, subsets=subsets
            )
        elif name == "imagenet_det_video":
            ImageNetDETVideoDataset.register(
                dataset_filter=dataset_filter, subsets=subsets
            )
        elif name == "youtube_vos_video":
            YouTubeVOSVideoDataset.register(
                dataset_filter=dataset_filter, subsets=subsets
            )
        elif name == "youtube_bb_video":
            YouTubeBBVideoDataset.register(
                dataset_filter=dataset_filter, subsets=subsets
            )
        elif name == "coco_video":
            COCOVideoDataset.register(
                dataset_filter=dataset_filter,
                versions=set([x[0] for x in subsets]),
                subsets=set([x[1] for x in subsets]),
            )
        elif name == "lasot_video":
            LaSOTVideoDataset.register(dataset_filter=dataset_filter, subsets=subsets)
        else:
            assert False, f"invalid dataset name found: {name}"


def filter_invalid_or_only_crowd_annotations(dataset_dicts):
    results = []
    for video in dataset_dicts:
        track_updated = False
        new_tracks = {}
        for obj_id, track in video["tracks"].items():
            new_frames = []
            for frame in track["frames"]:
                new_annos = []
                for anno in frame["annotations"]:
                    if anno.get("iscrowd", 0) != 0:  # exclude crowd frames
                        continue
                    bbox = BoxMode.convert(
                        anno["bbox"],
                        from_mode=anno["bbox_mode"],
                        to_mode=BoxMode.XYXY_ABS,
                    )
                    bbox = Boxes(torch.Tensor([bbox]))
                    bbox.clip((track["height"], track["width"]))
                    bbox = bbox.tensor[0]
                    if bbox[2] - bbox[0] > 0 and bbox[3] - bbox[1] > 0:
                        new_annos.append(anno)
                if len(new_annos) == 0:
                    continue  # filter empty annotations or if has only crowd annotation
                if len(new_annos) != len(frame["annotations"]):
                    new_frame = copy.deepcopy(
                        {k: v for k, v in frame.items() if k != "annotations"}
                    )
                    new_frame["annotations"] = new_annos
                    new_frames.append(new_frame)
                else:
                    new_frames.append(frame)
            if len(new_frames) == 0:
                continue
            if len(new_frames) != len(track["frames"]):
                new_track = copy.deepcopy(
                    {k: v for k, v in track.items() if k != "frames"}
                )
                new_track["frames"] = new_frames
                new_tracks[obj_id] = new_track
                track_updated = True
            else:
                new_tracks[obj_id] = track

        if len(new_tracks) == 0:
            continue
        if (
            track_updated
            or len(set(video["tracks"].keys()) - set(new_tracks.keys())) != 0
        ):
            new_video = copy.deepcopy(
                {k: v for k, v in video.items() if k != "tracks" and k != "frames"}
            )
            new_video["frames"] = video["frames"]
            new_video["tracks"] = new_tracks
            results.append(new_video)
        else:
            results.append(video)
    return results


def filter_tracks_with_only_crowd_annotations(dataset_dicts):
    """
    Filter out images with none annotations or only crowd annotations
    (i.e., images without non-crowd annotations).
    A common training-time preprocessing on COCO dataset.

    Args:
        dataset_dicts (list[dict]): annotations in Detectron2 Dataset format.

    Returns:
        list[dict]: the same format, but filtered.
    """

    def valid(video):
        if len(video["frames"]) == 0:
            print("no frames >", video["id"])
            return False
        for frame in video["frames"]:
            has_anno = False
            if len(frame["annotations"]) == 0:
                print("empty annotations >", video["id"])
            for ann in frame["annotations"]:
                if ann.get("iscrowd", 0) == 0:
                    has_anno = True
                    break
            if not has_anno:
                print("has crowd anno >", video["id"])
                return False
        return True

    dataset_dicts = [x for x in dataset_dicts if valid(x)]
    return dataset_dicts
