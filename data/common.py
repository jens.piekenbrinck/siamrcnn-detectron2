from torch.utils import data


class SiamAspectRatioGroupedDataset(data.IterableDataset):
    """
    Batch data that have similar aspect ratio together.
    In this implementation, images whose aspect ratio < (or >) 1 will
    be batched together.
    This improves training speed because the images then need less padding
    to form a batch.
    It assumes the underlying dataset produces dicts with "width" and "height" keys.
    It will then produce a list of original dicts with length = batch_size,
    all with similar aspect ratios.
    """

    def __init__(self, dataset, batch_size):
        """
        Args:
            dataset: an iterable. Each element must be a dict with keys "template" and "search",
                which will be used to batch data.
            batch_size (int):
        """
        self.dataset = dataset
        self.batch_size = batch_size
        # Hard-coded four aspect ratio groups
        # Can add support for more aspect ratio groups, but doesn't seem useful
        self._buckets = {
            0: [],  # template w > h and search w > h
            1: [],  # template w > h and search w < h
            2: [],  # template w < h and search w > h
            3: [],  # template w < h and search w < h
        }

    def __iter__(self):
        for d in self.dataset:
            template = d["template"]
            if isinstance(d["search"], list):
                search = d["search"][0]  # all search images have to be of the same size
            else:
                search = d["search"]
            _, tw, th = template["image"].shape
            _, sw, sh = search["image"].shape
            if tw > th and sw > sh:
                bucket_id = 0
            elif tw > th and sw < sh:
                bucket_id = 1
            elif tw < th and sw > sh:
                bucket_id = 2
            else:
                bucket_id = 3
            # bucket_id = 0 if w > h else 1
            bucket = self._buckets[bucket_id]
            bucket.append(d)
            if len(bucket) == self.batch_size:
                yield bucket[:]
                del bucket[:]


class SiamAspectRatioSegmentationGroupedDataset(data.IterableDataset):
    """
    Batch data that have similar aspect ratio together, additionally all
    items have a segmentation mask or not.
    This improves training speed because the images then need less padding
    to form a batch.
    It assumes the underlying dataset produces dicts with "template" and "search" keys.
    It will then produce a list of original dicts with length = batch_size,
    all with similar aspect ratios and either a segmentation mask or not.
    """

    def __init__(self, dataset, batch_size):
        """
        Args:
            dataset: an iterable. Each element must be a dict with keys "template" and "search",
                which will be used to batch data.
            batch_size (int):
        """
        self.dataset = dataset
        self.batch_size = batch_size
        # Hard-coded four aspect ratio groups
        # Can add support for more aspect ratio groups, but doesn't seem useful
        self._buckets = {
            0: {True: [], False: []},  # template w > h and search w > h
            1: {True: [], False: []},  # template w > h and search w < h
            2: {True: [], False: []},  # template w < h and search w > h
            3: {True: [], False: []},  # template w < h and search w < h
        }

    def __iter__(self):
        for d in self.dataset:
            template = d["template"]
            if isinstance(d["search"], list):
                search = d["search"][0]  # all search images have to be of the same size
            else:
                search = d["search"]
            _, tw, th = template["image"].shape
            _, sw, sh = search["image"].shape
            if tw > th and sw > sh:
                bucket_id = 0
            elif tw > th and sw < sh:
                bucket_id = 1
            elif tw < th and sw > sh:
                bucket_id = 2
            else:
                bucket_id = 3
            # bucket_id = 0 if w > h else 1
            bucket = self._buckets[bucket_id]
            has_segmentation = all(
                [x["instances"].has("gt_masks") for x in d["search"]]
            )
            bucket = bucket[has_segmentation]
            bucket.append(d)
            if len(bucket) == self.batch_size:
                yield bucket[:]
                del bucket[:]
