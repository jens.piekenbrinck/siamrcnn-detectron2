import numpy as np
from detectron2.data.transforms import Transform, NoOpTransform, TransformGen


class ColorTransform(Transform):
    def __init__(self, rgbVar):
        self.rgbVar = rgbVar

    def apply_image(self, img: np.ndarray) -> np.ndarray:
        is_uint8 = img.dtype == np.uint8
        if is_uint8:
            img = img.astype(np.float32)
        offset = np.dot(self.rgbVar, np.random.randn(3, 1))
        offset = offset[::-1]  # rgb 2 bgr
        offset = offset.reshape(3)
        img = img - offset
        if is_uint8:
            return np.clip(img, 0, 255).astype(np.uint8)
        else:
            return img

    def apply_coords(self, coords: np.ndarray) -> np.ndarray:
        """
        Apply no transform on the coordinates.
        """
        return coords

    def apply_segmentation(self, segmentation: np.ndarray) -> np.ndarray:
        """
        Apply no transform on the full-image segmentation.
        """
        return segmentation

    def inverse(self) -> Transform:
        """
        The inverse is a no-op.
        """
        return NoOpTransform()


class RandomColor(TransformGen):
    def __init__(self, rgbVar, fraction=0.25):
        super().__init__()
        self.rgbVar = rgbVar
        self.fraction = fraction

    def get_transform(self, img):
        if np.random.rand() < self.fraction:
            return ColorTransform(self.rgbVar)
        else:
            return NoOpTransform()
