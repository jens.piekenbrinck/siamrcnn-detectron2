import cv2
import numpy as np
from detectron2.data.transforms import Transform, NoOpTransform, TransformGen


def _generate_motion_blur_kernel(min_size, max_size):
    kernel_size = np.random.randint(min_size, max_size)
    kernel = np.zeros((kernel_size, kernel_size))
    kernel[int((kernel_size - 1) / 2), :] = np.ones(kernel_size)
    kernel = kernel / kernel_size
    return kernel


def apply_motion_blur(img, min_size, max_size):
    kernel = _generate_motion_blur_kernel(min_size, max_size)
    return cv2.filter2D(img, -1, kernel)


class RandomMotionBlurTransform(Transform):
    def __init__(self, fraction, kernel_size_range):
        super().__init__()
        self.fraction = fraction
        self.kernel_size_range = kernel_size_range

    def apply_image(self, img: np.ndarray) -> np.ndarray:
        if np.random.rand() < self.fraction:
            return apply_motion_blur(img, *self.kernel_size_range)
        else:
            return img

    def apply_coords(self, coords: np.ndarray) -> np.ndarray:
        """
        Apply no transform on the coordinates.
        """
        return coords

    def apply_segmentation(self, segmentation: np.ndarray) -> np.ndarray:
        """
        Apply no transform on the full-image segmentation.
        """
        return segmentation

    def inverse(self) -> Transform:
        """
        The inverse is a no-op.
        """
        return NoOpTransform()


class RandomMotionBlur(TransformGen):
    def __init__(self, fraction, kernel_size_range):
        super().__init__()
        self.fraction = fraction
        self.kernel_size_range = kernel_size_range

    def get_transform(self, img):
        return RandomMotionBlurTransform(self.fraction, self.kernel_size_range)
