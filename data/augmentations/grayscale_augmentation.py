import numpy as np
from detectron2.data.transforms import Transform, NoOpTransform, TransformGen


class GrayscaleTransform(Transform):
    def apply_image(self, img: np.ndarray) -> np.ndarray:
        is_uint8 = img.dtype == np.uint8
        if is_uint8:
            img = img.astype(np.float32)
        img = np.repeat(img.dot([0.299, 0.587, 0.114])[:, :, np.newaxis], 3, axis=-1)
        if is_uint8:
            return np.clip(img, 0, 255).astype(np.uint8)
        else:
            return img

    def apply_coords(self, coords: np.ndarray) -> np.ndarray:
        """
        Apply no transform on the coordinates.
        """
        return coords

    def apply_segmentation(self, segmentation: np.ndarray) -> np.ndarray:
        """
        Apply no transform on the full-image segmentation.
        """
        return segmentation

    def inverse(self) -> Transform:
        """
        The inverse is a no-op.
        """
        return NoOpTransform()


class RandomGrayscale(TransformGen):
    def __init__(self, fraction=.25):
        super().__init__()
        self.fraction = fraction

    def get_transform(self, img):
        if np.random.rand() < self.fraction:
            return GrayscaleTransform()
        else:
            return NoOpTransform()
