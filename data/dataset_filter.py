from abc import ABCMeta, abstractmethod
from typing import List, Tuple

from detectron2.structures import BoxMode


class DatasetFilter(metaclass=ABCMeta):
    @abstractmethod
    def __call__(self, dataset: List[dict]) -> List[dict]:
        pass


class NoOpFilter(DatasetFilter):
    def __call__(self, dataset: List[dict]) -> List[dict]:
        return dataset


class FrameLengthFilter(DatasetFilter):
    """
    Filter an item based on amount of frames.
    """

    def __init__(self, min_length: int):
        self.min_length = min_length

    def __call__(self, dataset: List[dict]) -> List[dict]:
        return [data for data in dataset if len(data["frames"]) >= self.min_length]


class SiamLimitedTrackFilter(DatasetFilter):
    def __init__(self, min_length: int, min_time_step: int):
        self.min_length = min_length
        self.min_time_step = min_time_step

    def filter(self, dataset_dict: dict) -> bool:
        res = []
        for frame in dataset_dict["frames"]:
            if len(res) == 0 or res[-1]["time"] + self.min_time_step <= frame["time"]:
                res.append(frame)
        return len(res) >= self.min_length + 1

    def __call__(self, dataset: List[dict]) -> List[dict]:
        return [data for data in dataset if self.filter(data)]


class FilterList(DatasetFilter):
    def __init__(self, filters: List[DatasetFilter]):
        self.filters = filters

    def __call__(self, dataset: List[dict]) -> List[dict]:
        result = dataset
        for f in self.filters:
            result = f(result)
            if len(result) == 0:
                break
        return result


class BBoxFilter(DatasetFilter):
    def __init__(self, min_size: Tuple[float, float]):
        self.min_size = min_size

    def filter(self, dataset_dict: dict) -> bool:
        for frame in dataset_dict["frames"]:
            for obj in frame["annotations"]:
                box = BoxMode.convert(
                    obj["bbox"], from_mode=obj["bbox_mode"], to_mode=BoxMode.XYXY_ABS
                )
                x1, y1, x2, y2 = box
                if x2 - x1 < self.min_size[0] or y2 - y1 < self.min_size[1]:
                    return False
        return True

    def __call__(self, dataset: List[dict]) -> List[dict]:
        return [data for data in dataset if self.filter(data)]


class TrackLengthFilter(DatasetFilter):
    def __init__(self, min_track_length: int):
        self.min_track_length = min_track_length

    def filter(self, dataset_dict: dict) -> bool:
        assert "frames" in dataset_dict, "dataset_dict has to contain frames key"
        assert isinstance(dataset_dict["frames"], list), "frames has to be a list"

        tracks = {}
        for frame in dataset_dict["frames"]:
            for anno in frame["annotations"]:
                assert (
                    "obj_id" in anno
                ), f"missing object id 'obj_id: int' in frame: {frame}"
                if anno["obj_id"] in tracks:
                    tracks[anno["obj_id"]] += 1
                else:
                    tracks[anno["obj_id"]] = 1

        tracks = [v for v in tracks.values() if v >= self.min_track_length]
        return len(tracks) > 0

    def __call__(self, dataset: List[dict]) -> List[dict]:
        return [data for data in dataset if self.filter(data)]
