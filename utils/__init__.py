from termcolor import colored
from inspect import signature
from contextlib import contextmanager
from time import time
from datetime import timedelta
import torch

from detectron2.config import CfgNode


def extend_config(cfg):
    # Add custom config properties which can be then added to yaml files
    cfg.MODEL.PROPOSAL_GENERATOR.FREEZE = False

    cfg.MODEL.USE_PREVIOUS_PREDICTION = False
    cfg.MODEL.REPLACE_TOP_INSTANCE_WITH_GT_IOU_THRESH = -1.0

    cfg.MODEL.ROI_HEADS.ENABLED = True
    cfg.MODEL.ROI_HEADS.FREEZE = False

    cfg.DATALOADER.MASK_GROUPING = False

    cfg.DATASETS.SAMPLER = CfgNode()
    cfg.DATASETS.SAMPLER.SAMPLING_SIZE_TRAIN = 1
    cfg.DATASETS.SAMPLER.SAMPLING_SIZE_TEST = 12
    cfg.DATASETS.SAMPLER.SAMPLING_RANGE = 100
    cfg.DATASETS.SAMPLER.SEQUENTIAL = True
    cfg.DATASETS.SAMPLER.ALLOW_TEMPLATE_REUSE = True
    cfg.DATASETS.SAMPLER.DISTRIBUTION = "random"
    cfg.DATASETS.NEGATIVE_SAMPLING_RATIO = 0.0

    return cfg


def freeze(module: torch.nn.Module):
    for p in module.parameters():
        p.requires_grad = False


def model_summary(model: torch.nn.Module, colorized: bool = False) -> str:
    max_name_len = max([len(name) for name, _ in model.named_parameters()])
    max_size_len = max(
        [len(str(list(param.data.shape))) for _, param in model.named_parameters()]
    )
    max_param_len = max(
        [
            len("{0:,}".format(param.data.numel()))
            for _, param in model.named_parameters()
        ]
    )

    line_template = f"{{:>{max_name_len}}}   {{:>{max_size_len}}}   {{:>{max_param_len}}}  {{:>{len('Trainable')}}}"
    line_template_len = len(
        line_template.format(
            " " * max_name_len,
            " " * max_size_len,
            " " * max_param_len,
            " " * len("Trainable"),
        )
    )

    summary_str = ""
    summary_str += "-" * line_template_len + "\n"
    line_new = line_template.format(
        "Param Name", "Output Shape", "Param #", "Trainable"
    )
    summary_str += line_new + "\n"
    summary_str += "=" * line_template_len + "\n"
    total_params = 0
    trainable_params = 0

    def colorize(string: str, color: str, colorized: bool) -> str:
        if colorized:
            return colored(string, color)
        else:
            return string

    for name, param in model.named_parameters():
        output_size = param.data.numel()
        total_params += output_size
        if param.requires_grad:
            trainable_params += output_size
            line_new = line_template.format(
                name,
                str(list(param.data.shape)),
                "{0:,}".format(output_size),
                str(True),
            )
            summary_str += colorize(line_new, "green", colorized) + "\n"
        else:
            line_new = line_template.format(
                name,
                str(list(param.data.shape)),
                "{0:,}".format(output_size),
                str(False),
            )
            # summary_str += "\033[%dm%s\033[0m" % (90, line_new + "\n")
            summary_str += colorize(line_new, "cyan", colorized) + "\n"

    summary_str += "=" * line_template_len + "\n"
    summary_str += "Total params: {0:,}".format(total_params) + "\n"
    summary_str += (
        colorize("Trainable params: {0:,}".format(trainable_params), "green", colorized)
        + "\n"
    )
    summary_str += (
        colorize(
            "Non-trainable params: {0:,}".format(total_params - trainable_params),
            "cyan",
            colorized,
        )
        + "\n"
    )
    summary_str += "-" * line_template_len + "\n"
    return summary_str


def freeze_proposal_generator(proposal_generator: torch.nn.Module, **kwargs):
    has_freeze = getattr(proposal_generator, "freeze", None) is not None
    if has_freeze:
        proposal_generator.freeze(**kwargs)
    else:
        freeze(proposal_generator)


def has_context_param(module: torch.nn.Module) -> bool:
    return (
        signature(getattr(module, "forward", None)).parameters.get("context")
        is not None
    )


def is_siamese(module: torch.nn.Module) -> bool:
    params = signature(getattr(module, "forward", None)).parameters
    return (
        params.get("features") is not None
        and params.get("template_features") is not None
    )


def index(obj, i):
    if obj is None:
        return None
    if isinstance(obj, list):
        if isinstance(i, list) or isinstance(i, torch.LongTensor):
            return [obj[j] for j in i]
        if isinstance(i, torch.BoolTensor):
            return [obj[j] for j, m in enumerate(i) if m]
    elif isinstance(obj, tuple):
        if isinstance(i, list) or isinstance(i, torch.LongTensor):
            return (obj[j] for j in i)
        if isinstance(i, torch.BoolTensor):
            return (obj[j] for j, m in enumerate(i) if m)
    return obj[i]


@contextmanager
def timing(name: str) -> None:
    start = time()
    yield
    elapsed = time() - start

    print(f"{name}: {timedelta(seconds=elapsed)}")
