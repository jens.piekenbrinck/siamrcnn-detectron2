import detectron2.utils.comm as comm
from detectron2.config import get_cfg
from detectron2.utils.logger import setup_logger
from detectron2.engine import default_argument_parser, default_setup, launch

from data.datasets.dataset_utils import register_datasets
from engine import SiamTrainer
from modeling import *  # used for REGISTRY

from utils import extend_config, model_summary

# https://github.com/pytorch/pytorch/issues/973#issuecomment-346405667
import resource

rlimit = resource.getrlimit(resource.RLIMIT_NOFILE)
resource.setrlimit(
    resource.RLIMIT_NOFILE, (4096 if rlimit[0] < 4096 else rlimit[0], rlimit[1])
)


def setup(args, output_dir=None):
    cfg = get_cfg()

    cfg = extend_config(cfg)

    cfg.merge_from_file(args.config_file)
    cfg.merge_from_list(args.opts)

    if output_dir:
        cfg.OUTPUT_DIR = output_dir

    return cfg


def main(args):
    cfg = setup(args)

    register_datasets(cfg)

    cfg.freeze()
    default_setup(cfg, args)

    trainer = SiamTrainer(cfg)
    trainer.resume_or_load(resume=args.resume)

    summary = model_summary(trainer.model)
    rank = comm.get_rank()
    logger = setup_logger(output=cfg.OUTPUT_DIR, distributed_rank=rank, name="siam")
    logger.info("Model Parameter Summary:\n%s" % summary)

    return trainer.train()


if __name__ == "__main__":
    parser = default_argument_parser()
    args = parser.parse_args()
    print("Command Line Args:", args)
    launch(
        main,
        args.num_gpus,
        num_machines=args.num_machines,
        machine_rank=args.machine_rank,
        dist_url=args.dist_url,
        args=(args,),
    )
