from typing import List
import torch


def merge_loss_dicts(loss_dicts: List[dict]) -> dict:
    # has to be merged for all timesteps
    # maybe with custom merge-strategy like time-dependent weighting
    loss_dict = {}
    for d in loss_dicts:
        for k, v in d.items():
            if k in loss_dict:
                loss_dict[k].append(v)
            else:
                loss_dict[k] = [v]

    loss_dict = {k:torch.mean(torch.stack(v)) for k, v in loss_dict.items()}
    return loss_dict
