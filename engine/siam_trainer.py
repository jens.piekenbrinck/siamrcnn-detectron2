import os
import logging
from typing import List
from pathlib import Path
import pickle

from detectron2.config import CfgNode
from detectron2.evaluation import DatasetEvaluators
from detectron2.engine import DefaultTrainer
from detectron2.data import DatasetCatalog

from data import (
    SiamDatasetMapper,
    SiamFrameSampler,
    SiamRCNNSubDataset,
    SiamRCNNDataset,
)
from data.datasets.dataset_utils import filter_invalid_or_only_crowd_annotations
from data.datasets.build import (
    build_detection_train_loader,
    build_detection_test_loader,
)
from evaluation.iou_evaluator import IoUEvaluator
from evaluation.visualization_evaluator import VisualizationEvaluator


def build_dataset(dataset_config: dict):
    logger = logging.getLogger("detectron2")
    subdatasets = []
    for dataset_name, dataset_cfg in dataset_config["subdatasets"].items():
        num_use = dataset_cfg.get("num_use", 0)
        repeat = dataset_cfg.get("repeat", 1)
        cache_dir = "./cache/datasets/siam_trainer"
        Path(cache_dir).mkdir(parents=True, exist_ok=True)
        cache_path = os.path.join(cache_dir, f"{dataset_name}.pkl")
        if os.path.exists(cache_path):
            logger.info("{} -> load from cache: {}".format(dataset_name, cache_path))
            dataset_dicts = pickle.load(open(cache_path, "rb"))["videos"]
        else:
            # TODO register required datasets here
            dataset_dicts = DatasetCatalog.get(dataset_name)
            logger.info(
                "{} -> filter and caching of {} items".format(
                    dataset_name, len(dataset_dicts)
                )
            )
            dataset_dicts = filter_invalid_or_only_crowd_annotations(dataset_dicts)
            pickle.dump(
                {"videos": dataset_dicts}, open(cache_path, "wb")
            )  # only save videos
            logger.info("{} -> cached saved to {}".format(dataset_name, cache_path))
        subdatasets.append(
            SiamRCNNSubDataset(
                dataset_name,
                dataset_dicts,
                sampler=dataset_cfg["sampler"],
                num_use=num_use,
                repeat=repeat,
            )
        )
    dataset = SiamRCNNDataset(
        subdatasets,
        num_use=dataset_config.get("num_use", 0),
        repeat=dataset_config.get("repeat", 1),
        negative=dataset_config.get("negative", 0.0),
    )
    return dataset


def build_dataset_config(cfg: CfgNode, dataset_names: List[str], is_train: bool):
    dataset_configs = {}
    sampler = SiamFrameSampler(
        max_count=cfg.DATASETS.SAMPLER.SAMPLING_SIZE_TRAIN
        if is_train
        else cfg.DATASETS.SAMPLER.SAMPLING_SIZE_TEST,
        max_frame_range=cfg.DATASETS.SAMPLER.SAMPLING_RANGE
        if is_train
        else cfg.DATASETS.SAMPLER.SAMPLING_SIZE_TEST,  # 100 if is_train
        sequential=cfg.DATASETS.SAMPLER.SEQUENTIAL,
        allow_template_reuse=cfg.DATASETS.SAMPLER.ALLOW_TEMPLATE_REUSE,
        distribution=cfg.DATASETS.SAMPLER.DISTRIBUTION,
    )
    for dataset_name in dataset_names:
        if dataset_name.startswith("youtube_bb"):
            dataset_configs[dataset_name] = {
                "sampler": SiamFrameSampler(
                    max_count=cfg.DATASETS.SAMPLER.SAMPLING_SIZE_TRAIN
                    if is_train
                    else cfg.DATASETS.SAMPLER.SAMPLING_SIZE_TEST,
                    max_frame_range=cfg.DATASETS.SAMPLER.SAMPLING_RANGE // 30
                    if is_train
                    else cfg.DATASETS.SAMPLER.SAMPLING_SIZE_TEST,  # 3 if is_train
                    sequential=cfg.DATASETS.SAMPLER.SEQUENTIAL,
                    allow_template_reuse=cfg.DATASETS.SAMPLER.ALLOW_TEMPLATE_REUSE,
                    distribution=cfg.DATASETS.SAMPLER.DISTRIBUTION,
                )
            }
        elif dataset_name.startswith("imagenet_vid"):
            dataset_configs[dataset_name] = {
                # ImageNetVID is small compared to other datasets, therefore it is repeated multiple times so it is sampled more often
                "repeat": 25,
                "sampler": sampler,
            }
        else:
            dataset_configs[dataset_name] = {"sampler": sampler}
    return {
        "num_use": 0 if is_train else 0,
        "repeat": 1 if is_train else 1,
        "negative": cfg.DATASETS.NEGATIVE_SAMPLING_RATIO if is_train else 0,
        "subdatasets": dataset_configs,
    }


class SiamTrainer(DefaultTrainer):
    @classmethod
    def build_train_loader(cls, cfg: CfgNode):
        dataset_config = build_dataset_config(
            cfg=cfg, dataset_names=cfg.DATASETS.TRAIN, is_train=True
        )
        mapper = SiamDatasetMapper(cfg=cfg, is_train=True)
        dataset = build_dataset(dataset_config=dataset_config)
        return build_detection_train_loader(cfg=cfg, dataset=dataset, mapper=mapper)

    @classmethod
    def build_test_loader(cls, cfg: CfgNode, dataset_name: str):
        dataset_config = build_dataset_config(
            cfg=cfg, dataset_names=[dataset_name], is_train=False
        )
        mapper = SiamDatasetMapper(
            cfg=cfg,
            is_train=False,
            keep_annotations=True,  # important to get instances for frames
        )
        dataset = build_dataset(dataset_config=dataset_config)
        return build_detection_test_loader(cfg=cfg, dataset=dataset, mapper=mapper)

    @classmethod
    def build_evaluator(cls, cfg, dataset_name):
        return DatasetEvaluators(
            [
                IoUEvaluator(
                    dataset_name=dataset_name,
                    cfg=cfg,
                    distributed=False,
                    output_dir=None,
                ),
                VisualizationEvaluator(dataset_name=dataset_name, cfg=cfg),
            ]
        )


if __name__ == "__main__":
    from detectron2.config import get_cfg
    from detectron2.utils.logger import setup_logger

    from data.datasets.videos import (
        ImageNetVIDVideoDataset,
        ImageNetDETVideoDataset,
        YouTubeBBVideoDataset,
        COCOVideoDataset,
        OTBVideoDataset,
        YouTubeVOSVideoDataset,
        GOT10kVideoDataset,
        LaSOTVideoDataset,
    )
    from utils import extend_config

    setup_logger()

    print("Registering datasets...")
    ImageNetVIDVideoDataset.register(subsets=set(["train"]))
    print("ImageNetVID registered.")
    ImageNetDETVideoDataset.register(subsets=set(["train"]))
    print("ImageNetDET registered.")
    # YouTubeBBVideoDataset.register(subsets=set(["train"]))
    YouTubeVOSVideoDataset.register(subsets=set(["train"]))
    print("YouTubeVOS registered.")
    GOT10kVideoDataset.register(subsets=set(["train"]))
    print("GOT10k registered.")
    COCOVideoDataset.register(subsets=set(["train"]), versions=set(["2017"]))
    print("COCO registered.")
    LaSOTVideoDataset.register(subsets=set(["train"]))
    print("LaSOT registered.")
    OTBVideoDataset.register()
    print("OTB registered.")
    print("Registering done!")

    cfg = get_cfg()
    cfg = extend_config(cfg)
    cfg.merge_from_file("configs/siam_rcnn/baseline.yaml")
    cfg.DATASETS.TRAIN = (
        "imagenet_vid_video_train",
        "imagenet_det_video_train",
        "coco_video_2017_train",
        "youtube_vos_video_train",
        "got10k_video_train",
        "lasot_video_train",
    )  # youtube_bb_video_train
    cfg.DATASETS.TEST = ("otb_video",)

    print("SiamTrainer > build_train_loader ...")
    SiamTrainer.build_train_loader(cfg=cfg)
    print("SiamTrainer > build_train_loader done!")
    for dataset_name in cfg.DATASETS.TEST:
        print("SiamTrainer > build_test_loader ...")
        SiamTrainer.build_test_loader(cfg=cfg, dataset_name=dataset_name)
        print("SiamTrainer > build_test_loader done!")
