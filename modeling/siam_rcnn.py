from typing import List, Optional, Tuple, Dict, Union
import itertools
import numpy as np
import torch
from torch import nn

from detectron2.config import configurable, CfgNode
from detectron2.structures import ImageList, Instances
from detectron2.utils.events import get_event_storage
from detectron2.data.detection_utils import convert_image_to_rgb
from detectron2.modeling import META_ARCH_REGISTRY
from detectron2.modeling.backbone import build_backbone, Backbone
from detectron2.modeling.postprocessing import detector_postprocess
from detectron2.modeling.proposal_generator import build_proposal_generator
from detectron2.modeling.roi_heads import build_roi_heads

from engine.train_utils import merge_loss_dicts
from modeling.template_extractor import TemplateExtractor
import utils

__all__ = ["SiamRCNNContext", "SiamRCNN"]


class SiamRCNNContext:
    """
    SiamRCNNContext stores the state of `SiamRCNN` .
    """

    def __init__(
        self,
        gt_template: Optional[Dict[str, Union[torch.Tensor, List[torch.Tensor]]]],
        ff_predictions=None,
    ):
        self.gt_template = gt_template
        self.ff_predictions = ff_predictions

    def __getitem__(self, i):
        return SiamRCNNContext(
            gt_template={k: utils.index(v, i) for k, v in self.gt_template.items()}
            if self.gt_template is not None
            else None,
            ff_predictions=utils.index(self.ff_predictions, i),
        )


@META_ARCH_REGISTRY.register()
class SiamRCNN(nn.Module):
    """
    Siam R-CNN model.
    """

    @configurable
    def __init__(
        self,
        *,
        backbone: Backbone,
        proposal_generator: nn.Module,
        roi_heads: nn.Module,
        template_extractor: TemplateExtractor,
        pixel_mean: Tuple[float],
        pixel_std: Tuple[float],
        input_format: Optional[str] = None,
        vis_period: int = 0,
        frozen_proposal_generator: bool = False,
        use_previous_prediction: bool = False,
    ):
        super().__init__()

        self.backbone = backbone

        self.proposal_generator = proposal_generator
        assert self.proposal_generator is not None, "needs a RPN"
        self.frozen_proposal_generator = frozen_proposal_generator

        self.roi_heads = roi_heads

        self.input_format = input_format
        self.vis_period = vis_period
        if vis_period > 0:
            assert (
                input_format is not None
            ), "input_format is required for visualization!"

        self.register_buffer(
            "pixel_mean", torch.Tensor(pixel_mean).view(-1, 1, 1), False
        )
        self.register_buffer("pixel_std", torch.Tensor(pixel_std).view(-1, 1, 1), False)
        assert (
            self.pixel_mean.shape == self.pixel_std.shape
        ), f"{self.pixel_mean} and {self.pixel_std} have different shapes!"

        self.template_extractor = template_extractor

        self.use_previous_prediction = use_previous_prediction

        self._tracklet_distance_threshold = 0.08

    @classmethod
    def from_config(cls, cfg: CfgNode):
        backbone = build_backbone(cfg)
        if cfg.MODEL.BACKBONE.FREEZE_AT >= 6:
            utils.freeze(backbone)

        input_shape = backbone.output_shape()
        proposal_generator = build_proposal_generator(cfg, input_shape=input_shape)
        if cfg.MODEL.PROPOSAL_GENERATOR.FREEZE:
            utils.freeze_proposal_generator(proposal_generator)

        roi_heads = build_roi_heads(cfg, input_shape=input_shape)

        template_extractor = TemplateExtractor(
            in_features=cfg.MODEL.ROI_HEADS.IN_FEATURES,
            pooler_resolution=cfg.MODEL.ROI_BOX_HEAD.POOLER_RESOLUTION,
            sampling_ratio=cfg.MODEL.ROI_BOX_HEAD.POOLER_SAMPLING_RATIO,
            pooler_type=cfg.MODEL.ROI_BOX_HEAD.POOLER_TYPE,
            input_shape=input_shape,
        )

        return {
            "backbone": backbone,
            "proposal_generator": proposal_generator,
            "roi_heads": roi_heads,
            "template_extractor": template_extractor,
            "input_format": cfg.INPUT.FORMAT,
            "vis_period": cfg.VIS_PERIOD,
            "pixel_mean": cfg.MODEL.PIXEL_MEAN,
            "pixel_std": cfg.MODEL.PIXEL_STD,
            "frozen_proposal_generator": cfg.MODEL.PROPOSAL_GENERATOR.FREEZE,
            "use_previous_prediction": cfg.MODEL.USE_PREVIOUS_PREDICTION,
        }

    @property
    def device(self):
        return self.pixel_mean.device

    def visualize_training(
        self,
        batched_inputs: List[dict],
        proposals: List[Instances],
        context: Optional[SiamRCNNContext] = None,
    ):
        """
        A function used to visualize images and proposals. It shows ground truth
        bounding boxes on the original image and up to 20 predicted object
        proposals on the original image. Users can implement different
        visualization functions for different models.
        Args:
            batched_inputs (list): a list that contains input to the model.
            proposals (list[list]): a list that contains predicted proposals of format (TxB). Both
                batched_inputs and proposals should have the same length.
            context (SiamContext): the current context of the model
            final_output (list[list]): a list that contains the final predictions of the network (after roi heads)
        """
        from detectron2.utils.visualizer import Visualizer

        storage = get_event_storage()
        max_vis_prop = 10
        scale = 0.5
        proposals = [
            [k for k in p if k is not None]
            for p in list(itertools.zip_longest(*proposals))
        ]

        for input, prop in zip(batched_inputs, proposals):
            res = []
            template_img = convert_image_to_rgb(
                input["template"]["image"].permute(1, 2, 0), self.input_format
            )
            # NOTE: currently only positive examples supported => template and search have to be of same size
            v_template = Visualizer(template_img, None, scale=scale)
            v_template = v_template.overlay_instances(
                boxes=input["template"]["instances"].gt_boxes,
                assigned_colors=[[0.0, 1.0, 0.0]]
                * len(input["template"]["instances"].gt_boxes),
            )
            template_img = v_template.get_image()

            for t, frame in enumerate(input["search"]):
                img = frame["image"]
                img = convert_image_to_rgb(img.permute(1, 2, 0), self.input_format)

                v_gt = Visualizer(img, None, scale=scale)
                v_gt = v_gt.overlay_instances(
                    boxes=frame["instances"].gt_boxes,
                    assigned_colors=[[0.0, 1.0, 0.0]]
                    * len(frame["instances"].gt_boxes),
                )
                anno_img = v_gt.get_image()

                box_size = min(len(prop[t].proposal_boxes), max_vis_prop)

                v_pred = Visualizer(img, None, scale=scale)
                v_pred = v_pred.overlay_instances(
                    boxes=prop[t].proposal_boxes[:box_size].tensor.cpu().numpy()
                )
                prop_img = v_pred.get_image()

                vis_img = np.concatenate((template_img, anno_img, prop_img), axis=1)
                vis_img = vis_img.transpose(2, 0, 1)
                res.append(vis_img)

            vis_name = "Left: Template;  Middle: GT bounding boxes;  Right: Predicted proposals before roi heads"
            vis_img = np.concatenate(res, axis=1)
            storage.put_image(vis_name, vis_img)
            break  # only visualize one video in a batch

    def get_template_features(
        self, batched_inputs: List[dict]
    ) -> Dict[str, Union[torch.Tensor, List[torch.Tensor]]]:
        images = self.preprocess_image(batched_inputs)
        template_features = self.backbone(images.tensor)

        assert "instances" in batched_inputs[0]
        gt_instances = [x["instances"].to(self.device) for x in batched_inputs]

        return self.template_extractor(template_features, gt_instances)

    def _init(
        self, batched_inputs: List[dict]
    ) -> Tuple[Dict[str, Union[torch.Tensor, List[torch.Tensor]]], List[dict]]:
        images = self.preprocess_image(batched_inputs)
        template_features = self.backbone(images.tensor)

        assert "instances" in batched_inputs[0]
        gt_instances = [x["instances"].to(self.device) for x in batched_inputs]

        gt_template_features: Dict[
            str, Union[torch.Tensor, List[torch.Tensor]]
        ] = self.template_extractor(template_features, gt_instances)

        proposals, _ = self.proposal_generator(
            images=images, features=template_features, gt_instances=None
        )

        roi_heads_result = self.roi_heads(
            images=images,
            features=template_features,
            proposals=proposals,
            targets=None,
            gt_template_features=gt_template_features,
        )

        results = roi_heads_result["predictions"]

        features = [template_features[f] for f in self.roi_heads.box_in_features]
        box_features: torch.Tensor = self.roi_heads.box_pooler(
            features, [x.pred_boxes for x in results]
        )
        box_features = box_features.split([len(x) for x in results])

        post_processed: List[dict] = SiamRCNN._postprocess(
            results, batched_inputs, images.image_sizes
        )
        for out, features in zip(post_processed, box_features):
            out["box_features"] = features

        return gt_template_features, post_processed

    def forward(
        self,
        batched_inputs: List[dict],
        context: Optional[SiamRCNNContext] = None,
    ):
        """
        Args:
            batched_inputs: a list, batched outputs of :class:`DatasetMapper` .
                Each item in the list contains the inputs for one video.
                For now, each item in the list is a list of dict for each timestep that contains:
                * image: Tensor, image in (C, H, W) format.
                * instances (optional): groundtruth :class:`Instances`
                * proposals (optional): :class:`Instances`, precomputed proposals.
                Other information that's included in the original dicts, such as:
                * "height", "width" (int): the output resolution of the model, used in inference.
                  See :meth:`postprocess` for details.
            context: optional context of type :class:`SiamRCNNContext` of last run or initial context.
                This object contains the "roi_aligned_template_features", therefore the features
                are cached and can be reused for every time step.
        Returns:
            list[dict]:
                Each dict is the output for one input image.
                The dict contains one key "instances" whose value is a :class:`Instances`.
                The :class:`Instances` object has the following keys:
                "pred_boxes", "pred_classes", "scores", "pred_masks", "pred_keypoints"
            SiamRCNNContext:
                New context for next run.
        """

        # prepare batched inputs, sort by sequence length
        sorted_batch_idx = np.argsort([len(x["search"]) for x in batched_inputs])[
            ::-1
        ].tolist()
        batched_inputs = sorted(
            batched_inputs, key=lambda x: len(x["search"]), reverse=True
        )

        if not self.training:
            result, context = self.inference(batched_inputs, context)
            return [result[i] for i in sorted_batch_idx], context[sorted_batch_idx]

        # currently unroll the input, the whole sequence as input is important to support e.g. 3D-Convs etc.
        data = [x["search"] for x in batched_inputs]
        assert all(
            isinstance(seq, list) and len(seq) > 0 for seq in data
        ), "all items in batch have to be of type list (sequence) and have to be non-empty"
        max_seq_len = len(data[0])
        negative = [x["negative"] for x in batched_inputs]

        loss_dicts = []
        vis_proposals = []
        context: SiamRCNNContext = (
            self.get_initial_context(batched_inputs) if context is None else context
        )
        for t in range(max_seq_len):
            batch = [item[t] for item in data if len(item) > t]
            context = context[: len(batch)]
            neg = negative[: len(batch)]

            search_images = self.preprocess_image(batch)
            if "instances" in batch[0]:
                gt_instances = [x["instances"].to(self.device) for x in batch]
            else:
                gt_instances = None

            search_features = self.backbone(search_images.tensor)

            proposals, proposal_losses = self.proposal_generator(
                images=search_images,
                features=search_features,
                gt_instances=gt_instances,
            )

            vis_proposals.append(proposals)

            # if self.use_previous_prediction and context.last_prediction is not None:
            #     proposals = append_previous_prediction(proposals, context.last_prediction)

            roi_heads_result = self.roi_heads(
                images=search_images,
                features=search_features,
                proposals=proposals,
                targets=gt_instances,
                gt_template_features=context.gt_template,
                negative=neg,
            )

            detector_losses = roi_heads_result["losses"]

            losses = {}
            losses.update(detector_losses)
            losses.update(proposal_losses)

            loss_dicts.append(losses)

            context = SiamRCNNContext(gt_template=context.gt_template)

        if self.vis_period > 0:
            storage = get_event_storage()
            if storage.iter % self.vis_period == 0:
                self.visualize_training(batched_inputs, vis_proposals, context)

        # merge losses
        loss_dict = merge_loss_dicts(loss_dicts)

        return loss_dict

    def inference(
        self,
        batched_inputs: List[dict],
        context: SiamRCNNContext,
        detected_instances: Optional[List[Instances]] = None,
        do_postprocess: bool = True,
    ):
        """
        Run inference on the given inputs.
        Args:
            batched_inputs (list[dict]): same as in :meth:`forward`
            context (SiamContext): same as in :meth:`forward`
            detected_instances (None or list[list[Instances]]): if not None, it
                contains an `Instances` object per image. The `Instances`
                object contains "pred_boxes" and "pred_classes" which are
                known boxes in the image.
                The inference will then skip the detection of bounding boxes,
                and only predict other per-ROI outputs.
            do_postprocess (bool): whether to apply post-processing on the outputs.
        Returns:
            When do_postprocess=True, same as in :meth:`forward`.
            Otherwise, a list[Instances] containing raw network outputs.
        """
        assert not self.training

        # currently unroll the input, the whole sequence as input is important to support e.g. 3D-Convs etc.
        data = [x["search"] for x in batched_inputs]
        assert all(isinstance(seq, list) and len(seq) == 1 for seq in data), (
            "all items in batch have to be of type list (sequence) and have to contain exactly one search image",
            data,
        )

        res = []
        context: SiamRCNNContext = (
            self.get_initial_context(batched_inputs) if context is None else context
        )
        batch = [item[0] for item in data]
        context = context[: len(batch)]

        search_images = self.preprocess_image(batch)

        search_features = self.backbone(search_images.tensor)

        results = None
        if detected_instances is None:
            proposals, _ = self.proposal_generator(
                images=search_images,
                features=search_features,
                gt_instances=None,
            )

            """
            if self.use_previous_prediction and context.last_prediction is not None:
                proposals = append_previous_prediction(proposals, context.last_prediction)
            """

            roi_heads_result = self.roi_heads(
                images=search_images,
                features=search_features,
                proposals=proposals,
                targets=None,
                gt_template_features=context.gt_template,
            )
            results = roi_heads_result["predictions"]
        else:
            local_detected_instances = [x.to(self.device) for x in detected_instances]
            results = self.roi_heads.forward_with_given_boxes(
                search_features, local_detected_instances
            )

        third_stage_result = (
            self._run_third_stage(
                predictions=results,
                search_features=search_features,
                ff_gt_tracklet_features=[
                    x["tracklets"]["ff_gt_tracklet_features"].to(self.device)
                    for x in batch
                ],
                ff_distractor_features=[
                    x["tracklets"]["ff_distractor_features"].to(self.device)
                    for x in batch
                ],
                ff_distractor_tracklets_features=[
                    x["tracklets"]["ff_distractor_tracklets_features"].to(self.device)
                    for x in batch
                ],
                active_tracklets_features=[
                    x["tracklets"]["active_tracklets_features"].to(self.device)
                    for x in batch
                ],
                active_tracklets_boxes=[
                    x["tracklets"]["active_tracklets_boxes"].to(self.device)
                    for x in batch
                ],
            )
            if batch[0].get("tracklets", None) is not None
            else None
        )

        context = SiamRCNNContext(
            gt_template=context.gt_template,
            ff_predictions=context.ff_predictions,
        )

        if do_postprocess:
            assert (
                not torch.jit.is_scripting()
            ), "Scripting is not supported for postprocess."
            post_processed = SiamRCNN._postprocess(
                results, batch, search_images.image_sizes
            )
            # post_processed = [{k:v.to("cpu") if k == "instances" else v for k, v in x.items()} for x in post_processed]
            if third_stage_result is not None:
                for i, out in enumerate(post_processed):
                    for k, v in third_stage_result.items():
                        out[k] = v[i]
            res.append(post_processed)
        else:
            res.append([{"instances": inst} for inst in results])

        # swap axis (time x batch) to (batch x time)
        res = [
            [k for k in p if k is not None] for p in list(itertools.zip_longest(*res))
        ]
        return res, context

    def _run_third_stage(
        self,
        predictions: List[Instances],
        search_features: Dict[str, torch.Tensor],
        ff_gt_tracklet_features: List[torch.Tensor],
        ff_distractor_features: List[torch.Tensor],
        ff_distractor_tracklets_features: List[torch.Tensor],
        active_tracklets_features: List[torch.Tensor],
        active_tracklets_boxes: List[torch.Tensor],
    ) -> dict:
        """Run third stage of Siam R-CNN.

        Args:
            predictions (list[Instances]): predictions of current search frame
            search_features (dict[str, Tensor]): backbone features of search frame
            ff_gt_tracklet_features (list[Tensor]): batched list of first-frame ground-truth tracklet features, tensor i of shape (256, 7, 7)
            ff_distractor_features (list[Tensor]): batched list of first-frame distractor features, tensor i is of shape (Di, 256, 7, 7)
            ff_distractor_tracklets_features (list[Tensor]): batched list of first-frame distractor tracklets features, tensor i is of shape (Di, 256, 7, 7)
            active_tracklets_features (list[Tensor]): batched list of active tracklets features, tensor i is of shape (Ai, 256, 7, 7)
            active_tracklets_boxes (list[Tensor]): batched list of active tracklets boxes, tensor i is of shape (Ai, 4)

        Returns:
            (dict): contains following items
                - "box_features" (list[Tensor]): tensor of shape (B, 256, 7, 7)
                - "ff_gt_tracklet_scores" (list[Tensor]): similarity scores, tensor of shape (B, 1)
                - "ff_distractor_scores" (list[Tensor]): similarity scores, tensor of shape (D, 1)
                - "ff_distractor_tracklets_scores" (list[Tensor]): similarity scores, tensor of shape (D, 1)
                - "sparse_tracklet_scores" (list[Tensor]): similarity scores, tensor of shape (A, 1)
                - "tracklet_score_keep_mask" (List[BoolTensor]): similarity scores, tensor of shape (Ai,)
        """
        features = [search_features[f] for f in self.roi_heads.box_in_features]
        box_features: torch.Tensor = self.roi_heads.box_pooler(
            features, [x.pred_boxes for x in predictions]
        )

        _ff_gt_tracklet_features = torch.cat(
            [
                f.unsqueeze(0).repeat(r, 1, 1, 1)
                for f, r in zip(ff_gt_tracklet_features, [len(x) for x in predictions])
            ],
            dim=0,
        )  # (N x B, 256, 7, 7)

        ff_gt_tracklet_scores = self._run_cascade_stage(
            ref_features=_ff_gt_tracklet_features, det_features=box_features
        ).split(
            [len(x) for x in predictions]
        )  # (N x B, 1)

        _ff_distractor_features = torch.cat(
            [
                f.unsqueeze(1).repeat(1, r, 1, 1, 1).view(-1, *f.shape[1:])
                for f, r in zip(ff_distractor_features, [len(x) for x in predictions])
            ],
            dim=0,
        )  # (N x D x B, 256, 7, 7)

        box_features_per_batch: List[torch.Tensor] = box_features.split(
            [len(x) for x in predictions]
        )
        _box_features = torch.cat(
            [
                f.repeat(r, 1, 1, 1)
                for f, r in zip(
                    box_features_per_batch, [x.size(0) for x in ff_distractor_features]
                )
            ],
            dim=0,
        )  # (N x D x B, 256, 7, 7)

        ff_distractor_scores = self._run_cascade_stage(
            ref_features=_ff_distractor_features, det_features=_box_features
        ).split(
            [
                dist.size(0) * len(pred)
                for dist, pred in zip(ff_distractor_features, predictions)
            ]
        )  # (N x D x B, 1)

        ff_distractor_scores = [
            x.view(dist.size(0), len(pred), *x.shape[1:])
            for x, dist, pred in zip(
                ff_distractor_scores, ff_distractor_features, predictions
            )
        ]

        _ff_distractor_tracklets_features = torch.cat(
            [
                f.unsqueeze(1).repeat(1, r, 1, 1, 1).view(-1, *f.shape[1:])
                for f, r in zip(
                    ff_distractor_tracklets_features, [len(x) for x in predictions]
                )
            ],
            dim=0,
        )  # (N x D x B, 256, 7, 7)

        _box_features = torch.cat(
            [
                f.repeat(r, 1, 1, 1)
                for f, r in zip(
                    box_features_per_batch,
                    [x.size(0) for x in ff_distractor_tracklets_features],
                )
            ],
            dim=0,
        )  # (N x D x B, 256, 7, 7)

        ff_distractor_tracklets_scores = self._run_cascade_stage(
            ref_features=_ff_distractor_tracklets_features, det_features=_box_features
        ).split(
            [
                dist.size(0) * len(pred)
                for dist, pred in zip(ff_distractor_tracklets_features, predictions)
            ]
        )  # (N x D x B, 1)

        ff_distractor_tracklets_scores = [
            x.view(dist.size(0), len(pred), *x.shape[1:])
            for x, dist, pred in zip(
                ff_distractor_tracklets_scores,
                ff_distractor_tracklets_features,
                predictions,
            )
        ]

        keep_masks = self._distance_pruning(
            image_size=[x.image_size for x in predictions],
            ref_boxes=active_tracklets_boxes,
            det_boxes=[x.pred_boxes.tensor for x in predictions],
            tracklet_distance_threshold=self._tracklet_distance_threshold,
        )

        _active_tracklets_features = torch.cat(
            [
                f.unsqueeze(1).repeat(1, r, 1, 1, 1).view(-1, *f.shape[1:])[k.view(-1)]
                for f, r, k in zip(
                    active_tracklets_features, [len(x) for x in predictions], keep_masks
                )
            ],
            dim=0,
        )  # (N x A x B, 256, 7, 7)

        _box_features = torch.cat(
            [
                f.repeat(r, 1, 1, 1)[k.view(-1)]
                for f, r, k in zip(
                    box_features_per_batch,
                    [x.size(0) for x in active_tracklets_features],
                    keep_masks,
                )
            ],
            dim=0,
        )  # (N x A x B, 256, 7, 7)

        active_tracklet_scores: List[torch.Tensor] = self._run_cascade_stage(
            ref_features=_active_tracklets_features, det_features=_box_features
        ).split(
            [k.sum() for k in keep_masks]
        )  # (N x A x B, 1)

        return {
            "box_features": box_features_per_batch,
            "ff_gt_tracklet_scores": ff_gt_tracklet_scores,
            "ff_distractor_scores": ff_distractor_scores,
            "ff_distractor_tracklets_scores": ff_distractor_tracklets_scores,
            "active_tracklet_scores": active_tracklet_scores,
            "tracklet_score_keep_mask": keep_masks,
        }

    def _distance_pruning(
        self,
        image_size: List[Tuple[int, int]],
        ref_boxes: List[torch.Tensor],
        det_boxes: List[torch.Tensor],
        tracklet_distance_threshold: float,
    ) -> List[torch.BoolTensor]:
        """Prune boxes that have a distance to each other greater than a threshold.

        Args:
            image_size (List[Tuple[int, int]]): height and width of the images, used to normalize box coordinates
            ref_boxes (List[Tensor]): batched list of reference boxes, tensor i is of shape (Ai, 4)
            det_boxes (List[Tensor]): batched list of detection boxes, tensor i is of shape (Bi, 4)
            tracklet_distance_threshold (float): threshold for pruning boxes by distance

        Returns:
            (List[BoolTensor]): batched list of boolean masks which indicates which boxes to keep
        """
        keep_masks = []
        for image_size, ref_boxes, det_boxes in zip(image_size, ref_boxes, det_boxes):
            ref_wh = ref_boxes[:, 2:] - ref_boxes[:, :2]
            ref_center = ref_boxes[:, :2] + ref_wh / 2
            ref_cxcywh = torch.cat([ref_center, ref_wh], dim=1)

            det_wh = det_boxes[:, 2:] - det_boxes[:, :2]
            det_center = det_boxes[:, :2] + det_wh / 2
            det_cxcywh = torch.cat([det_center, det_wh], dim=1)

            norm = torch.Tensor(
                [image_size[1], image_size[0], image_size[1], image_size[0]]
            ).to(ref_cxcywh.device)

            # (#ref_boxes, #det_boxes, 4)
            diffs = torch.abs(
                ref_cxcywh.unsqueeze(1) - det_cxcywh.unsqueeze(0)
            ) / norm.unsqueeze(0).unsqueeze(0)
            keep = diffs < tracklet_distance_threshold
            keep = keep.all(dim=-1)  # (#ref_boxes, #det_boxes)
            keep_masks.append(keep)

        return keep_masks

    def _run_cascade_stage(
        self, ref_features: torch.Tensor, det_features: torch.Tensor
    ) -> torch.Tensor:
        """Run cascade stage but with slight modifications.
        Compute only the similarity scores and don't reuse previous stage predictions.

        Args:
            ref_features (Tensor): reference features of shape (N x B, 256, 7, 7)
            det_features (Tensor): detection features of shape (N x B, 256, 7, 7)

        Returns:
            (Tensor): similarity scores of shape (N x B, 1)
        """
        scores = []
        for stage in range(len(self.roi_heads.box_head)):
            features = torch.cat([det_features, ref_features], dim=1)
            box_features = self.roi_heads.siam_combiner[stage](features)

            box_features = self.roi_heads.box_head[stage](box_features)
            if box_features.dim() > 2:
                box_features = torch.flatten(box_features, start_dim=1)
            stage_scores = self.roi_heads.box_predictor[stage].cls_score(box_features)
            stage_scores = torch.softmax(stage_scores, dim=-1)
            scores.append(stage_scores)
            # probs = probs.split([len(p) for p in proposals], dim=0)

        scores = sum(scores) / len(scores)
        scores = scores[:, :-1]
        return scores

    def get_initial_context(self, batched_inputs: List[dict]) -> SiamRCNNContext:
        template_frames = [inp["template"] for inp in batched_inputs]
        template = None
        predictions = None
        if self.training:
            template = self.get_template_features(template_frames)
        else:
            template, predictions = self._init(template_frames)

        # transform ground-truth instances to predictions
        last_prediction = []
        for inp in batched_inputs:
            gt_inst = inp["template"]["instances"]
            pred = Instances(gt_inst.image_size)
            pred.pred_boxes = gt_inst.gt_boxes
            pred.scores = torch.ones(
                len(gt_inst),
                device=gt_inst.gt_boxes.tensor.device,
            )
            last_prediction.append(pred.to(self.device))

        return SiamRCNNContext(
            gt_template=template,
            ff_predictions=predictions
            # last_prediction=last_prediction
        )

    def preprocess_image(self, batched_inputs: List[dict]) -> ImageList:
        """
        Normalize, pad and batch the input images.
        """
        images = [x["image"].to(self.device) for x in batched_inputs]
        images = [(x - self.pixel_mean) / self.pixel_std for x in images]
        images = ImageList.from_tensors(images, self.backbone.size_divisibility)
        return images

    @staticmethod
    def _postprocess(
        instances: List[Instances],
        batched_inputs: List[dict],
        image_sizes: List[tuple],
    ) -> List[dict]:
        """
        Rescale the output instances to the target size.
        """
        # note: private function; subject to changes
        processed_results = []
        for results_per_image, input_per_image, image_size in zip(
            instances, batched_inputs, image_sizes
        ):
            height = input_per_image.get("height", image_size[0])
            width = input_per_image.get("width", image_size[1])
            r = detector_postprocess(results_per_image, height, width)
            processed_results.append({"instances": r})
        return processed_results
