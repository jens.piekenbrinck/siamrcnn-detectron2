from typing import List, Optional, Dict, Tuple, Union
import numpy as np
import torch
from torch import nn

from detectron2.config import configurable, CfgNode
from detectron2.layers import ShapeSpec, Conv2d
from detectron2.structures import Boxes, Instances, pairwise_iou, ImageList
from detectron2.utils.events import get_event_storage
from detectron2.modeling.proposal_generator.proposal_utils import (
    add_ground_truth_to_proposals,
)
from detectron2.modeling.roi_heads.cascade_rcnn import (
    CascadeROIHeads,
    _ScaleGradient,
    fast_rcnn_inference,
)
from detectron2.modeling.roi_heads.roi_heads import ROI_HEADS_REGISTRY


def filter_scores(
    scores_per_stage: List[List[torch.Tensor]],
    keep_mask_per_stage: List[List[torch.BoolTensor]],
) -> List[List[torch.Tensor]]:
    """
    Filter scores while training, because the shape of scores per stage can be different due to non-empty filtering.
    See method `_create_proposals_from_boxes()` for non-empty filtering.
    Args:
        scores_per_stage (list[list[Tensor]]): list of head scores containing list of length N with tensors of shape (Ri x (K+1)).
        keep_mask_per_stage (list[list[BoolTensor]]): list of length num_stages containing list of length batch_size.
            Each tensor is a mask of shape (Ri'), where Ri' can vary between stages which depends on previous filtering.
    Returns:
        list[list[Tensor]]: filtered scores per stage
    """
    # batch x head x (scores, keep)
    batches = [
        list(zip(*x)) for x in zip(zip(*scores_per_stage), zip(*keep_mask_per_stage))
    ]
    filtered_scores_per_stage = []
    for stages in batches:
        filtered = []
        for h, (scores, _) in enumerate(stages[:-1]):
            keep_inds = torch.arange(scores.shape[0])
            for mask in list(map(lambda x: x[-1], stages[h + 1 :])):
                keep_inds = keep_inds[mask]
            filtered.append(scores[keep_inds])
        # last stage can be added directly
        filtered.append(stages[-1][0])
        filtered_scores_per_stage.append(filtered)

    filtered_scores_per_stage = list(map(list, zip(*filtered_scores_per_stage)))
    return filtered_scores_per_stage


@ROI_HEADS_REGISTRY.register()
class SiamCascadeROIHeads(CascadeROIHeads):
    @configurable
    def __init__(
        self,
        *,
        siam_combiner: List[nn.Module],
        **kwargs,
    ):
        super().__init__(**kwargs)

        self.siam_combiner = nn.ModuleList(siam_combiner)

    @classmethod
    def from_config(cls, cfg: CfgNode, input_shape: Dict[str, ShapeSpec]):
        ret = super().from_config(cfg, input_shape)

        num_stages = len(ret["box_heads"])

        in_features = ret["box_in_features"]
        in_channels = [input_shape[f].channels for f in in_features]
        # Check all channel counts are equal
        assert len(set(in_channels)) == 1, in_channels
        in_channels = in_channels[0]

        siam_combiner = []
        for _ in range(num_stages):
            conv = Conv2d(
                in_channels=2 * in_channels,
                out_channels=in_channels,
                kernel_size=1,
                bias=True,
                norm=None,
                activation=None,
            )
            if hasattr(conv, "activation") and conv.activation is None:
                nn.init.xavier_normal_(conv.weight)
            else:
                nn.init.kaiming_normal_(conv.weight)
            if conv.bias is not None:
                nn.init.constant_(conv.bias, 0)
            siam_combiner.append(conv)

        ret.update(
            {
                "siam_combiner": siam_combiner,
            }
        )

        return ret

    def forward(
        self,
        *,
        images: ImageList,
        features: Dict[str, torch.Tensor],
        gt_template_features: Dict[str, Union[torch.Tensor, List[torch.Tensor]]],
        proposals: List[Instances],
        negative: Optional[List[bool]] = None,
        targets: Optional[List[Instances]] = None,
        **kwargs,
    ):
        del images
        assert all(
            [instances.proposal_boxes.tensor.shape[0] != 0 for instances in proposals]
        ), (targets, proposals)

        if self.training:
            proposals = self.label_and_sample_proposals_with_negatives(
                proposals, targets, negative
            )

        if self.training:
            losses, pred_instances = self._forward_box(
                features,
                gt_template_features,
                proposals,
                targets,
            )
            losses.update(self._forward_mask(features, proposals))
            losses.update(self._forward_keypoint(features, proposals))
            return {"predictions": pred_instances, "losses": losses}
        else:
            pred_instances = self._forward_box(
                features,
                gt_template_features,
                proposals,
            )
            pred_instances = self.forward_with_given_boxes(features, pred_instances)
            return {"predictions": pred_instances}

    def _forward_box(
        self,
        features: Dict[str, torch.Tensor],
        gt_template_features: Dict[str, Union[torch.Tensor, List[torch.Tensor]]],
        proposals: List[Instances],
        targets: Optional[List[Instances]] = None,
    ):
        """
        Args:
            features, targets: the same as in
                Same as in :meth:`ROIHeads.forward`.
            proposals (list[Instances]): the per-image object proposals with
                their matching ground truth.
                Each has fields "proposal_boxes", and "objectness_logits",
                "gt_classes", "gt_boxes".
        """
        features = [features[f] for f in self.box_in_features]
        gt_template_features = [gt_template_features[f] for f in self.box_in_features]
        head_outputs = []  # (predictor, predictions, proposals)
        prev_pred_boxes = None
        image_sizes = [x.image_size for x in proposals]
        head_keep_masks = []

        for k in range(self.num_cascade_stages):
            if k > 0:
                # The output boxes of the previous stage are used to create the input
                # proposals of the next stage.
                proposals, keep_mask = self._create_proposals_from_boxes(
                    prev_pred_boxes, image_sizes
                )
                if self.training:
                    proposals = self._match_and_label_boxes(proposals, k, targets)
            else:
                # TODO keep mask not needed for first stage
                keep_mask = [
                    torch.zeros([x.proposal_boxes.tensor.shape[0]]) == 0
                    for x in proposals
                ]

            assert all(
                [
                    instances.proposal_boxes.tensor.shape[0] != 0
                    for instances in proposals
                ]
            ), (k, targets, proposals)
            box_features = self.box_pooler(
                features, [x.proposal_boxes for x in proposals]
            )

            predictions = self._run_stage(
                box_features,
                gt_template_features,
                stage=k,
                keep_mask=keep_mask,
            )
            prev_pred_boxes = self.box_predictor[k].predict_boxes(
                predictions, proposals
            )

            head_outputs.append((self.box_predictor[k], predictions, proposals))
            head_keep_masks.append(keep_mask)

        if self.training:
            losses = {}
            storage = get_event_storage()
            for stage, (predictor, predictions, proposals) in enumerate(head_outputs):
                with storage.name_scope("stage{}".format(stage)):
                    stage_losses = predictor.losses(predictions, proposals)
                losses.update(
                    {k + "_stage{}".format(stage): v for k, v in stage_losses.items()}
                )

            pred_instances = self._train_inference(
                image_sizes, head_outputs, head_keep_masks
            )

            return losses, pred_instances
        else:
            # Each is a list[Tensor] of length #image. Each tensor is Ri x (K+1)
            scores_per_stage = [h[0].predict_probs(h[1], h[2]) for h in head_outputs]

            # Average the scores across heads
            scores = [
                sum(list(scores_per_image)) * (1.0 / self.num_cascade_stages)
                for scores_per_image in zip(*scores_per_stage)
            ]
            # Use the boxes of the last head
            predictor, predictions, proposals = head_outputs[-1]

            boxes = predictor.predict_boxes(predictions, proposals)
            # fast_rcnn_inference_with_indices added by Jens to add selected indices of batched_nms to instances
            pred_instances, _ = fast_rcnn_inference(
                boxes,
                scores,
                image_sizes,
                predictor.test_score_thresh,
                predictor.test_nms_thresh,
                predictor.test_topk_per_image,
            )

            return pred_instances

    def _forward_mask(
        self, features: Dict[str, torch.Tensor], instances: List[Instances]
    ):
        if self.training and not all([x.has("gt_masks") for x in instances]):
            return {}
        else:
            return super()._forward_mask(features, instances)

    def _train_inference(
        self,
        image_sizes: List[tuple],
        head_outputs: List[tuple],
        head_keep_masks: List[List[torch.BoolTensor]],
    ):
        scores_per_stage = [
            h[0].predict_probs(h[1], h[2]) for h in head_outputs
        ]  # head x batch x scores

        filtered_scores_per_stage = filter_scores(scores_per_stage, head_keep_masks)

        # Average the scores across heads
        # Score shape is Ri x (K+1) where background is at last position (= K+1)
        scores = [
            sum(list(scores_per_image)) * (1.0 / self.num_cascade_stages)
            for scores_per_image in zip(*filtered_scores_per_stage)
        ]

        # Use the boxes of the last head
        predictor, predictions, proposals = head_outputs[-1]
        boxes = predictor.predict_boxes(predictions, proposals)

        # fast_rcnn_inference_with_indices added by Jens to add selected indices of batched_nms to instances
        pred_instances, _ = fast_rcnn_inference(
            boxes,
            scores,
            image_sizes,
            predictor.test_score_thresh,
            predictor.test_nms_thresh,
            predictor.test_topk_per_image,
        )

        return pred_instances

    def _run_stage(
        self,
        box_features: torch.Tensor,
        gt_template_features: List[Union[torch.Tensor, List[torch.Tensor]]],
        stage: int,
        keep_mask: List[torch.Tensor],
    ):
        """
        Args:
            box_features (Tensor): #pooled features of shape (N x B, C, P, P)
            gt_template_features (list[union[Tensor, list[Tensor]]]): list of length #levels containing tensor or list of tensor of length N.
                Here each tensor has to be of the same shape (C, P, P).
            stage (int): the current stage
        Returns:
            Same output as `FastRCNNOutputLayers.forward()`.
        """
        _gt_template_features = gt_template_features[0]
        _gt_template_features = torch.cat(
            [
                _gt_template_features[i].unsqueeze(0).repeat(k.shape[0], 1, 1, 1)[k]
                for i, k in enumerate(keep_mask)
            ],
            dim=0,
        )

        features = torch.cat([box_features, _gt_template_features], dim=1)
        box_features = self.siam_combiner[stage](features)

        # The original implementation averages the losses among heads,
        # but scale up the parameter gradients of the heads.
        # This is equivalent to adding the losses among heads,
        # but scale down the gradients on features.
        box_features = _ScaleGradient.apply(box_features, 1.0 / self.num_cascade_stages)
        box_features = self.box_head[stage](box_features)
        return self.box_predictor[stage](box_features)

    def _create_proposals_from_boxes(
        self, boxes: List[torch.Tensor], image_sizes: List[tuple]
    ) -> Tuple[List[Instances], List[torch.BoolTensor]]:
        """
        Args:
            boxes (list[Tensor]): per-image predicted boxes, each of shape Ri x 4
            image_sizes (list[tuple]): list of image shapes in (h, w)

        Returns:
            list[Instances]: per-image proposals with the given boxes.
        """
        # Just like RPN, the proposals should not have gradients
        boxes = [Boxes(b.detach()) for b in boxes]
        proposals = []
        keep_mask = []
        for boxes_per_image, image_size in zip(boxes, image_sizes):
            boxes_per_image.clip(image_size)
            if self.training:
                # do not filter empty boxes at inference time,
                # because the scores from each stage need to be aligned and added later
                keep = boxes_per_image.nonempty()
                keep_mask.append(keep)
                boxes_per_image = boxes_per_image[keep]
            else:
                keep = torch.zeros([boxes_per_image.tensor.shape[0]]) == 0
                keep_mask.append(keep)
            prop = Instances(image_size)
            prop.proposal_boxes = boxes_per_image
            proposals.append(prop)
        return proposals, keep_mask

    @torch.no_grad()
    def label_and_sample_proposals_with_negatives(
        self,
        proposals: List[Instances],
        targets: List[Instances],
        negative: Optional[List[bool]],
    ) -> List[Instances]:
        gt_boxes = [x.gt_boxes for x in targets]
        if self.proposal_append_gt:
            proposals = add_ground_truth_to_proposals(gt_boxes, proposals)

        proposals_with_gt = []

        num_fg_samples = []
        num_bg_samples = []
        negative = negative if negative is not None else [False] * len(proposals)
        for proposals_per_image, targets_per_image, neg in zip(
            proposals, targets, negative
        ):
            has_gt = len(targets_per_image) > 0
            match_quality_matrix = pairwise_iou(
                targets_per_image.gt_boxes, proposals_per_image.proposal_boxes
            )
            matched_idxs, matched_labels = self.proposal_matcher(match_quality_matrix)
            if neg:
                matched_labels[matched_labels == 1] = 0
            sampled_idxs, gt_classes = self._sample_proposals(
                matched_idxs, matched_labels, targets_per_image.gt_classes
            )

            proposals_per_image = proposals_per_image[sampled_idxs]
            proposals_per_image.gt_classes = gt_classes

            if has_gt:
                sampled_targets = matched_idxs[sampled_idxs]
                for (trg_name, trg_value) in targets_per_image.get_fields().items():
                    if trg_name.startswith("gt_") and not proposals_per_image.has(
                        trg_name
                    ):
                        proposals_per_image.set(trg_name, trg_value[sampled_targets])
            else:
                gt_boxes = Boxes(
                    targets_per_image.gt_boxes.tensor.new_zeros((len(sampled_idxs), 4))
                )
                proposals_per_image.gt_boxes = gt_boxes

            num_bg_samples.append((gt_classes == self.num_classes).sum().item())
            num_fg_samples.append(gt_classes.numel() - num_bg_samples[-1])
            proposals_with_gt.append(proposals_per_image)

        # Log the number of fg/bg samples that are selected for training ROI heads
        storage = get_event_storage()
        storage.put_scalar("roi_head/num_fg_samples", np.mean(num_fg_samples))
        storage.put_scalar("roi_head/num_bg_samples", np.mean(num_bg_samples))

        return proposals_with_gt
