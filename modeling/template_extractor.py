from typing import Dict, List, Tuple, Union
import torch
from torch import nn

from detectron2.structures import Instances
from detectron2.layers import ShapeSpec
from detectron2.modeling.poolers import ROIPooler


class TemplateExtractor(nn.Module):
    def __init__(
        self,
        *,
        in_features: List[str],
        pooler_resolution: Union[int, Tuple[int], List[int]],
        sampling_ratio: int,
        pooler_type: str,
        input_shape: Dict[str, ShapeSpec],
        **kwargs,
    ):
        super().__init__()
        pooler_scales = tuple(1.0 / input_shape[k].stride for k in in_features)

        # If roi pooler is applied on multiple feature maps (as in FPN),
        # then we share the same predictors and therefore the channel counts must be the same
        in_channels = [input_shape[f].channels for f in in_features]
        # Check all channel counts are equal
        assert len(set(in_channels)) == 1, in_channels
        in_channels = in_channels[0]

        self.pooler = ROIPooler(
            output_size=pooler_resolution,
            scales=pooler_scales,
            sampling_ratio=sampling_ratio,
            pooler_type=pooler_type,
        )
        self.in_features = in_features  # cfg.MODEL.MODEL.ROI_HEADS.IN_FEATURES

    def forward(
        self,
        features: Dict[str, torch.Tensor],
        gt_instances: List[Instances],
    ) -> Dict[str, Union[torch.Tensor, List[torch.Tensor]]]:
        assert all([len(x) == 1 for x in gt_instances]), (
            "only one target supported:",
            gt_instances,
        )
        # (N, C, H, W)
        pooled_features = self.pooler(
            [features[f] for f in self.in_features],
            [x.gt_boxes for x in gt_instances],
        )
        # repeat features across levels
        return {f: pooled_features for f in self.in_features}
