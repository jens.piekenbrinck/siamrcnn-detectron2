# Siam R-CNN

PyTorch reimplementation of [Siam R-CNN](https://github.com/VisualComputingInstitute/SiamR-CNN "Siam R-CNN") using [Detectron2](https://github.com/facebookresearch/detectron2 "Detectron2").
The model does not achieve the same performance as the original TensorFlow version and it is not guaranteed that the code is correct.

---

## Installation

### Download Repository and Install Dependencies
```bash
# clone GOT10k-Toolkit repository
git clone https://github.com/pvoigtlaender/got10k-toolkit.git

# create conda environment "siamrcnn"
conda create -n siamrcnn python=3.9
conda activate siamrcnn

# install pytorch
# conda install pytorch torchvision torchaudio cudatoolkit=10.2 -c pytorch -c conda-forge
conda install pytorch==1.7.1 torchvision==0.8.2 torchaudio==0.7.2 cudatoolkit=10.2 -c pytorch -c conda-forge

# install conda dependencies
conda install xmltodict rope

# install detectron2
CUDA_HOME=/usr/local/cuda-10.2
python -m pip install 'git+https://github.com/facebookresearch/detectron2.git'


# install pip dependencies
pip install opencv-python wget shapely ipykernel
```
### **Note**
- Detectron2 has to be built with GPU/CUDA support, therefore CUDA_HOME must be available on installation:
    ```bash
    CUDA_HOME=/usr/local/cuda-10.2
    ```
- Currently CUDA 11.x has issues when using torch.randperm. The versions used in this repository:
    - python 3.9.4
    - pytorch 1.7.1
    - CUDA 10.2
- All dependencies are also listed in [requirements.txt](./requirements.txt). Some packages should be installed with conda and some with pip.

## Train

### Train on single machine with multiple GPUs:
```bash
python train.py --config-file=./configs/siam_rcnn/baseline.yaml --num-gpus=X OUTPUT_DIR output/train/siam_rcnn/baseline
```

## Evaluation

### To run evaluation on OTB:
```bash
python evaluation/got10k_toolkit/eval_otb.py --model ./output/train/siam_rcnn/baseline --name "siam_rcnn-baseline" --report --force
```

## Predict

### To run the predictor on specific OTB videos (e.g. Basketball and Diving):
```bash
python predictor/predict.py --model ./output/train/siam_rcnn/baseline --out ./output/vis/predictor/siam_rcnn-baseline --otb Basketball Diving
```
To run the predictor on all OTB videos, remove the `--otb` parameter.

## Visualization

### Run TensorBoard:
```bash
tensorboard --logdir=output/train/siam_rcnn
```
When using VSCode, Tensorboard can be run directly in VSCode, see [here](https://devblogs.microsoft.com/python/python-in-visual-studio-code-february-2021-release/) for more details.


## Datasets:
The following datasets are implemented, but must be downloaded beforehand:
- GOT10k
- ImageNet Video
- ImageNet Detection (as track dataset)
- YouTubeVOS
- YouTubeBB
- LaSOT (missing category ids)
- TAO (missing category ids)
- COCO
- OTB (only for evaluation)


## Info
The following directories will be created while executing scripts.

- *./cache*: The cache directory contains pickled datasets. The pickled datasets will be created when executing a dataset script.
- *./output/train*: This directory contains all the training outputs and pretrained models.
- *./output/got_toolkit_tracking_data*: All GOT10k-Toolkit results and reports will be stored in this directory.
- *./output/vis*: Used for non-training visualizations, e.g. preparing a dataset or results of a predictor.
