from typing import Any, Tuple, Dict, List, Union
import torch

from detectron2.structures import Boxes
from detectron2.structures import pairwise_iou
from detectron2.data import transforms as T

from predictor.siam_context_predictor import SiamContextPredictor, to_cpu


class Tracklet:
    def __init__(self, start_time: int):
        self.start_time = start_time
        self.end_time = start_time
        self.features = []
        self.boxes = []

        # first-frame data
        self.ff_gt_scores = []
        self.ff_gt_tracklet_scores = []
        self.ff_distractor_scores = []
        self.ff_distractor_tracklet_scores = []

    def add_detection(
        self,
        features: torch.Tensor,
        box: torch.Tensor,
        ff_gt_score: torch.Tensor,
        ff_gt_tracklet_score: torch.Tensor,
        ff_distractor_scores: torch.Tensor,
        ff_distractor_tracklet_scores: torch.Tensor,
    ) -> None:
        """Add detection to tracklet.
        Args:
            features (torch.Tensor): features of prediction with shape (256, 7, 7)
            box (torch.Tensor): predicted box of shape (4,)
            ff_gt_score (torch.Tensor): first-frame ground-truth score
            ff_gt_tracklet_score (torch.Tensor): first-frame ground-truth score of tracklet?
            ff_distractor_scores (torch.Tensor): first-frame distractor scores
            ff_distractor_tracklet_scores (torch.Tensor): first-frame distractor scores of tracklet?
        """
        self.features = [features]
        self.boxes.append(box)
        self.ff_gt_scores.append(ff_gt_score)
        self.ff_gt_tracklet_scores.append(ff_gt_tracklet_score)
        self.ff_distractor_scores.append(ff_distractor_scores)
        self.ff_distractor_tracklet_scores.append(ff_distractor_tracklet_scores)
        self.end_time += 1


# TODO inject last_prediction into model context or create temporal model with tracklet update
class SiamRCNNPredictor(SiamContextPredictor):
    def __init__(self, cfg):
        super().__init__(cfg)

        self._ff_distractor_score_threshold = 0.5
        self._ff_gt_iou_thres = 0.1
        self._tracklet_merging_threshold = 0.4
        self._tracklet_merging_second_best_relative_threshold = 0.1

        self._ff_gt_score_weight = 0.4
        self._ff_gt_tracklet_score_weight = 0.2
        self._ff_distractors_score_weight = 0.2
        self._ff_distractor_tracklet_score_weight = 0.2
        self._location_score_weight = 0.0

    def __call__(self, image, context) -> Tuple[Any, Dict[str, Any]]:
        assert self.frame_transforms is not None

        inputs = {}

        time = context["time"] + 1
        tracklet_context = context["tracklet_context"]
        active_tracklets: List[Tracklet] = [
            t for t in tracklet_context["all_tracklets"] if t.end_time == time
        ]

        with torch.no_grad():  # https://github.com/sphinx-doc/sphinx/issues/4258
            frame = self._data_to_frame(image, None)
            frame = self._preprocess(frame)

            if len(active_tracklets) == 0:
                active_tracklets_boxes = torch.zeros((0, 4))
                active_tracklets_features = torch.zeros((0, 256, 7, 7))
            else:
                active_tracklets_boxes = torch.stack(
                    [t.boxes[-1] for t in active_tracklets], dim=0
                )
                active_tracklets_features = torch.stack(
                    [t.features[-1] for t in active_tracklets], dim=0
                )

            active_tracklets_boxes = T.TransformList(self.frame_transforms).apply_box(
                active_tracklets_boxes.numpy()
            )
            active_tracklets_boxes = torch.from_numpy(active_tracklets_boxes)

            ff_distractor_tracklets: List[Tracklet] = tracklet_context[
                "ff_distractor_tracklets"
            ]
            if len(ff_distractor_tracklets) == 0:
                ff_distractor_tracklets_features = torch.zeros((0, 256, 7, 7))
            else:
                ff_distractor_tracklets_features = torch.stack(
                    [t.features[-1] for t in ff_distractor_tracklets], dim=0
                )

            ff_gt_tracklet: Tracklet = tracklet_context["ff_gt_tracklet"]
            ff_distractor_features = tracklet_context["ff_distractor_features"]
            frame["tracklets"] = {
                "ff_gt_tracklet_features": ff_gt_tracklet.features[-1],
                "ff_distractor_features": ff_distractor_features,
                "ff_distractor_tracklets_features": ff_distractor_tracklets_features,
                "active_tracklets_features": active_tracklets_features,
                "active_tracklets_boxes": active_tracklets_boxes,
            }

            inputs = {"search": [frame]}
            # only process one video at a time
            predictions, model_context = self.model([inputs], context["model_context"])
            predictions = predictions[0][0]  # one batch item and one time step

            # move instances to cpu, otherwise cuda oom
            predictions = to_cpu(predictions)

        tracklet_scores = predictions["active_tracklet_scores"].squeeze(-1)
        tracklet_score_keep_mask = predictions["tracklet_score_keep_mask"]
        sparse_tracklet_scores = tracklet_scores.new_zeros(
            (len(active_tracklets), *predictions["instances"].scores.shape)
        )
        sparse_tracklet_scores[tracklet_score_keep_mask] = tracklet_scores

        # TODO tracklet context is modified inplace!
        self._update_tracklets(
            active_tracklets=active_tracklets,
            boxes=predictions["instances"].pred_boxes.tensor,
            scores=predictions["instances"].scores,
            box_features=predictions["box_features"],
            ff_gt_tracklet_scores=predictions["ff_gt_tracklet_scores"].squeeze(-1),
            ff_distractor_scores=predictions["ff_distractor_scores"].squeeze(-1),
            ff_distractor_tracklets_scores=predictions[
                "ff_distractor_tracklets_scores"
            ].squeeze(-1),
            tracklet_scores=sparse_tracklet_scores,
            tracklet_context=tracklet_context,
            time=time,
        )

        box, score, dynprog_scores = self._track(
            tracklet_context=tracklet_context, img_shape=image.shape[:2], time=time
        )
        tracklet_context["dynprog_scores"] = dynprog_scores

        """
        if len(predictions["instances"]) == 0:
            return predictions, {
                "model_context": model_context,
                "tracklet_context": tracklet_context,
                "time": time
            }
        """

        predictions["bbox"] = box.numpy()
        predictions["score"] = score

        return predictions, {
            "model_context": model_context,
            "tracklet_context": tracklet_context,
            "time": time,
        }

    def init(self, image, box) -> Dict[str, Any]:
        self.frame_transforms = None
        frame = self._data_to_frame(image, box)
        inputs = {"template": self._preprocess(frame)}

        with torch.no_grad():
            model_context = self.model.get_initial_context([inputs])

        ff_predictions = to_cpu(model_context.ff_predictions[0])
        # ff_predictions = model_context.ff_predictions[0]

        result = self._init_tracklets(
            box=torch.Tensor(box),
            template=model_context.gt_template,
            predictions=ff_predictions,
        )

        model_context.ff_predictions = None

        return {
            "model_context": model_context,
            "tracklet_context": result,
            "time": 0,
        }

    def _init_tracklets(
        self,
        box: torch.Tensor,
        template: Dict[str, Union[torch.Tensor, List[torch.Tensor]]],
        predictions: Dict[str, Any],
    ) -> Dict[str, Any]:
        template_features = template[list(template.keys())[0]][0].cpu()
        boxes = predictions["instances"].pred_boxes
        scores = predictions["instances"].scores

        # filter by score and filter out boxes with high overlap with gt box
        keep_mask = scores > self._ff_distractor_score_threshold
        boxes = boxes[keep_mask]
        scores = scores[keep_mask]
        box_features = predictions["box_features"][keep_mask]

        if keep_mask.any():
            ious = pairwise_iou(boxes, Boxes(box.unsqueeze(0))).squeeze(-1)
            keep_mask = ious <= self._ff_gt_iou_thres
            boxes = boxes[keep_mask]
            scores = scores[keep_mask]
            box_features = box_features[keep_mask]

        _ff_distractor_feats = box_features
        n_distractors = len(box_features)
        _ff_gt_tracklet = Tracklet(start_time=0)
        _ff_gt_tracklet.add_detection(
            template_features,
            box,
            1.0,
            1.0,
            torch.zeros((n_distractors,)),
            torch.zeros((n_distractors,)),
        )

        _ff_distractor_tracklets = []
        for idx in range(n_distractors):
            t = Tracklet(start_time=0)
            ff_distractor_scores = torch.zeros((n_distractors,))
            ff_distractor_scores[idx] = 1.0
            t.add_detection(
                predictions["box_features"][idx],
                boxes.tensor[idx],
                0.0,
                0.0,
                ff_distractor_scores,
                ff_distractor_scores.clone(),
            )
            _ff_distractor_tracklets.append(t)
        _all_tracklets = [_ff_gt_tracklet] + _ff_distractor_tracklets

        return {
            "ff_gt_tracklet": _ff_gt_tracklet,
            "ff_distractor_features": _ff_distractor_feats,
            "ff_distractor_tracklets": _ff_distractor_tracklets,
            "all_tracklets": _all_tracklets,
            "dynprog_scores": None,
        }

    def _update_tracklets(
        self,
        active_tracklets: List[Tracklet],
        boxes: torch.Tensor,
        scores: torch.Tensor,
        box_features: torch.Tensor,
        ff_gt_tracklet_scores: torch.Tensor,
        ff_distractor_scores: torch.Tensor,
        ff_distractor_tracklets_scores: torch.Tensor,
        tracklet_scores: torch.Tensor,
        tracklet_context: Dict[str, Any],
        time: int,
    ):
        all_tracklets = tracklet_context["all_tracklets"]
        for det_idx in range(boxes.size(0)):
            merged = False
            det_args = (
                box_features[det_idx],
                boxes[det_idx],
                scores[det_idx].item(),
                ff_gt_tracklet_scores[det_idx].item(),
                ff_distractor_scores[:, det_idx],
                ff_distractor_tracklets_scores[:, det_idx],
            )

            # try to extend tracklets in active_tracklets
            if tracklet_scores.size(0) > 0:
                if tracklet_scores[:, det_idx].max() > self._tracklet_merging_threshold:
                    tracklet_idx = tracklet_scores[:, det_idx].argmax()
                    max_score = tracklet_scores[tracklet_idx, det_idx]
                    # there should be no other det which has a high similarity
                    if (
                        tracklet_scores[tracklet_idx]
                        >= max_score
                        - self._tracklet_merging_second_best_relative_threshold
                    ).sum() == 1:
                        # there should be no other tracklet to which this det is similar...
                        if (
                            tracklet_scores[:, det_idx]
                            >= max_score
                            - self._tracklet_merging_second_best_relative_threshold
                        ).sum() == 1:
                            active_tracklets[tracklet_idx].add_detection(*det_args)
                            merged = True

            # otherwise start new tracklet
            if not merged:
                tracklet = Tracklet(start_time=time)
                tracklet.add_detection(*det_args)
                all_tracklets.append(tracklet)

    def _track(
        self,
        tracklet_context: Dict[str, Any],
        img_shape: Tuple[int, int],
        time: int,
    ) -> Tuple[torch.Tensor, float]:
        all_tracklets = tracklet_context["all_tracklets"]
        last_dynprog_scores = tracklet_context["dynprog_scores"]
        # we know that the tracklets are always sorted by time!
        n_tracklets = len(all_tracklets)
        dynprog_scores = torch.full((n_tracklets,), fill_value=-1e20)
        # init gt tracklet score
        dynprog_scores[0] = 0.0
        if last_dynprog_scores is not None:
            dynprog_scores[: last_dynprog_scores.size(0)] = last_dynprog_scores
        end_times = torch.Tensor([t.end_time for t in all_tracklets])
        im_h, im_w = img_shape  # self._ff_img_noresize.shape[:2]
        norm = torch.Tensor([im_w, im_h, im_w, im_h])

        (active_indices,) = torch.where(end_times >= time + 1)
        active_tracklets = [all_tracklets[idx] for idx in active_indices]

        def xyxy_to_cxcywh(boxes: torch.Tensor):
            wh = boxes[:, 2:] - boxes[:, :2]
            centers = boxes[:, :2] + wh / 2
            return torch.cat([centers, wh], dim=-1)

        TRACKLET_KEEP_ALIVE_TIME = 1500
        if len(active_tracklets) > 0:
            if len(active_tracklets) == n_tracklets:
                alive_start_time = 0
            else:
                # select non-active tracklets: end_times < self._time_idx + 1
                alive_start_time = end_times[end_times < time + 1].max()

            (alive_indices,) = torch.where(
                end_times >= alive_start_time + 1 - TRACKLET_KEEP_ALIVE_TIME
            )
            alive_tracklets = [all_tracklets[idx] for idx in alive_indices]
            alive_end_boxes_cxcywh = xyxy_to_cxcywh(
                torch.stack([t.boxes[-1] for t in alive_tracklets])
            )
            alive_end_times = end_times[alive_indices]
            alive_dynprog_scores = dynprog_scores[alive_indices]
            active_start_boxes_cxcywh = xyxy_to_cxcywh(
                torch.stack([t.boxes[0] for t in active_tracklets])
            )
            all_pairwise_diffs = (
                torch.abs(
                    active_start_boxes_cxcywh.unsqueeze(1)
                    - alive_end_boxes_cxcywh.unsqueeze(0)
                )
                / norm
            )
            all_pairwise_diffs = -all_pairwise_diffs.mean(axis=2)

        for idx, t_idx in enumerate(active_indices):
            tracklet = all_tracklets[t_idx]
            unary = self._ff_gt_score_weight * sum(
                tracklet.ff_gt_scores
            ) + self._ff_gt_tracklet_score_weight * sum(tracklet.ff_gt_tracklet_scores)
            if (
                self._ff_distractors_score_weight > 0
                and self._ff_distractor_tracklet_score_weight > 0
                and len(all_tracklets[0].ff_distractor_scores[0]) > 0
            ):
                distractor_scores_stacked = torch.stack(tracklet.ff_distractor_scores)
                distractor_tracklet_scores_stacked = torch.stack(
                    tracklet.ff_distractor_tracklet_scores
                )
                unary += (
                    self._ff_distractors_score_weight
                    * (1.0 - torch.max(distractor_scores_stacked, axis=1).values).sum()
                )
                unary += (
                    self._ff_distractor_tracklet_score_weight
                    * (
                        1.0
                        - torch.max(distractor_tracklet_scores_stacked, axis=1).values
                    ).sum()
                )

            valid_mask = tracklet.start_time >= alive_end_times
            if valid_mask.any():
                pairwise_scores = all_pairwise_diffs[idx]
                pred_scores = (
                    alive_dynprog_scores + self._location_score_weight * pairwise_scores
                )
                pred_scores[~valid_mask] = -1e20
                best_pred_idx = pred_scores.argmax()
                best_pred_score = pred_scores[best_pred_idx]
                if best_pred_score > -1e20:
                    dynprog_scores[t_idx] = best_pred_score + unary

        t_idx = dynprog_scores.argmax()
        tracklet = all_tracklets[t_idx]
        # add current frame score weighted with epsilon to change relative ranking within tracklet
        EPSILON = 0.00001
        if tracklet.end_time >= time + 1:
            score = (
                self._ff_gt_score_weight * max(tracklet.ff_gt_scores)
                + self._ff_gt_tracklet_score_weight
                * max(tracklet.ff_gt_tracklet_scores)
                + EPSILON * tracklet.ff_gt_scores[-1]
            )
        else:
            score = -1.0 + EPSILON * tracklet.ff_gt_scores[-1]

        # or we could select the best tracklet in current frame
        return tracklet.boxes[-1], score, dynprog_scores
