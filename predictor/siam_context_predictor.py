from typing import Any, Dict, Optional, Tuple
import torch
import numpy as np

from detectron2.data import detection_utils as utils
from detectron2.data import transforms as T
from detectron2.checkpoint import DetectionCheckpointer
from detectron2.modeling import build_model
from detectron2.structures import BoxMode, Instances, Boxes


def to_cpu(x):
    if x is None:
        return None
    if isinstance(x, Instances) or isinstance(x, Boxes):
        return x.to("cpu")
    elif isinstance(x, torch.Tensor):
        return x.cpu()
    elif isinstance(x, list):
        return [to_cpu(y) for y in x]
    elif isinstance(x, dict):
        return {k: to_cpu(v) for k, v in x.items()}
    else:
        raise RuntimeError(f"unknown type: {type(x)}")


class SiamContextPredictor:
    def __init__(self, cfg):
        self.cfg = cfg.clone()  # cfg can be modified by model
        self.model = build_model(self.cfg)
        self.model.eval()

        checkpointer = DetectionCheckpointer(self.model)
        checkpointer.load(cfg.MODEL.WEIGHTS)

        self.transform_gen = T.ResizeShortestEdge(
            [cfg.INPUT.MIN_SIZE_TEST, cfg.INPUT.MIN_SIZE_TEST], cfg.INPUT.MAX_SIZE_TEST
        )

        self.input_format = cfg.INPUT.FORMAT
        assert self.input_format in ["RGB", "BGR"], self.input_format

        self.frame_transforms = None
        self.mask_format = cfg.INPUT.MASK_FORMAT

    def __call__(self, image, context) -> Tuple[Any, Dict[str, Any]]:
        pass

    def init(self, image, box) -> Dict[str, Any]:
        pass

    def _data_to_frame(self, image, box) -> Dict[str, Any]:
        # apply pre-processing to image
        if self.input_format == "RGB":
            # whether the model expects BGR inputs or RGB
            image = image[:, :, ::-1]
        height, width = image.shape[:2]
        frame = {"image": image, "height": height, "width": width}
        if box is not None:
            frame["annotations"] = [
                {"bbox": box, "bbox_mode": BoxMode.XYXY_ABS, "category_id": 0}
            ]
        return frame

    def _preprocess(self, frame: Dict[str, Any]) -> Dict[str, Any]:
        image = frame["image"]
        if self.frame_transforms is None:
            image, transforms = T.apply_transform_gens([self.transform_gen], image)
            self.frame_transforms = transforms.transforms
        else:
            image, transforms = T.apply_transform_gens(self.frame_transforms, image)

        image_shape = image.shape[:2]
        frame["image"] = torch.as_tensor(np.ascontiguousarray(image.transpose(2, 0, 1)))

        if "annotations" in frame:
            annos = [
                utils.transform_instance_annotations(
                    obj, transforms, image_shape, keypoint_hflip_indices=None
                )
                for obj in frame.pop("annotations")
                if obj.get("iscrowd", 0) == 0
            ]
            instances = utils.annotations_to_instances(
                annos, image_shape, mask_format=self.mask_format
            )
            frame["instances"] = utils.filter_empty_instances(instances)

        return frame