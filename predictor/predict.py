import os
import cv2
from pathlib import Path
import argparse
from tqdm import tqdm
import torch
import numpy as np

from detectron2.config import get_cfg
from detectron2.data import MetadataCatalog, DatasetCatalog
from detectron2.utils.visualizer import Visualizer, ColorMode
from detectron2.structures import pairwise_iou, Boxes, BoxMode

from utils import extend_config
from modeling import *  # used for REGISTRY
from data.datasets import OTBVideoDataset
from data import SiamVideoSampler
from predictor.siam_rcnn_predictor import SiamRCNNPredictor

parser = argparse.ArgumentParser(description="Run Got10kToolkit tracker on OTB2015")
parser.add_argument("--config", type=str, help="Path to the detectron2 config file")
parser.add_argument(
    "--model",
    type=str,
    help="Path to the detectron2 model directory or explicit checkpoint file",
)
parser.add_argument("--out", type=str, help="Output path", required=True)
parser.add_argument("--otb", nargs="+", help="OTB videos which should be evaluated")
parser.add_argument(
    "--opts",
    help="Modify config options using the command-line",
    default=None,
    nargs=argparse.REMAINDER,
)
args = parser.parse_args()

OTBVideoDataset.register()

if args.model is None and args.config is None:
    assert False, "at least one --model or --config has to be defined"

if args.model is not None:
    if os.path.isfile(args.model):
        MODEL_CHECKPOINT = args.model
        assert args.config is not None and os.path.isfile(
            args.config
        ), "missing or invalid config that is required if --model is a checkpoint file"
        CONFIG = args.config
    else:
        with open(os.path.join(args.model, "last_checkpoint"), "r") as f:
            MODEL_CHECKPOINT = os.path.join(args.model, f.readline())
        CONFIG = os.path.join(args.model, "config.yaml")
        assert os.path.isfile(CONFIG), f"missing or invalid config ({CONFIG})"
else:
    CONFIG = args.config
    MODEL_CHECKPOINT = None

cfg = get_cfg()
cfg = extend_config(cfg)
cfg.merge_from_file(CONFIG)
if args.opts is not None:
    cfg.merge_from_list(args.opts)
if MODEL_CHECKPOINT is not None:
    cfg.MODEL.WEIGHTS = MODEL_CHECKPOINT

if MODEL_CHECKPOINT is not None:
    print("using checkpoint:", MODEL_CHECKPOINT)

dataset = DatasetCatalog.get("otb_video")
metadata = MetadataCatalog.get("otb_video")
frame_sampler = SiamVideoSampler()

assert cfg.MODEL.META_ARCHITECTURE == "SiamRCNN"

pred = SiamRCNNPredictor(cfg)
if MODEL_CHECKPOINT is not None:
    OUTPUT_PATH = os.path.join(args.out, os.path.split(MODEL_CHECKPOINT)[-1][:-4])
else:
    OUTPUT_PATH = os.path.join(args.out, "base")
Path(OUTPUT_PATH).mkdir(parents=True, exist_ok=True)

MAX_DATASET_ITEMS = 3
VISUALIZER_SCALE = 0.7
SHOW_IOU = True

OTB_VIDEOS = args.otb

for i, dataset_dict in enumerate(dataset):
    if len(OTB_VIDEOS) > 0 and dataset_dict["id"] not in OTB_VIDEOS:
        continue
    if len(OTB_VIDEOS) == 0:
        print("predict item", f"{i+1}/{MAX_DATASET_ITEMS}")
    else:
        print("predict item", dataset_dict["id"], "|", len(dataset_dict["frames"]))
    template_frame, search_frames = frame_sampler(dataset_dict)
    template_image = cv2.imread(template_frame["file_name"])

    template_box = BoxMode.convert(
        [float(c) for c in template_frame["annotations"][0]["bbox"]],
        from_mode=template_frame["annotations"][0]["bbox_mode"],
        to_mode=BoxMode.XYXY_ABS,
    )

    # cv2.imread returns channels in BGR format
    search_images = [cv2.imread(frame["file_name"]) for frame in search_frames]

    # run model
    tic = cv2.getTickCount()
    context = pred.init(template_image, template_box)
    outputs = []
    for i, search_image in tqdm(enumerate(search_images), total=len(search_images)):
        outs, context = pred(search_image, context)
        outputs.append(outs)
    time = (cv2.getTickCount() - tic) / cv2.getTickFrequency()
    print(f"estimated speed: {len(search_images) / time:3.1f} fps")

    out = None

    if len(OTB_VIDEOS) == 0:
        print("visualize item", f"{i+1}/{MAX_DATASET_ITEMS}")
    else:
        print("visualize item", dataset_dict["id"], "|", len(dataset_dict["frames"]))

    mean_iou = []
    for t, prediction in enumerate(outputs):
        gt_box = search_frames[t]["annotations"][0]["bbox"]
        gt_box = BoxMode.convert(
            gt_box,
            from_mode=search_frames[t]["annotations"][0]["bbox_mode"],
            to_mode=BoxMode.XYXY_ABS,
        )

        gt_img = Visualizer(
            search_images[t][:, :, ::-1], metadata=metadata, scale=VISUALIZER_SCALE
        )
        gt_img.draw_box(
            gt_box,
            edge_color=metadata.thing_colors[
                search_frames[t]["annotations"][0]["category_id"]
            ]
            / 255,
        )
        gt_img = gt_img.output
        img_shape = gt_img.get_image().shape

        if out is None:
            out = cv2.VideoWriter(
                f"{OUTPUT_PATH}/{dataset_dict['id']}.mp4",
                cv2.VideoWriter_fourcc(*"mp4v"),
                30,
                (img_shape[1] * (2 - 1), img_shape[0]),
            )

        v_best_pred = Visualizer(
            search_images[t][:, :, ::-1],
            metadata=metadata,
            scale=VISUALIZER_SCALE,
            instance_mode=ColorMode.SEGMENTATION,
        )
        v_best_pred.overlay_instances(boxes=[gt_box], assigned_colors=[[0.0, 1.0, 0.0]])
        if prediction["bbox"] is None:
            mean_iou.append(0.0)
            out_img = v_best_pred.output.get_image()
            out.write(out_img[:, :, ::-1])
        else:
            v_best_pred.overlay_instances(
                boxes=prediction["bbox"][np.newaxis, :],
                assigned_colors=[[1.0, 0.0, 0.0]],
                labels=["{:.0f}%".format(prediction["score"] * 100)],
            )

            match_quality_matrix = pairwise_iou(
                Boxes(torch.Tensor(gt_box).unsqueeze(0)),
                Boxes(torch.from_numpy(prediction["bbox"][np.newaxis, :])),
            )
            iou = match_quality_matrix.squeeze(0).squeeze(0)
            mean_iou.append(iou.item())

            if SHOW_IOU:
                v_best_pred.draw_text(
                    f"iou: {iou:.3f}",
                    (12, 12),
                    color="lightcyan",
                    horizontal_alignment="left",
                )

            best_pred_img = v_best_pred.output.get_image()
            vis_img = best_pred_img
            out.write(vis_img[:, :, ::-1])

    print(f"mean iou / success score (AUC): {torch.Tensor(mean_iou).mean().item():.4f}")

    if out is not None:
        out.release()
        out = None

    if len(OTB_VIDEOS) == 0 and i == MAX_DATASET_ITEMS - 1:
        break

print("visualizations finished and stored in", f"'{OUTPUT_PATH}'")
